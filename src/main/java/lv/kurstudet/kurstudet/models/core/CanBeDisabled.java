package lv.kurstudet.kurstudet.models.core;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract public class CanBeDisabled extends TimeTracker {

    @NotNull
    @Column(name = "active")
    private Boolean isActive;

    @Column(name = "disabled_at")
    private LocalDateTime disabledAt;

    public CanBeDisabled() {
        super();
        this.setIsActive(true);
    }

    public void deactivate() {
        this.setIsActive(false);
        this.setDisabledAt(LocalDateTime.now());
    }
}

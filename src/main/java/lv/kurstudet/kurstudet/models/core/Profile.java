package lv.kurstudet.kurstudet.models.core;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.json.JSONObject;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lv.kurstudet.kurstudet.models.SingleJSONableObj;
import lv.kurstudet.kurstudet.models.auth.User;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "core_profile")
public class Profile extends SingleJSONableObj {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence_locations")
    @SequenceGenerator(name = "pk_sequence_locations", sequenceName = "education_locations_id_seq", allocationSize = 1)
    @Column(name = "id", updatable = false, unique = true)
    private Long id;

    @NotNull
    @OneToOne(optional = false, cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Override
    public JSONObject constructBottomLayer() {
        JSONObject bottomLayer = new JSONObject();
        bottomLayer.put("user", this.getUser().getId());
        return bottomLayer;
    }

    public Profile(User user) {
        this.user = user;
    }
}

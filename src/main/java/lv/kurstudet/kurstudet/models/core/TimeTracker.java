package lv.kurstudet.kurstudet.models.core;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract public class TimeTracker {

    @NotNull
    @Column(name = "date_created")
    private LocalDateTime dateCreated;

    @NotNull
    @Column(name = "date_updated")
    private LocalDateTime dateUpdated;

    public void setInitialTimeTracker(LocalDateTime dt) {
        this.dateCreated = dt;
        this.dateUpdated = dt;
    }

    public TimeTracker() {
        this.setInitialTimeTracker(LocalDateTime.now());
    }


    @Override
    public String toString() {
        return "{" +
            " dateCreated='" + dateCreated + "'" +
            ", dateUpdated='" + dateUpdated + "'" +
            "}";
    }

}

package lv.kurstudet.kurstudet.models;

import org.json.JSONObject;

public interface ISendOverWire {
    JSONObject constructBottomLayer();
    JSONObject constructMiddleLayer();
    JSONObject constructJSONObject();
}

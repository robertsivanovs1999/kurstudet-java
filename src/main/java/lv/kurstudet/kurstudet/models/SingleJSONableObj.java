package lv.kurstudet.kurstudet.models;

import org.json.JSONObject;

abstract public class SingleJSONableObj implements ISendOverWire {

    abstract public Long getId();

    public JSONObject constructMiddleLayer() {
        JSONObject middleLayer = new JSONObject();
        middleLayer.put("fields", this.constructBottomLayer());
        middleLayer.put("pk", this.getId());
        return middleLayer;
    }

    public JSONObject constructJSONObject() {
        JSONObject thirdLayer = new JSONObject();
        String middleLayer = this.constructMiddleLayer().toString();
        thirdLayer.put("data", middleLayer);
        return thirdLayer;
    }

}

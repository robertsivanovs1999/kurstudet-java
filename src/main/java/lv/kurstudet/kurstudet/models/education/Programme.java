package lv.kurstudet.kurstudet.models.education;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.json.JSONArray;
import org.json.JSONObject;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lv.kurstudet.kurstudet.models.ISendOverWire;
import lv.kurstudet.kurstudet.models.WrapperJSONableObj;
import lv.kurstudet.kurstudet.models.core.CanBeDisabled;
import lv.kurstudet.kurstudet.models.questions.Answer;
import lv.kurstudet.kurstudet.models.questions.ProgrammeParameterMap;

@Getter
@Setter
@Entity
@Table(name = "education_programme", schema = "public")
public class Programme extends CanBeDisabled implements ISendOverWire {

    @Transient // <-- this means "ignore" for hibernate
    WrapperJSONableObj jsonWrapper;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence_programme")
    @SequenceGenerator(name = "pk_sequence_programme", sequenceName = "education_programme_id_seq", allocationSize = 1)
    @Column(name = "id", updatable = false, unique = true)
    private Long id;

    @Column(name = "title")
    @Size(max = 100)
    private String title;

    @Column(name = "code")
    @Min(0)
    private long code;

    @Column(name = "creditpoints")
    @Size(max = 50)
    private String creditPoints;

    @Column(name = "description")
    private String description;

    @Column(name = "website")
    @Size(max = 300)
    private String website;

    @Column(name = "phone")
    @Size(max = 300)
    private String phone;

    @Column(name = "email")
    @Size(max = 254)
    private String email;

    @ManyToOne
    @JoinColumn(name = "degree_id")
    @NotNull
    private Degree degree;

    @ManyToOne
    @JoinColumn(name = "faculty_id")
    @NotNull
    private Faculty faculty;

    @ManyToOne
    @JoinColumn(name = "programme_type_id")
    @NotNull
    private ProgrammeTypes programme_type;

    @ManyToOne
    @JoinColumn(name = "university_id")
    @NotNull
    private University university;

    // Many to many field for duration
    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "education_programme_time", joinColumns = @JoinColumn(name = "programme_id"), inverseJoinColumns = @JoinColumn(name = "duration_id"))
    private Set<Duration> duration = new HashSet<Duration>();

    // Many to many field for duration
    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "education_programme_pay", joinColumns = @JoinColumn(name = "programme_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "payment_id", referencedColumnName = "id"))
    private Set<Payment> payment = new HashSet<Payment>();

    // Many to many field for studyForm
    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "education_programme_study_form", joinColumns = @JoinColumn(name = "programme_id"), inverseJoinColumns = @JoinColumn(name = "studyform_id"))
    private Set<StudyForm> studyForms = new HashSet<StudyForm>();

    // Many to many field for language
    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "education_programme_language", joinColumns = @JoinColumn(name = "programme_id"), inverseJoinColumns = @JoinColumn(name = "language_id"))
    private Set<Language> language = new HashSet<Language>();

    // Many to many field for studyType
    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "education_programme_study_type", joinColumns = @JoinColumn(name = "programme_id"), inverseJoinColumns = @JoinColumn(name = "studytypes_id"))
    private Set<StudyTypes> studyType = new HashSet<StudyTypes>();

    @Valid
    @OneToMany(mappedBy = "programme", fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
    private List<ProgrammeParameterMap> allPPM = new ArrayList<ProgrammeParameterMap>();

    @Override
    public String toString() {
        return "{" + " title='" + getTitle() + "'" + "}";
    }

    public Programme() {
        Programme u = this;
        Callable<JSONObject> getBottomLayer = new Callable<JSONObject>() {
            @Override
            public JSONObject call() throws Exception {
                return u.constructBottomLayer();
            }
        };
        Callable<Long> getId = new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                return u.getId();
            }
        };
        this.jsonWrapper = new WrapperJSONableObj(getId, getBottomLayer);
    }

    @Override
    public JSONObject constructBottomLayer() {
        JSONObject obj = new JSONObject();

        obj.put("active", this.getIsActive());
        obj.put("code", this.getCode());
        obj.put("title", this.getTitle());
        obj.put("phone", this.getPhone());
        obj.put("date_created", this.getDateCreated());
        obj.put("date_updated", this.getDateCreated());
        obj.put("description", this.getDescription());
        obj.put("disabled_at", this.getDisabledAt() == null ? JSONObject.NULL : this.getDisabledAt());
        obj.put("email", this.getEmail());
        obj.put("faculty_name", this.getFaculty().getName());
        obj.put("faculty_address", this.getFaculty().getLocation().getAddress());
        obj.put("faculty_id", this.getFaculty().getId());
        obj.put("time", new JSONArray());
        this.getDuration().forEach(i -> {
            obj.accumulate("time", i.getLength());
        });
        obj.put("university", this.getUniversity().getId());
        obj.put("university_name", this.getUniversity().getName());
        obj.put("website", this.getWebsite());
        // obj.put("language", String.join(", ",
        //         this.getLanguage().stream().map(Language::getAbbreviation).collect(Collectors.joining(", "))));
        obj.put("language", new JSONArray());
        this.getLanguage().forEach(i -> {
            obj.accumulate("language", i.getName());
        });
        obj.put("pay", new JSONArray());
        this.getPayment().forEach(i -> {
            obj.accumulate("pay", i.getType());
        });
        obj.put("degree", this.getDegree().getName());

        // System.out.println(obj);
        return obj;
    }

    @Override
    public JSONObject constructMiddleLayer() {
        return jsonWrapper.constructMiddleLayer();
    }

    @Override
    public JSONObject constructJSONObject() {
        return jsonWrapper.constructJSONObject();
    }
}

package lv.kurstudet.kurstudet.models.education;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lv.kurstudet.kurstudet.models.core.CanBeDisabled;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "education_studytypes")
public class StudyTypes extends CanBeDisabled {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "type", unique = true)
    @Size(max = 200)
    @NotNull
    private String type;

    // ManyToMany field for Programme
    @ManyToMany(mappedBy = "studyType", cascade = { CascadeType.ALL })
    private Set<Programme> programmes = new HashSet<Programme>();

    @Override
    public String toString() {
        return "{" +
            " type='" + getType() + "'" +
            "}";
    }
}

package lv.kurstudet.kurstudet.models.education;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.concurrent.Callable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.json.JSONObject;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lv.kurstudet.kurstudet.models.ISendOverWire;
import lv.kurstudet.kurstudet.models.WrapperJSONableObj;
import lv.kurstudet.kurstudet.models.core.CanBeDisabled;

@Getter
@Setter
@Entity
@Table(name = "education_university")
public class University extends CanBeDisabled implements ISendOverWire {

    @Transient // <-- this means "ignore" for hibernate
    WrapperJSONableObj jsonWrapper;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence_university")
    @SequenceGenerator(name = "pk_sequence_university", sequenceName = "education_university_id_seq", allocationSize = 1)
    @Column(name = "id", updatable = false, unique = true)
    private Long id;

    @Column(name = "name")
    @Size(max = 100)
    private String name;

    @ManyToOne
    @JoinColumn(name = "location_id")
    private Locations location;

    @OneToMany(mappedBy = "university", orphanRemoval = true, cascade = CascadeType.ALL)
    private Collection<Programme> programme;

    @Override
    public String toString() {
        return "{" + " name='" + getName() + "'" + "}";
    }

    public University() {

        // This is a workaround to access the constructBottomLayer() from the
        // University model when extending another class other than SingleJSONableObj
        // because JAVA does not allow multiple inheritance, at runtime we pass
        // the necessary functions to our "real" implementation.
        // This method seems overly complicated for something as basic as multiple
        // inheritance - there must be a simpler way?
        // Also - the class will only get populated at runtime, therefore we MUST
        // use some kind of code generation to achieve the desired behaviour.

        University u = this;
        Callable<JSONObject> getBottomLayer = new Callable<JSONObject>() {
            @Override
            public JSONObject call() throws Exception {
                return u.constructBottomLayer();
            }
        };
        Callable<Long> getId = new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                return u.getId();
            }
        };
        this.jsonWrapper = new WrapperJSONableObj(getId, getBottomLayer);
    }

    @Override
    public JSONObject constructBottomLayer() {
        JSONObject obj = new JSONObject();
        obj.put("date_created", this.getDateCreated());
        obj.put("date_updated", this.getDateUpdated());
        obj.put("active", this.getIsActive());
        obj.put("disabled_at", this.getDisabledAt());
        obj.put("name", this.getName());
        obj.put("location", this.getLocation().getId());
        return obj;

    }

    @Override
    public JSONObject constructMiddleLayer() {
        return jsonWrapper.constructMiddleLayer();
    }

    @Override
    public JSONObject constructJSONObject() {
        return jsonWrapper.constructJSONObject();
    }
}

package lv.kurstudet.kurstudet.models.education;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lv.kurstudet.kurstudet.models.core.CanBeDisabled;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "education_studyform")
public class StudyForm extends CanBeDisabled {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", unique = true)
    @Size(max = 200)
    @NotNull
    private String name;

    // ManyToMany field for Programme
    @ManyToMany(mappedBy = "studyForms", cascade = { CascadeType.ALL })
    private Set<Programme> programmes = new HashSet<Programme>();


    @Override
    public String toString() {
        return "{" +
            " name='" + getName() + "'" +
            "}";
    }
}

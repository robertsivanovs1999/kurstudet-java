package lv.kurstudet.kurstudet.models.education;

import java.time.LocalDateTime;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lv.kurstudet.kurstudet.models.core.CanBeDisabled;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "education_programmetypes")
public class ProgrammeTypes  extends CanBeDisabled {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "Name", unique = true)
    @Size(max = 200)
    @NotNull
    private String name;

    @OneToMany(mappedBy = "programme_type", orphanRemoval = true, cascade = CascadeType.ALL)
    private Collection<Programme> programme;

    @Override
    public String toString() {
        return "{" +
            " name='" + getName() + "'" +
            "}";
    }
}

package lv.kurstudet.kurstudet.models.education;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.json.JSONObject;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lv.kurstudet.kurstudet.models.ISendOverWire;
import lv.kurstudet.kurstudet.models.WrapperJSONableObj;
import lv.kurstudet.kurstudet.models.core.CanBeDisabled;

@Getter
@Setter
@Entity
@Table(name = "education_language")
public class Language extends CanBeDisabled implements ISendOverWire {

    @Transient // <-- this means "ignore" for hibernate
    WrapperJSONableObj jsonWrapper;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", unique = true)
    @Size(max = 100)
    @NotNull
    private String name;

    @Column(name = "abbreviation", unique = true)
    @Size(max = 100)
    @NotNull
    private String abbreviation;

    // ManyToMany field for Programme
    @ManyToMany(mappedBy = "language", cascade = { CascadeType.ALL })
    private Set<Programme> programmes = new HashSet<Programme>();


    public Language() {
        Language u = this;
        Callable<JSONObject> getBottomLayer = new Callable<JSONObject>() {
            @Override
            public JSONObject call() throws Exception {
                return u.constructBottomLayer();
            }
        };
        Callable<Long> getId = new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                return u.getId();
            }
        };
        this.jsonWrapper = new WrapperJSONableObj(getId, getBottomLayer);
    }

    @Override
    public String toString() {
        return "{" + " name='" + getName() + "'" + "}";
    }

    @Override
    public JSONObject constructBottomLayer() {
        JSONObject obj = new JSONObject();
        obj.put("date_created", this.getDateCreated());
        obj.put("date_updated", this.getDateUpdated());
        obj.put("active", this.getIsActive());
        obj.put("disabled_at", this.getDisabledAt());
        obj.put("name", this.getName());
        obj.put("abbreviation", this.getAbbreviation());
        return obj;
    }

    @Override
    public JSONObject constructMiddleLayer() {
        return jsonWrapper.constructMiddleLayer();
    }

    @Override
    public JSONObject constructJSONObject() {
        return jsonWrapper.constructJSONObject();
    }
}

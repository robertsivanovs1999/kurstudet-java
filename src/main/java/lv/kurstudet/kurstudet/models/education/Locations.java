package lv.kurstudet.kurstudet.models.education;

import java.time.LocalDateTime;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lv.kurstudet.kurstudet.models.core.CanBeDisabled;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "education_locations")
public class Locations extends CanBeDisabled {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence_locations")
    @SequenceGenerator(name = "pk_sequence_locations", sequenceName = "education_locations_id_seq", allocationSize = 1)
    @Column(name = "id", updatable = false, unique = true)
    private Long id;

    @Column(name = "country")
    @Size(max = 100)
    @NotNull
    private String country;

    @Column(name = "city")
    @Size(max = 100)
    @NotNull
    private String city;

    @Column(name = "address")
    @Size(max = 200)
    @NotNull
    private String address;

    @OneToMany(mappedBy = "location", orphanRemoval = true, cascade = CascadeType.ALL)
    private Collection<University> university;

    @OneToMany(mappedBy = "location", orphanRemoval = true, cascade = CascadeType.ALL)
    private Collection<Faculty> faculty;

    @Override
    public String toString() {
        return "{" + " country='" + getCountry() + "'" + ", city='" + getCity() + "'" + ", address='" + getAddress()
                + "'" + "}";
    }
}

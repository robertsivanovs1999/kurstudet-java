package lv.kurstudet.kurstudet.models.questions;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "questions_universityparametermap")
public class UniversityParameterMap extends ParamToObjectMap {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    // TODO uncomment this when merged with the education impl branch
    // @ManyToOne
    // @JoinColumn(name = "university_id")
    // private University programmeInstance;

}

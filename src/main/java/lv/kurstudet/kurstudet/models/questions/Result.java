package lv.kurstudet.kurstudet.models.questions;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import lv.kurstudet.kurstudet.models.core.Profile;
import lv.kurstudet.kurstudet.models.core.TimeTracker;
import lv.kurstudet.kurstudet.models.education.Degree;
import lv.kurstudet.kurstudet.models.education.Language;
import lv.kurstudet.kurstudet.models.education.StudyForm;
import lv.kurstudet.kurstudet.models.education.StudyTypes;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "questions_results")
public class Result extends TimeTracker {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence_result")
    @SequenceGenerator(name = "pk_sequence_result", sequenceName = "questions_results_id_seq", allocationSize = 1)
    @Column(name = "id", updatable = false, unique = true)
    private Long id;

    @Column(name = "calculated_time")
    private LocalDateTime calculatedTime;

    // Extra filters to apply
    @Column(name = "desired_only_free")
    private Boolean desiredOnlyFree;

    @Column(name = "desired_max_cost")
    private Integer desiredMaxCost;

    // Extra information about the respondent
    @Column(name = "location")
    @Size(max = 150)
    private String location;

    @Column(name = "current_school")
    @Size(max = 150)
    private String currentSchool;

    @Column(name = "grade")
    @Size(max = 100)
    private String grade;

    @Column(name = "age")
    private Integer age;

    // For validating the result
    @Column(name = "suggestions")
    private String suggestions;

    @Column(name = "is_satisfied")
    private Boolean isSatisfied;

    @ManyToOne
    @JoinColumn(name = "profile_id")
    private Profile profileInstance;

    // TODO create this as a possible many-to-many field
    @Valid
    @OneToMany(mappedBy = "result", orphanRemoval = true, cascade = CascadeType.ALL)
    private Collection<EvaluatedParameters> allEvaluatedParameters = new ArrayList<EvaluatedParameters>();

    @Valid
    @OneToMany(mappedBy = "result", fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Answer> allAnswers = new ArrayList<Answer>();

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "questions_results_desired_language", joinColumns = @JoinColumn(name = "results_id"), inverseJoinColumns = @JoinColumn(name = "language_id"))
    private Set<Language> allowedLanguages = new HashSet<Language>();

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "questions_results_desired_degree", joinColumns = @JoinColumn(name = "results_id"), inverseJoinColumns = @JoinColumn(name = "degree_id"))
    private Set<Degree> allowedDegrees = new HashSet<Degree>();

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "questions_results_desired_study_type", joinColumns = @JoinColumn(name = "results_id"), inverseJoinColumns = @JoinColumn(name = "studytypes_id"))
    private Set<StudyTypes> allowedStudyTypes = new HashSet<StudyTypes>();

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "questions_results_desired_study_form", joinColumns = @JoinColumn(name = "results_id"), inverseJoinColumns = @JoinColumn(name = "studyform_id"))
    private Set<StudyForm> allowedStudyForms = new HashSet<StudyForm>();

    @Override
    public String toString() {
        return "{" + super.toString() + " id='" + getId() + "'" + ", calculatedTime='" + getCalculatedTime() + "'"
                + ", desiredOnlyFree='" + desiredOnlyFree + "'" + ", desiredMaxCost='" + getDesiredMaxCost() + "'"
                + ", location='" + getLocation() + "'" + ", currentSchool='" + getCurrentSchool() + "'" + ", grade='"
                + getGrade() + "'" + ", age='" + getAge() + "'" + ", suggestions='" + getSuggestions() + "'"
                + ", isSatisfied='" + isSatisfied + "'" + ", profileInstance='" + getProfileInstance() + "'" + "}";
    }

    public static Result baseResult() {
        Result res = new Result();
        LocalDateTime t = LocalDateTime.now();
        res.setInitialTimeTracker(t);
        return res;
    }

}

package lv.kurstudet.kurstudet.models.questions;

import lv.kurstudet.kurstudet.models.core.CanBeDisabled;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "questions_answer")
public class Answer extends CanBeDisabled {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence_answer")
    @SequenceGenerator(name = "pk_sequence_answer", sequenceName = "questions_answer_id_seq", allocationSize = 1)
    @Column(name = "id", updatable = false, unique = true)
    private Long id;

    @NotNull
    @Column(name = "answer_weight")
    @Min(-100)
    @Max(100)
    private Integer answerWeight;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "questionsetmap_id")
    private QuestionSetAndQuestionMap questionSetMap;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "results_id")
    private Result result;

    public Answer(Integer answerWeight, QuestionSetAndQuestionMap questionSetMap, Result result) {
        super();
        this.answerWeight = answerWeight;
        this.questionSetMap = questionSetMap;
        this.result = result;
    }

    public Parameters getParameter() {
        return this.getQuestionSetMap().getQuestion().getParameter();
    }

    public Question getQuestion() {
        return this.getQuestionSetMap().getQuestion();
    }

    public Float evaluateAnswer() {
        return (float) (this.getQuestion().getParameterWeight() * this.answerWeight / 100);
    }

    @Override
    public String toString() {
        return "{" + " id='" + getId() + "'" + ", answerWeight='" + getAnswerWeight() + "'" + ", questionSetMap='"
                + getQuestionSetMap() + "'" + "}";
    }

}

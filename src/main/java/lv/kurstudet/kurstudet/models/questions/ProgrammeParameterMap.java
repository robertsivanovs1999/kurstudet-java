package lv.kurstudet.kurstudet.models.questions;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lv.kurstudet.kurstudet.models.education.Programme;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "questions_programmeparametermap")
public class ProgrammeParameterMap extends ParamToObjectMap {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "programme_id")
    private Programme programme;

}

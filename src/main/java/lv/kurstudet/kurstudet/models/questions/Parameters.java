package lv.kurstudet.kurstudet.models.questions;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lv.kurstudet.kurstudet.models.core.CanBeDisabled;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "questions_parameters")
public class Parameters extends CanBeDisabled {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "abbreviation")
    @Size(max = 20)
    private String abbreviation;

    @Column(name = "explanation")
    private String explanation;

    @Valid
    @OneToMany(mappedBy = "parameter", orphanRemoval = true, cascade = CascadeType.ALL)
    private Collection<Question> allQuestionsToParameter;

    @ManyToMany(mappedBy = "parameters", cascade = { CascadeType.ALL })
    private Set<ParameterTags> tags = new HashSet<ParameterTags>();

}

package lv.kurstudet.kurstudet.models.questions;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lv.kurstudet.kurstudet.models.core.CanBeDisabled;

@Getter
@Setter
@MappedSuperclass
public class ParamToObjectMap extends CanBeDisabled {

    @Column(name = "score")
    @Max(100)
    @Min(-100)
    private Integer score;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "parameter_id")
    private Parameters parameter;

}

package lv.kurstudet.kurstudet.models.questions;

import java.time.LocalDateTime;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.json.JSONObject;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lv.kurstudet.kurstudet.models.SingleJSONableObj;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "questions_questionsetandquestionmap")
public class QuestionSetAndQuestionMap extends SingleJSONableObj {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "date_created")
    private LocalDateTime dateCreated;

    @NotNull
    @Column(name = "date_updated")
    private LocalDateTime dateUpdated;

    @NotNull
    @Column(name = "\"order\"")
    private Integer order;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "question_id")
    private Question question;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "questionset_id")
    private QuestionSet questionSet;

    @Valid
    @OneToMany(mappedBy = "questionSetMap", orphanRemoval = true, cascade = CascadeType.ALL)
    private Collection<Answer> allAnswers;

    @Override
    public JSONObject constructBottomLayer() {
        JSONObject obj = new JSONObject();
        obj.put("active", this.getQuestion().getIsActive());
        obj.put("dateCreated", this.getQuestion().getDateCreated());
        obj.put("dateUpdated", this.getQuestion().getDateUpdated());
        obj.put("disabled_at", this.getQuestion().getDisabledAt());
        obj.put("parameter", this.getQuestion().getParameter());
        obj.put("parameter_weight", this.getQuestion().getParameterWeight());
        obj.put("text", this.getQuestion().getText());
        obj.put("questionsetandquestionmap", this.getId());
        return obj;
    }

    @Override
    public JSONObject constructMiddleLayer() {
        JSONObject middleLayer = new JSONObject();
        middleLayer.put("fields", this.constructBottomLayer());
        // Force the use of the question ID as the selected objects id
        middleLayer.put("pk", this.getQuestion().getId());
        return middleLayer;
    }

}

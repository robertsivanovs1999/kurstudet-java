package lv.kurstudet.kurstudet.models.questions;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "questions_parametertags")
public class ParameterTags {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Column(name = "tag")
    @Size(max=50)
    private String tag;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "questions_parameters_tags", joinColumns = @JoinColumn(name = "parametertags_id"), inverseJoinColumns = @JoinColumn(name = "parameters_id"))
    private Set<Parameters> parameters = new HashSet<Parameters>();

}

package lv.kurstudet.kurstudet.models.questions;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lv.kurstudet.kurstudet.models.core.CanBeDisabled;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "questions_question")
public class Question extends CanBeDisabled {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Column(name = "text")
    private String text;

    @NotBlank
    @Column(name = "parameter_weight")
    @Min(-100)
    @Max(100)
    private Integer parameterWeight;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "parameter_id")
    private Parameters parameter;

    // TODO set this as a many-to-many field
    @Valid
    @OneToMany(mappedBy = "questionSet", orphanRemoval = true, cascade = CascadeType.ALL)
    private Collection<QuestionSetAndQuestionMap> allQuestionSetMapsWithThisQuestion;
}

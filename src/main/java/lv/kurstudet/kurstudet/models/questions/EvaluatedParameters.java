package lv.kurstudet.kurstudet.models.questions;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import lv.kurstudet.kurstudet.models.core.TimeTracker;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "questions_evaluatedparameters")
public class EvaluatedParameters extends TimeTracker {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence_evaluatedparameters")
    @SequenceGenerator(name = "pk_sequence_evaluatedparameters", sequenceName = "questions_evaluatedparameters_id_seq", allocationSize = 1)
    @Column(name = "id", updatable = false, unique = true)
    private Long id;

    @NotNull
    @Column(name = "value")
    private Integer value;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "parameter_id")
    private Parameters parameter;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "results_id")
    private Result result;

    public EvaluatedParameters(Integer value, Parameters parameter, Result result){
        this.value = value;
        this.parameter = parameter;
        this.result = result;
    }

}

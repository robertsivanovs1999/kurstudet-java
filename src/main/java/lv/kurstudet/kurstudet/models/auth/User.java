package lv.kurstudet.kurstudet.models.auth;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.json.JSONObject;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lv.kurstudet.kurstudet.models.SingleJSONableObj;
import lv.kurstudet.kurstudet.models.core.Profile;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "auth_user")
public class User extends SingleJSONableObj implements Serializable {

    private static final long serialVersionUID = -8656301701882046700L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence_user")
    @SequenceGenerator(name = "pk_sequence_user", sequenceName = "auth_user_id_seq", allocationSize = 1)
    @Column(name = "id", updatable = false, unique = true)
    private Long id;

    @NotBlank
    @Column(name = "password")
    @Size(max = 128)
    private String password;

    @Column(name = "last_login")
    private LocalDateTime lastLogin;

    @NotNull
    @Column(name = "is_superuser")
    private boolean isSuperuser;

    @NotBlank
    @Column(name = "username")
    @Size(max = 150)
    private String username;

    @Column(name = "first_name")
    @Size(max = 30)
    private String firstName;

    @NotBlank
    @Column(name = "email")
    @Size(max = 254)
    private String email;

    @NotNull
    @Column(name = "is_staff")
    private boolean isStaff;

    @NotNull
    @Column(name = "is_active")
    private boolean isActive;

    @NotNull
    @Column(name = "date_joined")
    private LocalDateTime dateJoined;

    @Column(name = "last_name")
    @Size(max = 150)
    private String lastName;

    @OneToOne(optional = true, mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Profile profileInstance;

    @Override
    public JSONObject constructBottomLayer() {
        JSONObject bottomLayer = new JSONObject();
        bottomLayer.put("date_joined", this.getDateJoined());
        bottomLayer.put("email", this.getEmail());
        bottomLayer.put("first_name", this.getFirstName());
        // FIXME implement group passing
        bottomLayer.put("groups", "[]");
        bottomLayer.put("is_active", this.isActive());
        bottomLayer.put("is_staff", this.isStaff());
        bottomLayer.put("is_superuser", this.isSuperuser());
        bottomLayer.put("last_login", this.getLastLogin());
        bottomLayer.put("last_name", this.getLastName());
        // FIXME implement permissions
        bottomLayer.put("permissions", "[]");
        bottomLayer.put("username", this.getUsername());
        return bottomLayer;
    }

    @Override
    public String toString() {
        return "{" + " id='" + id + "'" + ", password='" + password + "'" + ", lastLogin='" + lastLogin + "'"
                + ", isSuperuser='" + isSuperuser + "'" + ", username='" + username + "'" + ", firstName='" + firstName
                + "'" + ", email='" + email + "'" + ", isStaff='" + isStaff + "'" + ", isActive='" + isActive + "'"
                + ", dateJoined='" + dateJoined + "'" + ", lastName='" + lastName + "'" + ", profile" + profileInstance
                + "}";
    }

    public static User baseUser(String username, String rawPassword, String email) {
        User u = new User();
        u.setLastName("");
        u.setFirstName("");
        u.setUsername(username);
        u.setPassword(rawPassword);
        u.setEmail(email);
        u.setDateJoined(LocalDateTime.now());
        u.setStaff(false);
        u.setActive(true);
        u.setSuperuser(false);
        return u;
    }
}

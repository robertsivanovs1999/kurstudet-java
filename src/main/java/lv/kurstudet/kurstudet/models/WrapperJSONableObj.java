package lv.kurstudet.kurstudet.models;

import java.util.concurrent.Callable;
import java.util.function.Function;

import org.json.JSONObject;

public class WrapperJSONableObj extends SingleJSONableObj {

    Callable<JSONObject> realBottomLayer;
    Callable<Long> registeredId;

    public WrapperJSONableObj(Callable<Long> registeredId, Callable<JSONObject> realBottomLayer) {
        this.registeredId = registeredId;
        this.realBottomLayer = realBottomLayer;
    }

    @Override
    public JSONObject constructBottomLayer() {
        try {
            return this.realBottomLayer.call();
        } catch (Exception e) {
            return new JSONObject();
        }
    }

    @Override
    public Long getId() {
        try {
            return registeredId.call();
        } catch (Exception e) {
            return null;
        }
    }

}

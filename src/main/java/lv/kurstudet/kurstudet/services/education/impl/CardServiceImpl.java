package lv.kurstudet.kurstudet.services.education.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.SetJoin;
import javax.persistence.criteria.Subquery;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import lv.kurstudet.kurstudet.models.education.Payment;
import lv.kurstudet.kurstudet.models.education.Degree;
import lv.kurstudet.kurstudet.models.education.Duration;
import lv.kurstudet.kurstudet.models.education.Language;
import lv.kurstudet.kurstudet.models.education.Programme;
import lv.kurstudet.kurstudet.models.education.StudyForm;
import lv.kurstudet.kurstudet.models.education.StudyTypes;
import lv.kurstudet.kurstudet.models.education.University;
import lv.kurstudet.kurstudet.repos.education.IDegreeRepo;
import lv.kurstudet.kurstudet.repos.education.IDurationRepo;
import lv.kurstudet.kurstudet.repos.education.ILanguageRepo;
import lv.kurstudet.kurstudet.repos.education.IPaymentRepo;
import lv.kurstudet.kurstudet.repos.education.IProgrammeRepo;
import lv.kurstudet.kurstudet.repos.education.IStudyFormRepo;
import lv.kurstudet.kurstudet.repos.education.IStudyTypesRepo;
import lv.kurstudet.kurstudet.repos.education.IUniversityRepo;
import lv.kurstudet.kurstudet.services.PagedMeta;
import lv.kurstudet.kurstudet.services.education.ICardService;

@Service
public class CardServiceImpl implements ICardService {

    @Autowired
    IUniversityRepo uniRepo;

    @Autowired
    IProgrammeRepo progRep;

    @Autowired
    IPaymentRepo payRep;

    @Autowired
    IStudyFormRepo studFormRep;

    @Autowired
    IStudyTypesRepo studTypeRep;

    @Autowired
    IDurationRepo durRep;

    @Autowired
    IDegreeRepo degRep;

    @Autowired
    ILanguageRepo langRep;

    @Override
    public ArrayList<University> getAllUniversities() {
        return (ArrayList<University>) uniRepo.findAll();
    }

    @Override
    public ArrayList<HashMap<String, Object>> getAllProgrammesAggregate() {
        HashMap<String, Number> uniqueValues = new HashMap<>();
        // FIXME: Is there a better way to use spring JPA to aggregate all of the data
        // as we want it using SQL ?
        ArrayList<Programme> nonUniqueValues = new ArrayList<>(progRep.findAll());

        nonUniqueValues.forEach(i -> {
            if (uniqueValues.containsKey(i.getTitle())) {
                uniqueValues.put(i.getTitle(), 1 + (Integer) uniqueValues.get(i.getTitle()));
            } else {
                uniqueValues.put(i.getTitle(), 1);
            }
        });

        ArrayList<HashMap<String, Object>> uniqueValueArray = new ArrayList<>();

        uniqueValues.forEach((key, val) -> {
            HashMap<String, Object> aggregatedIter = new HashMap<>();
            aggregatedIter.put("title", key);
            aggregatedIter.put("repeats", val);
            uniqueValueArray.add(aggregatedIter);
        });

        return uniqueValueArray;
    }

    @Override
    public ArrayList<Payment> getAllPayments() {
        return new ArrayList<>(payRep.findAll());
    }

    @Override
    public ArrayList<HashMap<String, Object>> getStudyFormAggregate() {
        HashMap<String, Number> uniqueValues = new HashMap<>();
        // FIXME: Is there a better way to use spring JPA to aggregate all of the data
        // as we want it using SQL ?
        ArrayList<StudyForm> nonUniqueValues = new ArrayList<>(studFormRep.findAll());

        nonUniqueValues.forEach(i -> {
            if (uniqueValues.containsKey(i.getName())) {
                uniqueValues.put(i.getName(), 1 + (Integer) uniqueValues.get(i.getName()));
            } else {
                uniqueValues.put(i.getName(), 1);
            }
        });

        ArrayList<HashMap<String, Object>> uniqueValueArray = new ArrayList<>();

        uniqueValues.forEach((key, val) -> {
            HashMap<String, Object> aggregatedIter = new HashMap<>();
            aggregatedIter.put("name", key);
            aggregatedIter.put("repeats", val);
            uniqueValueArray.add(aggregatedIter);
        });

        return uniqueValueArray;
    }

    @Override
    public ArrayList<HashMap<String, Object>> getStudyTypeAggregate() {
        HashMap<String, Number> uniqueValues = new HashMap<>();
        // FIXME: Is there a better way to use spring JPA to aggregate all of the data
        // as we want it using SQL ?
        ArrayList<StudyTypes> nonUniqueValues = new ArrayList<>(studTypeRep.findAll());

        nonUniqueValues.forEach(i -> {
            if (uniqueValues.containsKey(i.getType())) {
                uniqueValues.put(i.getType(), 1 + (Integer) uniqueValues.get(i.getType()));
            } else {
                uniqueValues.put(i.getType(), 1);
            }
        });

        ArrayList<HashMap<String, Object>> uniqueValueArray = new ArrayList<>();

        uniqueValues.forEach((key, val) -> {
            HashMap<String, Object> aggregatedIter = new HashMap<>();
            aggregatedIter.put("type", key);
            aggregatedIter.put("repeats", val);
            uniqueValueArray.add(aggregatedIter);
        });

        return uniqueValueArray;
    }

    @Override
    public ArrayList<Duration> getALlDurations() {
        return new ArrayList<>(durRep.findAll());
    }

    @Override
    public ArrayList<Degree> getAllDegrees() {
        return new ArrayList<>(degRep.findAll());
    }

    @Override
    public PagedMeta<Programme> fetchProgrammes(Map<String, Object> queryParams, Integer selectedPage,
            Integer pageSize) {
        PageRequest pageReq = PageRequest.of(Integer.valueOf(selectedPage) - 1, pageSize,
                Sort.by(Sort.Direction.ASC, "id"));

        JSONObject jsonQuery = new JSONObject((String) queryParams.get("json"));
        List<Programme> programmes = progRep.findAll(new Specification<Programme>() {
            @Override
            public Predicate toPredicate(Root<Programme> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                Predicate p = cb.conjunction();

                if (jsonQuery.has("university") && !(jsonQuery.getJSONArray("university").isEmpty())) {

                    ArrayList<Long> uniIds = new ArrayList<>();

                    jsonQuery.getJSONArray("university").forEach(i -> {
                        uniIds.add(Long.valueOf((int) i));
                    });

                    query.subquery(University.class);
                    Subquery<University> subquery = query.subquery(University.class);
                    Root<University> uni = subquery.from(University.class);
                    subquery.select(uni).distinct(true).where(uni.get("id").in(uniIds));
                    p = cb.and(p, cb.in(root.get("university")).value(subquery));

                }

                // FIXME: Using criteria API for filtering valid data is way more efficient than
                // doing it manually
                // but I was not able to get it working for many-to-many fields thus making the
                // whole website very
                // inefficient.
                /*
                 * if (jsonQuery.has("pay_id") && !(jsonQuery.getJSONArray("pay_id").isEmpty()))
                 * { ArrayList<Long> payIds = new ArrayList<>();
                 *
                 * jsonQuery.getJSONArray("pay_id").forEach(i -> { payIds.add(Long.valueOf((int)
                 * i)); });
                 *
                 * // Get all matching payments Join<Programme, Payment> join =
                 * root.join("payment", JoinType.INNER); Subquery<Payment> subquery =
                 * query.subquery(Payment.class); Subquery<Payment> subquery2 =
                 * subquery.select(join).where(join.get("payment").in(payIds)); // p = cb.and(p,
                 * cb.in(root.get("payment")).value(subquery2)); }
                 */
                if (jsonQuery.has("title") && !(jsonQuery.getJSONArray("title").isEmpty())) {

                    ArrayList<String> progrNames = new ArrayList<>();
                    // TODO try to find a way to look for "contains" % within strings insterad of
                    // exact matches
                    jsonQuery.getJSONArray("title").forEach(i -> {
                        progrNames.add(String.valueOf((String) i));
                    });

                    p = cb.and(p, root.get("title").in(progrNames));

                }
                query.orderBy(cb.desc(root.get("id")));
                return p;
            }
        });

        if (jsonQuery.has("pay_id") && !(jsonQuery.getJSONArray("pay_id").isEmpty())) {
            HashSet<Long> payIds = new HashSet<>();

            jsonQuery.getJSONArray("pay_id").forEach(i -> {
                payIds.add(Long.valueOf((int) i));
            });

            // Get all matching payments
            programmes.removeIf(i -> {
                HashSet<Long> validIds = new HashSet<>();
                i.getPayment().forEach(j -> validIds.add(j.getId()));
                validIds.retainAll(payIds);
                return validIds.isEmpty();
            });
        }
        if (jsonQuery.has("time") && !(jsonQuery.getJSONArray("time").isEmpty())) {
            HashSet<Long> payIds = new HashSet<>();

            jsonQuery.getJSONArray("time").forEach(i -> {
                payIds.add(Long.valueOf((int) i));
            });

            programmes.removeIf(i -> {
                HashSet<Long> validIds = new HashSet<>();
                i.getDuration().forEach(j -> validIds.add(j.getId()));
                validIds.retainAll(payIds);
                return validIds.isEmpty();
            });
        }
        if (jsonQuery.has("study_form") && !(jsonQuery.getJSONArray("study_form").isEmpty())) {
            HashSet<String> studyFormNames = new HashSet<>();

            jsonQuery.getJSONArray("study_form").forEach(i -> {
                studyFormNames.add(String.valueOf(i));
            });

            programmes.removeIf(i -> {
                HashSet<String> validIds = new HashSet<>();
                i.getStudyForms().forEach(j -> validIds.add(j.getName()));
                validIds.retainAll(studyFormNames);
                return validIds.isEmpty();
            });
        }
        if (jsonQuery.has("study_type") && !(jsonQuery.getJSONArray("study_type").isEmpty())) {
            HashSet<String> studyFormNames = new HashSet<>();

            jsonQuery.getJSONArray("study_type").forEach(i -> {
                studyFormNames.add(String.valueOf(i));
            });

            programmes.removeIf(i -> {
                HashSet<String> validIds = new HashSet<>();
                i.getStudyType().forEach(j -> validIds.add(j.getType()));
                validIds.retainAll(studyFormNames);
                return validIds.isEmpty();
            });
        }
        if (jsonQuery.has("degree") && !(jsonQuery.getJSONArray("degree").isEmpty())) {
            HashSet<String> degrees = new HashSet<>();

            jsonQuery.getJSONArray("degree").forEach(i -> {
                degrees.add(String.valueOf(i));
            });

            programmes.removeIf(i -> {
                return !degrees.contains(i.getDegree().getAggregate());
            });
        }

        List<Long> validId = new ArrayList<Long>();

        programmes.forEach(i -> validId.add(i.getId()));

        Page<Programme> page = progRep.findByIdIn(validId, pageReq);

        PagedMeta<Programme> pm = new PagedMeta<Programme>(page.getContent(), page.getTotalPages(),
                (int) page.getTotalElements(), selectedPage, pageSize);
        return pm;
    }

    @Override
    public ArrayList<Language> getAllLanguages() {
        return new ArrayList<>(langRep.findAll());
    }

}

package lv.kurstudet.kurstudet.services.education;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import lv.kurstudet.kurstudet.models.education.Degree;
import lv.kurstudet.kurstudet.models.education.Duration;
import lv.kurstudet.kurstudet.models.education.Language;
import lv.kurstudet.kurstudet.models.education.Payment;
import lv.kurstudet.kurstudet.models.education.Programme;
import lv.kurstudet.kurstudet.models.education.StudyForm;
import lv.kurstudet.kurstudet.models.education.University;
import lv.kurstudet.kurstudet.services.PagedMeta;

public interface ICardService {
    ArrayList<University> getAllUniversities();

    ArrayList<Payment> getAllPayments();

    ArrayList<Duration> getALlDurations();

    ArrayList<Degree> getAllDegrees();

    ArrayList<Language> getAllLanguages();

    ArrayList<HashMap<String, Object>> getAllProgrammesAggregate();

    ArrayList<HashMap<String, Object>> getStudyFormAggregate();

    ArrayList<HashMap<String, Object>> getStudyTypeAggregate();

    PagedMeta<Programme> fetchProgrammes(Map<String, Object> queryParams, Integer selectedPage,
            Integer pageSize);

}

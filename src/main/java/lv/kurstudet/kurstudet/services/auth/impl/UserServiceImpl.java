package lv.kurstudet.kurstudet.services.auth.impl;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lv.kurstudet.kurstudet.models.auth.User;
import lv.kurstudet.kurstudet.models.core.Profile;
import lv.kurstudet.kurstudet.repos.auth.IUserRepo;
import lv.kurstudet.kurstudet.repos.core.IProfileRepo;
import lv.kurstudet.kurstudet.services.auth.IUserService;

@Service
public class UserServiceImpl implements IUserService {

    private PasswordHasher pwHasher = new PasswordHasher();

    @Autowired
    IUserRepo iro;

    @Autowired
    IProfileRepo pro;

    @Override
    public boolean updateUserById(Long id, User newUserData) {
        if (id > 0 && newUserData != null) {
            User userToUpdate = iro.findById(id).get();
            if (userToUpdate != null) {
                userToUpdate.setFirstName(newUserData.getFirstName());
                userToUpdate.setLastName(newUserData.getLastName());
                userToUpdate.setEmail(newUserData.getEmail());
                userToUpdate.setStaff(newUserData.isStaff());
                userToUpdate.setSuperuser(newUserData.isSuperuser());
                userToUpdate.setActive(newUserData.isActive());
                this.iro.save(userToUpdate);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean updatePasswordById(Long id, String oldPass, String newPass) {
        if (id > 0 && newPass != null && !newPass.isEmpty()) {
            User userToUpdate = iro.findById(id).get();

            String existingSalt = userToUpdate.getPassword().split("\\$")[2];
            String inputHashedPw = pwHasher.validate(oldPass, existingSalt);
            String storedHashedPw = userToUpdate.getPassword().split("\\$")[3];

            boolean correctPassword = inputHashedPw.equals(storedHashedPw);
            if (correctPassword) {
                if (userToUpdate != null) {
                    userToUpdate.setPassword(pwHasher.hashPassword(newPass));
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean deleteUserById(Long id) {
        if (id > 0 && iro.existsById(id)) {
            iro.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public User getByUsername(String username) {
        return iro.findByUsername(username);
    }

    @Override
    public boolean existsByEmail(String email) {
        return iro.existsByEmail(email);
    }

    @Override
    public User getById(Long id) {
        return iro.findById(id).get();
    }

    @Override
    public List<User> getAllUsers() {
        return iro.findAll();
    }

    @Override
    public boolean authenticate(String username, String password) {
        User user = iro.findByUsername(username);
        String existingSalt = user.getPassword().split("\\$")[2];
        String inputHashedPw = pwHasher.validate(password, existingSalt);
        String storedHashedPw = user.getPassword().split("\\$")[3];

        boolean correctPassword = inputHashedPw.equals(storedHashedPw);
        if (correctPassword) {
            user.setLastLogin(LocalDateTime.now());
            this.iro.save(user);
        }
        return correctPassword;
    }

    @Override
    public boolean register(User userToRegister) {
        // Create user
        userToRegister.setPassword(pwHasher.hashPassword(userToRegister.getPassword()));
        userToRegister = iro.save(userToRegister);
        // Create profile
        Profile profileInstance = new Profile();
        profileInstance.setUser(userToRegister);
        profileInstance = pro.save(profileInstance);

        /**
         * Set profile instance (?) This part of the code logically is not needed as the
         * Profile entity is the one that is responsible for storing the FK to User
         * profile. The issue seems to be that Hibernate uses some kind of internal
         * caching mechanism and because of that the user instance returned by the repo
         * will not hold the attached profile instance (if the repo call is done within
         * the same controllers' endpoint)
         */
        userToRegister.setProfileInstance(profileInstance);
        userToRegister = iro.save(userToRegister);
        return true;
    }

    class PasswordHasher {
        /**
         * Trying to mimic the Django way of storing passwords for maximum compatibility
         * with previous data
         * https://github.com/django/django/blob/06670015f7e55a8be8137dbd95b7f4c536c3782b/django/contrib/auth/hashers.py#L230
         */

        private Integer iterations = 180000;
        private Integer length = 256;
        private SecretKeyFactory factory;

        public PasswordHasher() {
            try {
                this.factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            } catch (NoSuchAlgorithmException e) {
                this.factory = null;
                e.printStackTrace();
            }
        }

        public byte[] getSalt() {
            SecureRandom random = new SecureRandom();
            byte[] salt = new byte[12];
            random.nextBytes(salt);
            return salt;
        }

        public String hashPassword(String password) {
            return this.encode(password, this.getSalt());
        }

        private String encodeBytes(byte[] toEncode) {
            byte[] based = Base64.getEncoder().encode(toEncode);
            String stringified = new String(based, StandardCharsets.US_ASCII);
            return stringified;
        }

        private byte[] decodeStoredToBytes(String toDecode) {
            byte[] based = Base64.getDecoder().decode(toDecode);
            return Base64.getEncoder().encode(based);
        }

        public String encode(String password, byte[] salt) {
            try {
                // Construct hash
                KeySpec spec = new PBEKeySpec(password.toCharArray(), Base64.getEncoder().encode(salt), iterations,
                        length);
                byte[] hash = factory.generateSecret(spec).getEncoded();
                return "pbkdf2_sha256$" + iterations + "$" + encodeBytes(salt) + "$" + encodeBytes(hash);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        public String validate(String password, String salt) {
            try {
                // Construct hash
                byte[] existingSalt = pwHasher.decodeStoredToBytes(salt);
                KeySpec spec = new PBEKeySpec(password.toCharArray(), existingSalt, iterations, length);
                byte[] hash = factory.generateSecret(spec).getEncoded();
                return encodeBytes(hash);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }
    }

    @Override
    public boolean existsByUsername(String username) {
        return iro.existsByUsername(username);
    }

    @Override
    public List<User> getAllByIsActive(boolean isActive) {
        return iro.findAllByIsActive(isActive);
    }

    @Override
    public List<User> getAllByIsStaff(boolean isStaff) {
        return iro.findAllByIsStaff(isStaff);
    }

    @Override
    public List<User> getAllByIsSuperuser(boolean isSuperuser) {
        return iro.findAllByIsSuperuser(isSuperuser);
    }

    @Override
    public List<User> getAllByIsActiveAndIsStaffAndIsSuperuser(boolean isActive, boolean isStaff, boolean isSuperuser) {
        if (isActive && !isStaff && !isSuperuser) {
            return this.iro.findAllByIsActive(isActive);
        } else if (!isActive && isStaff && !isSuperuser) {
            return this.iro.findAllByIsStaff(isStaff);
        } else if (!isActive && !isStaff && isSuperuser) {
            return this.iro.findAllByIsSuperuser(isSuperuser);
        } else if (isActive && isStaff && !isSuperuser) {
            return this.iro.findAllByIsActiveAndIsStaff(isActive, isStaff);
        } else if (isActive && !isStaff && isSuperuser) {
            return this.iro.findAllByIsActiveAndIsSuperuser(isActive, isSuperuser);
        } else if (!isActive && isStaff && isSuperuser) {
            return this.iro.findAllByIsStaffAndIsSuperuser(isStaff, isSuperuser);
        } else if (isActive && isStaff && isSuperuser) {
            return this.iro.findAllByIsActiveAndIsStaffAndIsSuperuser(isActive, isStaff, isSuperuser);
        } else {
            return this.getAllUsers();
        }
    }

    @Override
    public User getByEmail(String email) {
        if (email != null && email != "") {
            return this.iro.findByEmail(email);
        }
        return null;
    }

    @Override
    public ArrayList<User> getAllByLastName(String lastName) {
        if (lastName != null && lastName != "") {
            return (ArrayList<User>) this.iro.findAllByLastName(lastName);
        }
        return null;
    }

    @Override
    public ArrayList<User> getAllByTextParams(String username, String lastName, String email) {
        if (username != null && lastName != null && email != null) {
            if (!username.equals("") && !lastName.equals("") && !email.equals("")) {
                return (ArrayList<User>) this.iro.findAllByUsernameAndLastNameAndEmail(username, lastName, email);
            } else if (!username.equals("") && !lastName.equals("") && email.equals("")) {
                return (ArrayList<User>) this.iro.findAllByUsernameAndLastName(username, lastName);
            } else if (username.equals("") && !lastName.equals("") && !email.equals("")) {
                return (ArrayList<User>) this.iro.findAllByLastNameAndEmail(lastName, email);
            } else if (!username.equals("") && lastName.equals("") && !email.equals("")) {
                return (ArrayList<User>) this.iro.findAllByUsernameAndEmail(username, email);
            } else if (!username.equals("") && lastName.equals("") && email.equals("")) {
                ArrayList<User> userList = new ArrayList<>();
                userList.add(this.iro.findByUsername(username));
                return userList;
            } else if (username.equals("") && !lastName.equals("") && email.equals("")) {
                return (ArrayList<User>) this.iro.findAllByLastName(lastName);
            } else if (username.equals("") && lastName.equals("") && !email.equals("")) {
                ArrayList<User> userList = new ArrayList<>();
                userList.add(this.iro.findByEmail(email));
                return userList;
            } else {
                return (ArrayList<User>) this.iro.findAll();
            }
        }
        return null;
    }
}

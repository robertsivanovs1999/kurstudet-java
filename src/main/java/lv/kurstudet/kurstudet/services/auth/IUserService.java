package lv.kurstudet.kurstudet.services.auth;

import java.util.List;

import lv.kurstudet.kurstudet.models.auth.User;

public interface IUserService {

    boolean updateUserById(Long id, User newUserData);

    boolean updatePasswordById(Long id, String oldPass, String newPass);

    boolean deleteUserById(Long id);

    User getByUsername(String username);

    User getByEmail(String email);

    List<User> getAllByLastName(String lastName);

    boolean existsByEmail(String email);

    boolean existsByUsername(String username);

    User getById(Long id);

    List<User> getAllByIsActive(boolean isActive);

    List<User> getAllByIsStaff(boolean isStaff);

    List<User> getAllByIsSuperuser(boolean isSuperuser);

    List<User> getAllByIsActiveAndIsStaffAndIsSuperuser(boolean isActive, boolean isStaff, boolean isSuperuser);

    List<User> getAllUsers();

    List<User> getAllByTextParams(String username, String lastName, String email);

    boolean authenticate(String username, String password);

    boolean register(User userToRegister);

}

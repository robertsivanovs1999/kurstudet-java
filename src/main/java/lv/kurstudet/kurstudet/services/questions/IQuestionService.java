package lv.kurstudet.kurstudet.services.questions;

import java.util.ArrayList;
import java.util.Map;

import lv.kurstudet.kurstudet.models.auth.User;
import lv.kurstudet.kurstudet.models.education.Degree;
import lv.kurstudet.kurstudet.models.education.Language;
import lv.kurstudet.kurstudet.models.education.Payment;
import lv.kurstudet.kurstudet.models.education.Programme;
import lv.kurstudet.kurstudet.models.education.StudyForm;
import lv.kurstudet.kurstudet.models.education.StudyTypes;
import lv.kurstudet.kurstudet.models.questions.Answer;
import lv.kurstudet.kurstudet.models.questions.QuestionSet;
import lv.kurstudet.kurstudet.models.questions.QuestionSetAndQuestionMap;
import lv.kurstudet.kurstudet.models.questions.Result;
import lv.kurstudet.kurstudet.models.questions.Parameters;
import lv.kurstudet.kurstudet.models.questions.ProgrammeParameterMap;
import lv.kurstudet.kurstudet.services.PagedMeta;

public interface IQuestionService {
    public PagedMeta<QuestionSetAndQuestionMap> fetchQuestions(QuestionSet selectedQuestionSet, Integer selectedPage,
            Integer pageSize);

    public QuestionSet getQuestionSetByName(String questionSetName);

    public QuestionSetAndQuestionMap getQmapById(Long id);

    public Result createResult(Result newResult);

    public Result findResultById(Long id);

    public Result calculateResult(Result result);

    public Answer saveAnswer(Answer answer);

    public ArrayList<Programme> getAllProgrammes();

    public Programme getProgrammeById(Long id);

    public ArrayList<Payment> getFreePayment();

    public ArrayList<Payment> getNonFreePayment();

    public ArrayList<ProgrammeParameterMap> getProgrammeParameterMap(Parameters param, ArrayList<Programme> programmes);

    public ArrayList<Degree> findDegreesByAggregate(ArrayList<String> degreeAggregates);

    public ArrayList<Language> findSelectedLanguages(ArrayList<String> languageAbbreviations);

    public ArrayList<StudyForm> findSelectedStudyForms(ArrayList<String> studyFormsName);

    public ArrayList<StudyTypes> findSelectedStudyTypes(ArrayList<String> studyTypesType);

    public Result setResultDataBesidesAnswers(Result result, Map<String, Object> answers, User currentUser);
}

package lv.kurstudet.kurstudet.services.questions.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import lv.kurstudet.kurstudet.models.auth.User;
import lv.kurstudet.kurstudet.models.education.Degree;
import lv.kurstudet.kurstudet.models.education.Language;
import lv.kurstudet.kurstudet.models.education.Payment;
import lv.kurstudet.kurstudet.models.education.Programme;
import lv.kurstudet.kurstudet.models.education.StudyForm;
import lv.kurstudet.kurstudet.models.education.StudyTypes;
import lv.kurstudet.kurstudet.models.questions.Answer;
import lv.kurstudet.kurstudet.models.questions.EvaluatedParameters;
import lv.kurstudet.kurstudet.models.questions.Parameters;
import lv.kurstudet.kurstudet.models.questions.ProgrammeParameterMap;
import lv.kurstudet.kurstudet.models.questions.QuestionSet;
import lv.kurstudet.kurstudet.models.questions.QuestionSetAndQuestionMap;
import lv.kurstudet.kurstudet.models.questions.Result;
import lv.kurstudet.kurstudet.repos.education.IDegreeRepo;
import lv.kurstudet.kurstudet.repos.education.ILanguageRepo;
import lv.kurstudet.kurstudet.repos.education.IPaymentRepo;
import lv.kurstudet.kurstudet.repos.education.IProgParamMapRepo;
import lv.kurstudet.kurstudet.repos.education.IProgrammeRepo;
import lv.kurstudet.kurstudet.repos.education.IStudyFormRepo;
import lv.kurstudet.kurstudet.repos.education.IStudyTypesRepo;
import lv.kurstudet.kurstudet.repos.questions.IAnswerRepo;
import lv.kurstudet.kurstudet.repos.questions.IEvaluatedParamRepo;
import lv.kurstudet.kurstudet.repos.questions.IQuestionSetAndQuestionMapRepo;
import lv.kurstudet.kurstudet.repos.questions.IQuestionSetRepo;
import lv.kurstudet.kurstudet.repos.questions.IResultRepo;
import lv.kurstudet.kurstudet.services.PagedMeta;
import lv.kurstudet.kurstudet.services.questions.IQuestionService;

@Service
public class QuestionServiceImpl implements IQuestionService {

    @Autowired
    IQuestionSetAndQuestionMapRepo qsqmapRep;

    @Autowired
    IQuestionSetRepo qsRep;

    @Autowired
    IResultRepo resRep;

    @Autowired
    IDegreeRepo degRep;

    @Autowired
    IAnswerRepo ansRep;

    @Autowired
    IEvaluatedParamRepo evalParamRep;

    @Autowired
    IProgrammeRepo progRep;

    @Autowired
    IPaymentRepo payRep;

    @Autowired
    IProgParamMapRepo ppmRep;

    @Autowired
    ILanguageRepo langRep;

    @Autowired
    IStudyFormRepo studFRepo;

    @Autowired
    IStudyTypesRepo studTRepo;

    @Override
    public PagedMeta<QuestionSetAndQuestionMap> fetchQuestions(QuestionSet selectedQuestionSet, Integer selectedPage,
            Integer pageSize) {
        // The PageRequest is zero padded for selected page value, whereas the incoming
        // data is 1 padded. That is an artifact of the frontend being made for the
        // django code first-hand
        PageRequest pageReq = PageRequest.of(Integer.valueOf(selectedPage) - 1, pageSize,
                Sort.by(Sort.Direction.ASC, "order"));
        Page<QuestionSetAndQuestionMap> page = qsqmapRep.findAllByQuestionSet(selectedQuestionSet, pageReq);
        ArrayList<QuestionSetAndQuestionMap> pageList = new ArrayList<>();
        page.forEach(i -> pageList.add(i));
        PagedMeta<QuestionSetAndQuestionMap> pm = new PagedMeta<>(pageList, page.getTotalPages(),
                (int) page.getTotalElements(), selectedPage, pageSize);
        return pm;
    }

    @Override
    public QuestionSet getQuestionSetByName(String questionSetName) {
        return qsRep.findByName(questionSetName);
    }

    @Override
    public Result createResult(Result newResult) {
        return resRep.save(newResult);
    }

    @Override
    public Answer saveAnswer(Answer answer) {
        Answer a = ansRep.save(answer);
        answer.getResult().getAllAnswers().add(a);
        return a;
    }

    @Override
    public QuestionSetAndQuestionMap getQmapById(Long id) {
        // TODO implement error handling (the .get() call)
        return qsqmapRep.findById(id).get();
    }

    @Override
    public Result calculateResult(Result result) {
        result.setCalculatedTime(LocalDateTime.now());
        result.setDateCreated(LocalDateTime.now());
        result.setDateUpdated(LocalDateTime.now());

        Result updatedRes = resRep.save(result);
        HashMap<Parameters, HashMap<String, Number>> viewedParameters = new HashMap<>();
        for (Answer answerIter : updatedRes.getAllAnswers()) {
            Parameters param = answerIter.getParameter();
            Float val = answerIter.evaluateAnswer();
            if (!viewedParameters.containsKey(param)) {
                HashMap<String, Number> miniData = new HashMap<>();
                miniData.put("count", 1);
                miniData.put("value", val);
                viewedParameters.put(param, miniData);
            } else {
                viewedParameters.get(param).put("value", (float) viewedParameters.get(param).get("value") + val);
                viewedParameters.get(param).put("count", (int) viewedParameters.get(param).get("count") + 1);
            }
        }

        viewedParameters.forEach((key, val) -> {
            EvaluatedParameters eParam = new EvaluatedParameters();
            eParam.setResult(updatedRes);
            eParam.setParameter(key);
            eParam.setValue((int) ((float) val.get("value") / Float.valueOf((int) val.get("count"))));
            evalParamRep.save(eParam);
        });

        return this.findResultById(updatedRes.getId());
    }

    @Override
    public Result findResultById(Long id) {
        // TODO implement error handling (the .get() call)
        return resRep.findById(id).get();
    }

    @Override
    public ArrayList<Programme> getAllProgrammes() {
        return new ArrayList<>(progRep.findAll());
    }

    @Override
    public ArrayList<Payment> getFreePayment() {
        return payRep.findAllByType("Valsts budžets");
    }

    @Override
    public ArrayList<Payment> getNonFreePayment() {
        return payRep.findAllByTypeNot("Valsts budžets");
    }

    @Override
    public ArrayList<ProgrammeParameterMap> getProgrammeParameterMap(Parameters param,
            ArrayList<Programme> programmes) {
        return ppmRep.findByParameterIsAndProgrammeIn(param, programmes);
    }

    @Override
    public Programme getProgrammeById(Long id) {
        // TODO implement error handling (the .get() call)
        return progRep.findById(id).get();
    }

    @Override
    public ArrayList<Degree> findDegreesByAggregate(ArrayList<String> degreeAggregates) {
        return degRep.findByAggregateIn(degreeAggregates);
    }

    @Override
    public ArrayList<Language> findSelectedLanguages(ArrayList<String> languageAbbreviations) {
        return langRep.findByAbbreviationIn(languageAbbreviations);
    }

    @Override
    public ArrayList<StudyForm> findSelectedStudyForms(ArrayList<String> studyFormsName) {
        return studFRepo.findByNameIn(studyFormsName);
    }

    @Override
    public ArrayList<StudyTypes> findSelectedStudyTypes(ArrayList<String> studyTypesType) {
        return studTRepo.findByTypeIn(studyTypesType);
    }

    @Override
    public Result setResultDataBesidesAnswers(Result createdResult, Map<String, Object> answers, User currentUser) {
        // TODO implement support for duration selection
        // selectedDegreesPR
        ArrayList<String> selectedDegreesPR = (ArrayList<String>) answers.get("selectedDegreesPR");
        if (!selectedDegreesPR.isEmpty()) {
            ArrayList<Degree> allowedDegrees = this.findDegreesByAggregate(selectedDegreesPR);
            createdResult.setAllowedDegrees(new HashSet<>(allowedDegrees));
        }
        // selectedLanguages
        ArrayList<String> selectedLanguages = (ArrayList<String>) answers.get("selectedLanguages");
        if (!selectedLanguages.isEmpty()) {
            ArrayList<Language> allowedLanguages = this.findSelectedLanguages(selectedLanguages);
            createdResult.setAllowedLanguages(new HashSet<>(allowedLanguages));
        }
        // selectedStudyForms
        ArrayList<String> selectedStudyForms = (ArrayList<String>) answers.get("selectedStudyForms");
        if (!selectedStudyForms.isEmpty()) {
            ArrayList<StudyForm> allowedStudyForms = this.findSelectedStudyForms(selectedStudyForms);
            createdResult.setAllowedStudyForms(new HashSet<>(allowedStudyForms));
        }
        // selectedStudyTypes
        ArrayList<String> selectedStudyTypes = (ArrayList<String>) answers.get("selectedStudyTypes");
        if (!selectedStudyTypes.isEmpty()) {
            ArrayList<StudyTypes> allowedStudyTypes = this.findSelectedStudyTypes(selectedStudyTypes);
            createdResult.setAllowedStudyTypes(new HashSet<>(allowedStudyTypes));
        }

        if (currentUser != null) {
            createdResult.setProfileInstance(currentUser.getProfileInstance());
        }
        createdResult.setDesiredOnlyFree((answers.get("currentOnlyFree")) == "true");

        if (((String) answers.get("currentMaxCost")) != "") {
            createdResult.setDesiredMaxCost(Integer.parseInt((String) answers.get("currentMaxCost")));
        }
        if (((String) answers.get("myLocation")) != "") {
            createdResult.setLocation((String) answers.get("myLocation"));
        }
        if (((String) answers.get("myAge")) != "") {
            createdResult.setAge(Integer.parseInt((String) answers.get("myAge")));
        }
        if (((String) answers.get("myInstitution")) != "") {
            createdResult.setCurrentSchool((String) answers.get("myInstitution"));
        }
        if (((String) answers.get("myGrade")) != "") {
            createdResult.setGrade((String) answers.get("myGrade"));
        }
        if (((String) answers.get("mySuggestions")) != "") {
            createdResult.setSuggestions((String) answers.get("mySuggestions"));
        }
        return createdResult;
    }

}

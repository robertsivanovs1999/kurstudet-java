package lv.kurstudet.kurstudet.services.admin.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lv.kurstudet.kurstudet.models.auth.User;
import lv.kurstudet.kurstudet.models.education.Degree;
import lv.kurstudet.kurstudet.models.education.Duration;
import lv.kurstudet.kurstudet.models.education.Faculty;
import lv.kurstudet.kurstudet.models.education.Language;
import lv.kurstudet.kurstudet.models.education.Locations;
import lv.kurstudet.kurstudet.models.education.Payment;
import lv.kurstudet.kurstudet.models.education.Programme;
import lv.kurstudet.kurstudet.models.education.ProgrammeTypes;
import lv.kurstudet.kurstudet.models.education.StudyForm;
import lv.kurstudet.kurstudet.models.education.StudyTypes;
import lv.kurstudet.kurstudet.models.education.University;
import lv.kurstudet.kurstudet.repos.auth.ILocationsRepo;
import lv.kurstudet.kurstudet.repos.education.IDegreeRepo;
import lv.kurstudet.kurstudet.repos.education.IDurationRepo;
import lv.kurstudet.kurstudet.repos.education.IFacultyRepo;
import lv.kurstudet.kurstudet.repos.education.ILanguageRepo;
import lv.kurstudet.kurstudet.repos.education.IPaymentRepo;
import lv.kurstudet.kurstudet.repos.education.IProgrammeRepo;
import lv.kurstudet.kurstudet.repos.education.IProgrammeTypesRepo;
import lv.kurstudet.kurstudet.repos.education.IStudyFormRepo;
import lv.kurstudet.kurstudet.repos.education.IStudyTypesRepo;
import lv.kurstudet.kurstudet.repos.education.IUniversityRepo;
import lv.kurstudet.kurstudet.services.admin.IAdminService;
import lv.kurstudet.kurstudet.services.auth.IUserService;

@Service
public class AdminServiceImpl implements IAdminService {
    @Autowired
    IUserService ius;

    @Autowired
    IUniversityRepo iur;

    @Autowired
    ILocationsRepo ilocr;

    @Autowired
    IProgrammeRepo iprogr;

    @Autowired
    IDegreeRepo idegr;

    @Autowired
    IFacultyRepo ifr;

    @Autowired
    IProgrammeTypesRepo iptr;

    @Autowired
    IDurationRepo idur;

    @Autowired
    IPaymentRepo ipayr;

    @Autowired
    IStudyFormRepo isfr;

    @Autowired
    ILanguageRepo ilangr;

    @Autowired
    IStudyTypesRepo istr;

    @Override
    public boolean updateUserById(Long userId, User userData) {
        if (userId > 0 && userData != null) {
            User userToUpdate = ius.getById(userId);
            if (userToUpdate != null) {
                userToUpdate.setFirstName(userData.getFirstName());
                userToUpdate.setLastName(userData.getLastName());
                userToUpdate.setEmail(userData.getEmail());
                userToUpdate.setPassword(userData.getPassword());
                userToUpdate.setSuperuser(userData.isSuperuser());
                userToUpdate.setStaff(userData.isStaff());
                userToUpdate.setActive(userData.isActive());
                userToUpdate.setProfileInstance(userData.getProfileInstance());
                return true;
            }
        }
        return false;
    }

    @Override
    public List<University> getAllUniversities() {
        return this.iur.findAll();
    }

    @Override
    public boolean addNewUniversity(String name, String address, String country, String city) {
        if (name != null && !name.isEmpty() && address != null && !address.isEmpty() && country != null
                && !country.isEmpty() && city != null && !city.isEmpty()) {
            // Create university location object
            Locations tempLocation = new Locations();
            tempLocation.setAddress(address);
            tempLocation.setCity(city);
            tempLocation.setCountry(country);
            this.addNewLocation(tempLocation);

            // Create university
            University newUni = new University();

            Locations uniLocation = this.ilocr.findByAddress(address);
            newUni.setLocation(uniLocation);
            newUni.setName(name);

            this.iur.save(newUni);
            return true;
        }
        return false;
    }

    @Override
    public boolean addNewLocation(Locations location) {
        if (location != null) {
            if (!this.ilocr.existsByAddress(location.getAddress())) {
                this.ilocr.save(location);
                return true;
            }
        }
        return false;
    }

    @Override
    public List<University> getAllUniversitiesByName(String name) {
        if (name != null && name != "") {
            return this.iur.findAllByNameContaining(name);
        }
        return null;
    }

    @Override
    public List<University> getAllUniversitiesByCity(String city) {
        if (city != null && city != "") {
            // Get all locations which address contain city
            List<Locations> allLocationsContaining = this.ilocr.findAllByAddressContaining(city);

            ArrayList<University> uniByCity = new ArrayList<>();
            // Get all universities which contain found locations
            for (Locations location : allLocationsContaining) {
                uniByCity.addAll(this.iur.findByLocation(location));
            }

            return uniByCity;
        }
        return null;
    }

    @Override
    public boolean updateProgrammeById(Long id, Programme tempProgramme) {
        if (id != null && tempProgramme != null) {
            Programme progToUpdate = this.iprogr.findById(id).get();
            if (progToUpdate != null) {
                progToUpdate.setCode(tempProgramme.getCode());
                progToUpdate.setCreditPoints(tempProgramme.getCreditPoints());
                progToUpdate.setDegree(tempProgramme.getDegree());
                progToUpdate.setDescription(tempProgramme.getDescription());
                progToUpdate.setDuration(tempProgramme.getDuration());
                progToUpdate.setEmail(tempProgramme.getEmail());
                progToUpdate.setFaculty(tempProgramme.getFaculty());
                progToUpdate.setIsActive(tempProgramme.getIsActive());
                progToUpdate.setLanguage(tempProgramme.getLanguage());
                progToUpdate.setPayment(tempProgramme.getPayment());
                progToUpdate.setPhone(tempProgramme.getPhone());
                progToUpdate.setProgramme_type(tempProgramme.getProgramme_type());
                progToUpdate.setStudyForms(tempProgramme.getStudyForms());
                progToUpdate.setStudyType(tempProgramme.getStudyType());
                progToUpdate.setTitle(tempProgramme.getTitle());
                progToUpdate.setUniversity(tempProgramme.getUniversity());
                progToUpdate.setWebsite(tempProgramme.getWebsite());
                progToUpdate.setDateUpdated(LocalDateTime.now());

                this.iprogr.save(progToUpdate);
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Degree> getAllDegrees() {
        return this.idegr.findAll();
    }

    @Override
    public List<Faculty> getAllFaculties() {
        return this.ifr.findAll();
    }

    @Override
    public List<ProgrammeTypes> getAllProgrammeTypes() {
        return this.iptr.findAll();
    }

    @Override
    public List<Duration> getAllDurations() {
        return this.idur.findAll();
    }

    @Override
    public List<Payment> getAllPayments() {
        return this.ipayr.findAll();
    }

    @Override
    public List<StudyForm> getAllStudyForms() {
        return this.isfr.findAll();
    }

    @Override
    public List<Language> getAllLanguages() {
        return this.ilangr.findAll();
    }

    @Override
    public List<StudyTypes> getAllStudyTypes() {
        return this.istr.findAll();
    }

    @Override
    public Degree getDegreeById(Long id) {
        if (id != null && id > 0) {
            return this.idegr.findById(id).get();
        }
        return null;
    }

    @Override
    public Faculty getFacultyById(Long id) {
        if (id != null && id > 0) {
            return this.ifr.findById(id).get();
        }
        return null;
    }

    @Override
    public ProgrammeTypes getProgrammeTypeById(Long id) {
        if (id != null && id > 0) {
            return this.iptr.findById(id).get();
        }
        return null;
    }

    @Override
    public University getUniversityById(Long id) {
        if (id != null && id > 0) {
            return this.iur.findById(id).get();
        }
        return null;
    }

    @Override
    public StudyTypes getStudyType(Long id) {
        if (id != null && id > 0) {
            return this.istr.findById(id).get();
        }
        return null;
    }

    @Override
    public Duration getDurationById(Long id) {
        if (id != null && id > 0) {
            return this.idur.findById(id).get();
        }
        return null;
    }

    @Override
    public Payment getPaymentById(Long id) {
        if (id != null && id > 0) {
            return this.ipayr.findById(id).get();
        }
        return null;
    }

    @Override
    public StudyForm getStudyFormsById(Long id) {
        if (id != null && id > 0) {
            return this.isfr.findById(id).get();
        }
        return null;
    }

    @Override
    public Language getLanguageById(Long id) {
        if (id != null && id > 0) {
            return this.ilangr.findById(id).get();
        }
        return null;
    }

    @Override
    public StudyTypes getStudyTypeById(Long id) {
        if (id != null && id > 0) {
            return this.istr.findById(id).get();
        }
        return null;
    }

    @Override
    public boolean addNewProgramme(Programme newProgramme) {
        if (newProgramme != null) {
            Programme programme = new Programme();
            programme.setTitle(newProgramme.getTitle());
            programme.setCode(newProgramme.getCode());
            programme.setCreditPoints(newProgramme.getCreditPoints());
            programme.setDescription(newProgramme.getDescription());
            programme.setWebsite(newProgramme.getWebsite());
            programme.setPhone(newProgramme.getPhone());
            programme.setEmail(newProgramme.getEmail());
            programme.setDegree(newProgramme.getDegree());
            programme.setFaculty(newProgramme.getFaculty());
            programme.setProgramme_type(newProgramme.getProgramme_type());
            programme.setUniversity(newProgramme.getUniversity());
            programme.setDuration(newProgramme.getDuration());
            programme.setPayment(newProgramme.getPayment());
            programme.setStudyForms(newProgramme.getStudyForms());
            programme.setLanguage(newProgramme.getLanguage());
            programme.setStudyType(newProgramme.getStudyType());
            programme.setIsActive(newProgramme.getIsActive());
            programme.setDateCreated(LocalDateTime.now());
            programme.setDateUpdated(LocalDateTime.now());

            this.iprogr.save(programme);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteProgrammeById(Long id) {
        if (id != null && id > 0) {
            if (this.iprogr.existsById(id)) {
                Programme programme = this.iprogr.findById(id).get();
                programme.deactivate();
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean updateUniversityById(Long id, University university) {
        if (id != null && id > 0 && university != null) {
            University uniToUpdate = this.iur.findById(id).get();
            if (uniToUpdate != null) {
                uniToUpdate.setName(university.getName());
                uniToUpdate.setLocation(university.getLocation());
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Locations> getAllLocations() {
        return this.ilocr.findAll();
    }

    @Override
    public boolean deleteUniversityById(Long id) {
        if (id != null && id > 0) {
            University university = this.iur.findById(id).get();
            if (university != null) {
                university.setIsActive(false);
                university.setDisabledAt(LocalDateTime.now());
                return true;
            }
        }
        return false;
    }
}
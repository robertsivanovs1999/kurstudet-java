package lv.kurstudet.kurstudet.services.admin;

import java.util.List;

import lv.kurstudet.kurstudet.models.auth.User;
import lv.kurstudet.kurstudet.models.education.Degree;
import lv.kurstudet.kurstudet.models.education.Duration;
import lv.kurstudet.kurstudet.models.education.Faculty;
import lv.kurstudet.kurstudet.models.education.Language;
import lv.kurstudet.kurstudet.models.education.Locations;
import lv.kurstudet.kurstudet.models.education.Payment;
import lv.kurstudet.kurstudet.models.education.Programme;
import lv.kurstudet.kurstudet.models.education.ProgrammeTypes;
import lv.kurstudet.kurstudet.models.education.StudyForm;
import lv.kurstudet.kurstudet.models.education.StudyTypes;
import lv.kurstudet.kurstudet.models.education.University;

public interface IAdminService {
    boolean updateUserById(Long userId, User userData);

    boolean addNewLocation(Locations location);

    List<University> getAllUniversities();

    List<Degree> getAllDegrees();

    List<Faculty> getAllFaculties();

    List<ProgrammeTypes> getAllProgrammeTypes();

    List<Duration> getAllDurations();

    List<Payment> getAllPayments();

    List<StudyForm> getAllStudyForms();

    List<Language> getAllLanguages();

    List<StudyTypes> getAllStudyTypes();

    List<Locations> getAllLocations();

    Degree getDegreeById(Long id);

    Faculty getFacultyById(Long id);

    ProgrammeTypes getProgrammeTypeById(Long id);

    University getUniversityById(Long id);

    Duration getDurationById(Long id);

    Payment getPaymentById(Long id);

    StudyForm getStudyFormsById(Long id);

    Language getLanguageById(Long id);

    StudyTypes getStudyTypeById(Long id);

    StudyTypes getStudyType(Long id);

    List<University> getAllUniversitiesByName(String name);

    List<University> getAllUniversitiesByCity(String city);

    boolean addNewProgramme(Programme newProgramme);

    boolean addNewUniversity(String name, String address, String country, String city);

    boolean updateProgrammeById(Long id, Programme tempProgramme);

    boolean deleteProgrammeById(Long id);

    boolean deleteUniversityById(Long id);

    boolean updateUniversityById(Long id, University university);
}
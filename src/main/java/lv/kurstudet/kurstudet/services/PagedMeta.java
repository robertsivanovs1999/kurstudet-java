package lv.kurstudet.kurstudet.services;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lv.kurstudet.kurstudet.models.ISendOverWire;
import lv.kurstudet.kurstudet.models.SingleJSONableObj;

@Getter
@Setter
@NoArgsConstructor
public class PagedMeta<T extends ISendOverWire> extends SingleJSONableObj {

    private Integer numberOfPages;
    private Integer totalCount;
    private Integer currentPage;
    private Integer perPage;
    private List<T> items;

    @Override
    public JSONObject constructBottomLayer() {
        return null;
    }

    @Override
    public JSONObject constructJSONObject() {
        JSONArray dataLayer = this.constructDataLayer();
        JSONObject pageLayer = this.constructPageLayer();

        JSONObject bottomLayer = new JSONObject();
        bottomLayer.put("data", dataLayer.toString());
        bottomLayer.put("page", pageLayer);
        return bottomLayer;
    }

    public JSONArray constructDataLayer() {
        ArrayList<JSONObject> itemsAsStrings = new ArrayList<>();
        this.items.forEach(i -> itemsAsStrings.add(i.constructMiddleLayer()));
        JSONArray itemsAsStringJSON = new JSONArray(itemsAsStrings);
        return itemsAsStringJSON;
    }

    public JSONObject constructPageLayer() {
        JSONObject pageObj = new JSONObject();
        pageObj.put("num_pages", this.numberOfPages);
        pageObj.put("total_count", this.totalCount);
        pageObj.put("current_page", this.currentPage);
        pageObj.put("per_page", this.perPage);
        return pageObj;
    }

    public PagedMeta(List<T> items, Integer numberOfPages, Integer totalCount, Integer currentPage,
            Integer perPage) {
        this.items = items;
        this.numberOfPages = numberOfPages;
        this.totalCount = totalCount;
        this.currentPage = currentPage;
        this.perPage = perPage;
    }

    @Override
    public Long getId() {
        return null;
    }


    @Override
    public String toString() {
        return "{" +
            " numberOfPages='" + numberOfPages + "'" +
            ", totalCount='" + totalCount + "'" +
            ", currentPage='" + currentPage + "'" +
            ", perPage='" + perPage + "'" +
            ", items='" + items + "'" +
            "}";
    }


}

/**
 *
 * This code snippet was copied from
 * https://github.com/JolaPsh/rest-with-contacts-filter/blob/fe498294dc2b71d090d3e89a3ab938471e769dd4/src/main/java/top/contacts/CustomTomcatContainer.java
 * And altered to fit our needs.
 */

package lv.kurstudet.kurstudet;

import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.stereotype.Component;

/**
 * Created by Joanna Pakosh on лист, 2018
 */

/**
 * If an application needs more special characters which is not supported by
 * tomcat by default then we need to add special characters in relaxedQueryChars
 * attribute, see RFC 7230 and RFC 3986.
 * https://stackoverflow.com/questions/51703746/setting-relaxedquerychars-for-embedded-tomcat
 * https://stackoverflow.com/questions/44011774/how-can-i-make-tomcat-accept-unescaped-brackets-in-urls/44027211
 */
@Component
public class CustomTomcatContainer implements WebServerFactoryCustomizer<TomcatServletWebServerFactory> {

    @Override
    public void customize(TomcatServletWebServerFactory factory) {
        factory.addConnectorCustomizers(connector -> connector.setAttribute("relaxedQueryChars", "[]"));
    }
}

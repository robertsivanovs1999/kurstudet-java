package lv.kurstudet.kurstudet.controllers.profile;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lv.kurstudet.kurstudet.models.auth.User;
import lv.kurstudet.kurstudet.services.auth.IUserService;

@Controller
@RequestMapping("/profile")
public class ProfileController {
    @Autowired
    IUserService usi;

    @GetMapping("")
    public String showProfileGet(Model model, HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            User user = (User) session.getAttribute("currentUser");
            model.addAttribute("username", user.getUsername());
            model.addAttribute("uEmail", user.getEmail());
            model.addAttribute("uFirstName", user.getFirstName());
            model.addAttribute("uLastName", user.getLastName());
            model.addAttribute("uDateJoined", user.getDateJoined());
            model.addAttribute("user", user);
            return "profile";
        }
        System.out.println("Error");
        return "index";
    }

    @PostMapping("")
    public String showProfilePost(Model model, HttpSession session, User user,
            @RequestParam Map<String, String> userParam) {
        if (session.getAttribute("currentUser") != null) {
            Long userId = this.usi.getByUsername(userParam.get("username")).getId();
            usi.updateUserById(userId, user);

            return "redirect:/profile";
        }
        return "redirect:/log-in/";
    }

    @GetMapping("/password-update")
    public String updatePasswordGet(Model model, HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            User user = usi.getByUsername(((User) (session.getAttribute("currentUser"))).getUsername());
            model.addAttribute("user", user);
            return "password-update";
        }
        return "redirect:/log-in/";
    }

    @PostMapping("/password-update")
    public String updatePasswordPost(Model model, HttpSession session, @RequestParam Map<String, String> userParam) {
        if (session.getAttribute("currentUser") != null) {
            Long userId = this.usi.getByUsername(userParam.get("username")).getId();
            String userPass = userParam.get("password");

            if (userParam.get("newPassword").equals(userParam.get("newPassword2"))) {
                if (usi.updatePasswordById(userId, userPass, userParam.get("newPassword"))) {
                    return "redirect:/profile";
                }
            }
        }
        return "redirect:/log-in/";
    }
}
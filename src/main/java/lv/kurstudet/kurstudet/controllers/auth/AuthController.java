package lv.kurstudet.kurstudet.controllers.auth;

import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lv.kurstudet.kurstudet.models.auth.User;
import lv.kurstudet.kurstudet.services.auth.IUserService;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    IUserService usi;

    @GetMapping("/fetch-current-user")
    public String fetchCurrentUser(HttpSession session) {

        if (session.getAttribute("currentUser") != null) {
            User u = (User) session.getAttribute("currentUser");
            return u.constructJSONObject().toString();
        }
        JSONObject empty = new JSONObject();
        empty.put("data", "{}");
        return empty.toString();
    }

    @PostMapping("/log-in")
    public ResponseEntity<String> authenticate(HttpSession session, @RequestBody Map<String, String> user) {

        try {
            boolean valid = this.usi.authenticate(user.get("username"), user.get("password"));
            if (valid) {
                User authUser = this.usi.getByUsername(user.get("username"));
                JSONObject response = authUser.getProfileInstance().constructJSONObject();
                response.put("status", "logged in");

                session.setAttribute("currentUser", authUser);
                return new ResponseEntity<>(response.toString(), HttpStatus.OK);
            }
        } catch (NullPointerException e) {
            // NOTE: The user has sent empty response
        }

        JSONObject response = new JSONObject();
        response.put("status", "Could not authenticate");
        return new ResponseEntity<>(response.toString(), HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/register")
    public ResponseEntity<String> register(HttpSession session, @RequestBody Map<String, String> userToRegister) {

        JSONArray errorsJSON = new JSONArray();

        try {
            if (!userToRegister.get("password1").equals(userToRegister.get("password2"))) {
                JSONArray tempError = new JSONArray();
                tempError.put("password");
                tempError.put("password do not match");
                errorsJSON.put(tempError);
            }
            if (usi.existsByUsername(userToRegister.get("username"))) {
                JSONArray tempError = new JSONArray();
                tempError.put("username");
                tempError.put("username exists");
                errorsJSON.put(tempError);
            }
            if (usi.existsByEmail(userToRegister.get("email"))) {
                JSONArray tempError = new JSONArray();
                tempError.put("email");
                tempError.put("email taken");
                errorsJSON.put(tempError);
            }

            // https://emailregex.com/
            String regex = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(userToRegister.get("email"));
            if (!matcher.matches()) {
                JSONArray tempError = new JSONArray();
                tempError.put("email");
                tempError.put("invalid email");
                errorsJSON.put(tempError);
            }

        } catch (NullPointerException e) {
            // NOTE: The user has sent empty response
        }
        if (errorsJSON.isEmpty()) {
            User user = User.baseUser(userToRegister.get("username"), userToRegister.get("password1"),
                    userToRegister.get("email"));
            usi.register(user);
            User authUser = this.usi.getByUsername(userToRegister.get("username"));
            JSONObject response = authUser.getProfileInstance().constructJSONObject();
            response.put("status", "user created successfully");
            return new ResponseEntity<>(response.toString(), HttpStatus.OK);
        } else {
            JSONObject outerShell = new JSONObject();
            outerShell.put("errors", errorsJSON);
            return new ResponseEntity<>(outerShell.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/log-out")
    public String logOut(HttpSession session) {

        if (session.getAttribute("currentUser") != null) {
            session.setAttribute("currentUser", null);
        }
        JSONObject response = new JSONObject();
        response.put("success", true);
        return response.toString();
    }
}

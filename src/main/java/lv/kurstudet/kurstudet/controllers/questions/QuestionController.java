package lv.kurstudet.kurstudet.controllers.questions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.qos.logback.core.util.Duration;
import lv.kurstudet.kurstudet.models.auth.User;
import lv.kurstudet.kurstudet.models.education.Degree;
import lv.kurstudet.kurstudet.models.education.Language;
import lv.kurstudet.kurstudet.models.education.Payment;
import lv.kurstudet.kurstudet.models.education.Programme;
import lv.kurstudet.kurstudet.models.education.StudyForm;
import lv.kurstudet.kurstudet.models.education.StudyTypes;
import lv.kurstudet.kurstudet.models.questions.Answer;
import lv.kurstudet.kurstudet.models.questions.EvaluatedParameters;
import lv.kurstudet.kurstudet.models.questions.ParameterTags;
import lv.kurstudet.kurstudet.models.questions.ProgrammeParameterMap;
import lv.kurstudet.kurstudet.models.questions.QuestionSet;
import lv.kurstudet.kurstudet.models.questions.QuestionSetAndQuestionMap;
import lv.kurstudet.kurstudet.models.questions.Result;
import lv.kurstudet.kurstudet.services.PagedMeta;
import lv.kurstudet.kurstudet.services.education.ICardService;
import lv.kurstudet.kurstudet.services.questions.IQuestionService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/api")
public class QuestionController {

    @Autowired
    IQuestionService qs;

    final static Integer pageSize = 20;

    @GetMapping("/get-question-set")
    public String fetchCurrentUser(@RequestParam Map<String, String> queryParameters) {
        JSONObject queryObj = new JSONObject(queryParameters);
        QuestionSet selectedQs = qs.getQuestionSetByName((String) queryObj.get("QuestionSet"));
        PagedMeta<QuestionSetAndQuestionMap> page = qs.fetchQuestions(selectedQs,
                Integer.parseInt(queryParameters.get("page")), pageSize);
        return page.constructJSONObject().toString();
    }

    // NOTE: This endpoint creates the result, instead of getting it as the name would imply.
    @PostMapping(value = "/get-result")
    public String createResult(HttpSession session, @RequestBody Map<String, Object> answers) {

        // Create a result object
        Result createdResult = Result.baseResult();
        createdResult = qs.setResultDataBesidesAnswers(createdResult, answers,
                (User) session.getAttribute("currentUser"));

        createdResult = qs.createResult(createdResult);

        // Iterate over "answers" and create answer objects
        for (LinkedHashMap<String, Object> answerIter : (ArrayList<LinkedHashMap<String, Object>>) answers
                .get("answers")) {
            JSONObject iterAnswerJsonObj = new JSONObject(answerIter);
            Answer iter = qs.saveAnswer(new Answer(iterAnswerJsonObj.getInt("answer_weight"),
                    qs.getQmapById(Long.parseLong((iterAnswerJsonObj.getString("questionsetmap")))), createdResult));
        }

        // Calculate result
        qs.calculateResult(qs.findResultById(createdResult.getId()));

        // return result id
        JSONObject responseObj = new JSONObject();
        JSONObject responseObjOuter = new JSONObject();
        responseObj.put("resultId", createdResult.getId());
        responseObjOuter.put("data", responseObj);

        return responseObjOuter.toString();
    }

    @GetMapping("/get-evaluated-results")
    public String getEvaluatedResults(@RequestParam Map<String, String> queryParameters) {
        // get result object
        Result res = qs.findResultById(Long.parseLong(queryParameters.get("result")));

        // get result evaluated parameters, sort them value max -> min
        ArrayList<EvaluatedParameters> resultsByPriority = new ArrayList<>(res.getAllEvaluatedParameters());
        resultsByPriority.sort(Comparator.comparing(EvaluatedParameters::getValue));

        // -----------------------------
        // TODO check out if we are able to use the Ctieria API for this
        // https://www.baeldung.com/hibernate-criteria-queries
        // -----------------------------

        ArrayList<Programme> programmes = qs.getAllProgrammes();
        // perform filtering by:
        // - study type
        if (!res.getAllowedStudyTypes().isEmpty()) {
            programmes.removeIf(i -> i.getStudyType().retainAll(res.getAllowedStudyTypes()));
        }
        // - study form
        if (!res.getAllowedStudyForms().isEmpty()) {
            programmes.removeIf(i -> i.getStudyForms().retainAll(res.getAllowedStudyForms()));
        }
        // - desired language
        if (!res.getAllowedLanguages().isEmpty()) {
            programmes.removeIf(i -> i.getLanguage().retainAll(res.getAllowedLanguages()));
        }
        // - desired degree
        if (!res.getAllowedDegrees().isEmpty()) {
            programmes.removeIf(i -> !res.getAllowedDegrees().contains(i.getDegree()));
        }
        // - free/non-free
        if (res.getDesiredOnlyFree() != null) {

            ArrayList<Payment> allowedPayment;
            if (res.getDesiredOnlyFree()) {
                allowedPayment = qs.getFreePayment();
            } else {
                allowedPayment = qs.getNonFreePayment();
            }
            programmes.removeIf(prog -> {
                boolean mustBeRemoved = true;
                for (Payment pay : prog.getPayment()) {
                    if (mustBeRemoved) {
                        mustBeRemoved = allowedPayment.contains(pay);
                    }
                }
                return mustBeRemoved;
            });
        }

        // -----------------------------
        // perform parameter calculations
        JSONObject allProgrammeMap = new JSONObject();
        for (EvaluatedParameters result : resultsByPriority) {
            ArrayList<ProgrammeParameterMap> ppm = qs.getProgrammeParameterMap(result.getParameter(), programmes);
            for (ProgrammeParameterMap ppmIter : ppm) {
                Float calc = Math.abs((float) (result.getValue()) - (float) (ppmIter.getScore())) / 2;
                JSONObject iterRes = new JSONObject();
                ParameterTags maxParamTag = ppmIter.getParameter().getTags().stream()
                        .max(Comparator.comparing(i -> ((ParameterTags) i).getId())).get();
                iterRes.put("tag", maxParamTag.getTag());
                iterRes.put("parameter_abbr", ppmIter.getParameter().getAbbreviation());
                iterRes.put("parameter_name", ppmIter.getParameter().getExplanation());
                iterRes.put("parameter_eval_score", 100 - calc);

                String idAsString = String.valueOf(ppmIter.getProgramme().getId());
                JSONObject iterResSmall = new JSONObject();
                if (!allProgrammeMap.has(idAsString)) {
                    JSONArray iterResArray = new JSONArray();
                    iterResArray.put(iterRes);

                    iterResSmall.put("score", calc);
                    iterResSmall.put("item_cnt", 1);
                    iterResSmall.put("results", iterRes);

                    allProgrammeMap.put(idAsString, iterResSmall);
                } else {
                    allProgrammeMap.getJSONObject(idAsString).put("score",
                            allProgrammeMap.getJSONObject(idAsString).getFloat("score") + calc);
                    allProgrammeMap.getJSONObject(idAsString).put("item_cnt",
                            allProgrammeMap.getJSONObject(idAsString).getInt("item_cnt") + 1);
                    allProgrammeMap.getJSONObject(idAsString).accumulate("results", iterRes);
                }
            }
        }

        // Find the most matching programme that fits the closest to each parameter
        // param_object_avg
        ArrayList<JSONObject> resultParameters = new ArrayList<>();
        allProgrammeMap.keys().forEachRemaining(i -> {
            JSONObject obj = new JSONObject();
            obj.put("programme", (String) i);
            obj.put("val", allProgrammeMap.get(i));
            resultParameters.add(obj);
        });
        resultParameters.sort((JSONObject o1, JSONObject o2) -> {
            return ((Float) o1.getJSONObject("val").getFloat("score"))
                    .compareTo((Float) o2.getJSONObject("val").getFloat("score"));
        });

        JSONArray bestFiveResults = new JSONArray();
        for (int j = 0; j < resultParameters.size(); j++) {
            if (j >= 5)
                break;

            JSONObject updatedItem = resultParameters.get(j);
            updatedItem.getJSONObject("val").put("score", 100 - updatedItem.getJSONObject("val").getFloat("score")
                    / updatedItem.getJSONObject("val").getInt("item_cnt"));

            updatedItem.put("programme", qs.getProgrammeById(updatedItem.getLong("programme")).constructMiddleLayer());
            bestFiveResults.put(updatedItem);
        }
        JSONObject resultResponse = new JSONObject();
        resultResponse.put("data", bestFiveResults);
        resultResponse.put("resultId", res.getId());
        JSONObject topLevel = new JSONObject();
        topLevel.put("data", resultResponse);
        return topLevel.toString();
    }

    @PostMapping(value = "/update-result")
    public ResponseEntity<String> postUpdateResult(@RequestBody Map<String, Object> queryParameters, HttpSession session) {
        Result resultToUpdate = qs.findResultById(Long.parseLong((String) queryParameters.get("result")));

        if (resultToUpdate.getProfileInstance() == null) {
            return new ResponseEntity<>("{}", HttpStatus.BAD_REQUEST);
        } else if (!resultToUpdate.getProfileInstance().getUser().equals((User) session.getAttribute("currentUser"))) {
            return new ResponseEntity<>("{}", HttpStatus.BAD_REQUEST);
        }
        resultToUpdate = qs.setResultDataBesidesAnswers(resultToUpdate,
                queryParameters,
                (User) session.getAttribute("currentUser"));

        resultToUpdate = qs.createResult(resultToUpdate);

        JSONObject innerLevel = new JSONObject();
        innerLevel.put("resultId", resultToUpdate.getId());
        JSONObject topLevel = new JSONObject();
        topLevel.put("data", innerLevel);

        return new ResponseEntity<>(topLevel.toString(), HttpStatus.ACCEPTED);
    }

}

package lv.kurstudet.kurstudet.controllers.education;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import lv.kurstudet.kurstudet.models.education.Degree;
import lv.kurstudet.kurstudet.models.education.Duration;
import lv.kurstudet.kurstudet.models.education.Language;
import lv.kurstudet.kurstudet.models.education.Payment;
import lv.kurstudet.kurstudet.models.education.Programme;
import lv.kurstudet.kurstudet.models.education.StudyForm;
import lv.kurstudet.kurstudet.services.PagedMeta;
import lv.kurstudet.kurstudet.services.education.ICardService;

@RestController
@RequestMapping("/api")
public class CardController {
    @Autowired
    ICardService cardService;

    @GetMapping("/get-university")
    public String getUniversity() {
        JSONObject uniData = new JSONObject();
        JSONArray uniArray = new JSONArray();
        cardService.getAllUniversities().forEach(i -> uniArray.put(i.constructMiddleLayer()));
        uniData.put("data", uniArray.toString());
        return uniData.toString();
    }

    @GetMapping("/get-programme-name-aggregate")
    public String getProgrammeNameAggregate() {
        ArrayList<HashMap<String, Object>> uniqueProgrammes = cardService.getAllProgrammesAggregate();
        JSONArray result = new JSONArray();
        uniqueProgrammes.forEach(i -> {
            JSONObject innerObjIter = new JSONObject();
            innerObjIter.put("title", (String) i.get("title"));
            innerObjIter.put("repeats", (Integer) i.get("repeats"));
            result.put(innerObjIter);
        });
        JSONObject outerObjIter = new JSONObject();
        outerObjIter.put("data", result);
        return outerObjIter.toString();
    }

    @GetMapping("/get-payment")
    public String getPayments() {
        ArrayList<Payment> payments = cardService.getAllPayments();
        JSONArray dataArray = new JSONArray();
        JSONObject outerObjIter = new JSONObject();
        payments.forEach(i -> dataArray.put(i.constructMiddleLayer()));
        outerObjIter.put("data", dataArray.toString());
        return outerObjIter.toString();
    }

    @GetMapping("/get-study-form-aggregate")
    public String getStudyFormAggregate() {
        ArrayList<HashMap<String, Object>> payments = cardService.getStudyFormAggregate();
        JSONArray result = new JSONArray();
        payments.forEach(i -> {
            JSONObject innerObjIter = new JSONObject();
            innerObjIter.put("name", (String) i.get("name"));
            innerObjIter.put("repeats", (Integer) i.get("repeats"));
            result.put(innerObjIter);
        });
        JSONObject outerObjIter = new JSONObject();
        outerObjIter.put("data", result);
        return outerObjIter.toString();
    }

    @GetMapping("/get-study-type-aggregate")
    public String getStudyTypeAggregate() {
        ArrayList<HashMap<String, Object>> payments = cardService.getStudyTypeAggregate();
        JSONArray result = new JSONArray();
        payments.forEach(i -> {
            JSONObject innerObjIter = new JSONObject();
            innerObjIter.put("type", (String) i.get("type"));
            innerObjIter.put("repeats", (Integer) i.get("repeats"));
            result.put(innerObjIter);
        });
        JSONObject outerObjIter = new JSONObject();
        outerObjIter.put("data", result);
        return outerObjIter.toString();
    }

    @GetMapping("/get-programme-duration-aggregate")
    public String getProgrammeDurationAggregate() {
        ArrayList<Duration> durations = cardService.getALlDurations();
        JSONArray result = new JSONArray();
        durations.forEach(i -> {
            JSONObject innerObjIter = new JSONObject();
            innerObjIter.put("pk", i.getId());
            innerObjIter.put("length", i.getLength());
            result.put(innerObjIter);
        });
        JSONObject outerObjIter = new JSONObject();
        outerObjIter.put("data", result);
        return outerObjIter.toString();
    }

    @GetMapping("/get-degree")
    public String getDegree() {
        ArrayList<Degree> degrees = cardService.getAllDegrees();
        JSONArray result = new JSONArray();
        HashSet<String> addedResultTypes = new HashSet<>();
        degrees.forEach(i -> {
            if (!addedResultTypes.contains(i.getAggregate())) {
                result.put(i.getAggregate());
                addedResultTypes.add(i.getAggregate());
            }
        });
        JSONObject outerObjIter = new JSONObject();
        outerObjIter.put("data", result);
        return outerObjIter.toString();
    }

    @GetMapping("/get-language")
    public String getLanguage() {
        JSONObject langData = new JSONObject();
        JSONArray langArray = new JSONArray();
        cardService.getAllLanguages().forEach(i -> langArray.put(i.constructMiddleLayer()));
        langData.put("data", langArray.toString());
        return langData.toString();
    }

    @GetMapping("/get-programme")
    public String getProgramme(@RequestParam Map<String, Object> queryParams) {
        PagedMeta<Programme> programmes = cardService.fetchProgrammes(
                queryParams, Integer.parseInt((String) queryParams.get("page")), Integer.parseInt((String) queryParams.get("count")));
        return programmes.constructJSONObject().toString();
    }
}

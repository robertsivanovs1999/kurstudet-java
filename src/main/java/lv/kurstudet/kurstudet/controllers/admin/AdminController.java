package lv.kurstudet.kurstudet.controllers.admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lv.kurstudet.kurstudet.models.auth.User;
import lv.kurstudet.kurstudet.models.education.Degree;
import lv.kurstudet.kurstudet.models.education.Duration;
import lv.kurstudet.kurstudet.models.education.Faculty;
import lv.kurstudet.kurstudet.models.education.Language;
import lv.kurstudet.kurstudet.models.education.Locations;
import lv.kurstudet.kurstudet.models.education.Payment;
import lv.kurstudet.kurstudet.models.education.Programme;
import lv.kurstudet.kurstudet.models.education.ProgrammeTypes;
import lv.kurstudet.kurstudet.models.education.StudyForm;
import lv.kurstudet.kurstudet.models.education.StudyTypes;
import lv.kurstudet.kurstudet.models.education.University;
import lv.kurstudet.kurstudet.repos.education.IProgrammeRepo;
import lv.kurstudet.kurstudet.services.admin.IAdminService;
import lv.kurstudet.kurstudet.services.auth.IUserService;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    IUserService usi;

    @Autowired
    IAdminService asi;

    @Autowired
    IProgrammeRepo pri;

    @GetMapping("")
    public String setAdminPanel(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            User currentUser = (User) session.getAttribute("currentUser");
            if (currentUser.isSuperuser()) {
                return "admin-panel";
            }
            return "index";
        }
        return "redirect:/log-in/";
    }

    @GetMapping("/new-user")
    public String addNewUserGet(HttpSession session, Model model) {
        if (session.getAttribute("currentUser") != null) {
            User currentUser = (User) session.getAttribute("currentUser");
            if (currentUser.isSuperuser()) {
                User user = new User();
                model.addAttribute("user", user);
                return "user-add-new";
            }
            return "index";
        }
        return "redirect:/log-in/";
    }

    @PostMapping("/new-user")
    public String addNewUserPost(HttpSession session, Model model, User newUser,
            @RequestParam Map<String, String> newUserParam) {
        if (session.getAttribute("currentUser") != null) {

            User currentUser = (User) session.getAttribute("currentUser");
            if (currentUser.isSuperuser()) {

                if (!usi.existsByUsername(newUserParam.get("username"))) {
                    if (newUserParam.get("password").equals(newUserParam.get("password2"))) {
                        User user = User.baseUser(newUser.getUsername(), newUser.getPassword(), newUser.getEmail());

                        if (usi.register(user)) {
                            User theUser = usi.getByUsername(newUserParam.get("username"));
                            theUser.setFirstName(newUserParam.get("firstName"));
                            theUser.setLastName(newUserParam.get("lastName"));

                            if (newUserParam.get("isStaff") != null && newUserParam.get("isStaff").equals("on")) {
                                theUser.setStaff(true);
                            }
                            if (newUserParam.get("isSuperuser") != null
                                    && newUserParam.get("isSuperuser").equals("on")) {
                                theUser.setSuperuser(true);
                            }

                            if (usi.updateUserById(theUser.getId(), theUser)) {
                                return "redirect:/admin/all-user";
                            }
                        }
                    }
                }
            }
            return "index";
        }
        return "redirect:/log-in/";
    }

    @GetMapping("/new-university")
    public String createNewUniversityGet(HttpSession session, Model model) {
        if (session.getAttribute("currentUser") != null) {
            User currentUser = (User) session.getAttribute("currentUser");
            if (currentUser.isSuperuser()) {
                model.addAttribute("university", new University());
                return "university-add-new";
            }
            return "index";
        }
        return "redirect:/log-in/";
    }

    @PostMapping("/new-university")
    public String createNewUniversityPost(HttpSession session, Model model,
            @RequestParam Map<String, String> universityParams) {
        if (session.getAttribute("currentUser") != null) {
            User currentUser = (User) session.getAttribute("currentUser");
            if (currentUser.isSuperuser()) {
                String uniName = universityParams.get("name");
                String uniAddress = universityParams.get("address");
                String uniCountry = universityParams.get("country");
                String uniCity = universityParams.get("city");

                if (this.asi.addNewUniversity(uniName, uniAddress, uniCountry, uniCity)) {
                    return "redirect:/admin/all-universities";
                } else {
                    return "university-add-new";
                }
            }
            return "index";
        }
        return "redirect:/log-in/";
    }

    @GetMapping("/user-update/{id}")
    public String updateUserByIdGet(HttpSession session, Model model, @PathVariable(name = "id") Long id) {
        if (session.getAttribute("currentUser") != null) {
            User user = (User) session.getAttribute("currentUser");
            if (user.isSuperuser()) {
                User userToUpdate = usi.getById(id);
                model.addAttribute("user", userToUpdate);
                return "user-update";
            }
            return "index";
        }
        return "redirect:/log-in/";
    }

    @PostMapping("/user-update")
    public String updateUserByIdPost(HttpSession session, Model model,
            @RequestParam Map<String, String> updateUserParam) {
        if (session.getAttribute("currentUser") != null) {
            User user = (User) session.getAttribute("currentUser");
            if (user.isSuperuser()) {
                User userToUpdate = usi.getByUsername(updateUserParam.get("username"));

                userToUpdate.setFirstName(updateUserParam.get("firstName"));
                userToUpdate.setLastName(updateUserParam.get("lastName"));
                userToUpdate.setEmail(updateUserParam.get("email"));

                if (updateUserParam.get("isStaff") != null && updateUserParam.get("isStaff").equals("on")) {
                    userToUpdate.setStaff(true);
                } else {
                    userToUpdate.setStaff(false);
                }
                if (updateUserParam.get("isSuperuser") != null && updateUserParam.get("isSuperuser").equals("on")) {
                    userToUpdate.setSuperuser(true);
                } else {
                    userToUpdate.setSuperuser(false);
                }
                if (updateUserParam.get("isActive") != null && updateUserParam.get("isActive").equals("on")) {
                    userToUpdate.setActive(true);
                } else {
                    userToUpdate.setActive(false);
                }

                if (usi.updateUserById(userToUpdate.getId(), userToUpdate)) {
                    model.addAttribute("allUsers", usi.getAllUsers());
                    return "redirect:/admin/all-user";
                }
            }
            return "index";
        }
        return "redirect:/log-in/";
    }

    @GetMapping("/university-update/{id}")
    public String updateUniversityByIdGet(HttpSession session, Model model, @PathVariable(name = "id") Long id) {
        if (session.getAttribute("currentUser") != null) {
            User currentUser = (User) session.getAttribute("currentUser");
            if (currentUser.isSuperuser()) {
                University university = this.asi.getUniversityById(id);
                if (university != null) {
                    List<Locations> locations = this.asi.getAllLocations();
                    model.addAttribute("allLocations", locations);
                    model.addAttribute("university", university);
                    return "university-update";
                }
            }
            return "index";
        }
        return "redirect:/log-in/";
    }

    @PostMapping("/university-update")
    public String updateUniversityByIdPost(HttpSession session, @RequestParam Map<String, String> updateUniversityParam,
            University university) {
        if (session.getAttribute("currentUser") != null) {
            User user = (User) session.getAttribute("currentUser");
            if (user.isSuperuser()) {
                if (university != null) {
                    if (updateUniversityParam.get("isActive") != null) {
                        if (updateUniversityParam.get("isActive").equals("on")) {
                            university.setIsActive(true);
                        } else {
                            university.setIsActive(false);
                        }
                    }

                    Long uniId = Long.parseLong(updateUniversityParam.get("id"));
                    if (this.asi.updateUniversityById(uniId, university)) {
                        return "redirect:/admin/all-universities";
                    } else {
                        return "redirect:/admin/university-update/" + uniId;
                    }
                }
            }
            return "index";
        }
        return "redirect:/log-in/";
    }

    @GetMapping("/user-delete/{id}")
    public String deleteUserById(HttpSession session, Model model, @PathVariable(name = "id") Long id) {
        if (session.getAttribute("currentUser") != null) {
            User user = (User) session.getAttribute("currentUser");
            if (user.isSuperuser()) {
                if (this.usi.deleteUserById(id)) {
                    model.addAttribute("allUsers", usi.getAllUsers());
                    return "user-all";
                }
            }
            return "index";
        }
        return "redirect:/log-in/";
    }

    @GetMapping("/university-delete/{id}")
    public String deleteUniversityById(HttpSession session, Model model, @PathVariable(name = "id") Long id) {
        if (session.getAttribute("currentUser") != null) {
            User user = (User) session.getAttribute("currentUser");
            if (user.isSuperuser()) {
                if (this.asi.deleteUniversityById(id)) {
                    ArrayList<University> allUniversities = (ArrayList<University>) this.asi.getAllUniversities();
                    model.addAttribute("universities", allUniversities);
                    return "university-all";
                }
            }
            return "index";
        }
        return "redirect:/log-in/";
    }

    @GetMapping("/all-user")
    public String showAllUsers(HttpSession session, @RequestParam Map<String, String> params, Model model) {
        if (session.getAttribute("currentUser") != null) {
            User currentUser = (User) session.getAttribute("currentUser");
            if (currentUser.isSuperuser()) {
                String username = params.get("username");
                String email = params.get("email");
                String lastName = params.get("lastName");

                boolean isActive = false;
                boolean isStaff = false;
                boolean isSuperuser = false;

                if (params.get("isActive") != null && params.get("isActive").equals("on")) {
                    isActive = true;
                }
                if (params.get("isStaff") != null && params.get("isStaff").equals("on")) {
                    isStaff = true;
                }
                if (params.get("isSuperuser") != null && params.get("isSuperuser").equals("on")) {
                    isSuperuser = true;
                }

                ArrayList<User> userList = new ArrayList<>();

                if ((username != null || email != null || lastName != null) && (isActive || isStaff || isSuperuser)) {
                    ArrayList<User> tempList = new ArrayList<>();

                    tempList = (ArrayList<User>) this.usi.getAllByTextParams(username, lastName, email);
                    userList = (ArrayList<User>) this.usi.getAllByIsActiveAndIsStaffAndIsSuperuser(isActive, isStaff,
                            isSuperuser);
                    ArrayList<User> usersToRemove = new ArrayList<>();

                    // Remove users that do not match
                    for (User user : userList) {
                        if (!tempList.contains(user)) {
                            usersToRemove.add(user);
                        }
                    }
                    userList.removeAll(usersToRemove);

                    model.addAttribute("allUsers", userList);
                } else if (username != null || email != null || lastName != null) {
                    userList = (ArrayList<User>) this.usi.getAllByTextParams(username, lastName, email);
                    model.addAttribute("allUsers", userList);
                } else {
                    userList = (ArrayList<User>) this.usi.getAllByIsActiveAndIsStaffAndIsSuperuser(isActive, isStaff,
                            isSuperuser);
                    model.addAttribute("allUsers", userList);
                }

                return "user-all";
            }
            return "index";
        }
        return "redirect:/log-in/";
    }

    @GetMapping("/all-universities")
    public String showAllUniversities(HttpSession session, Model model, @RequestParam Map<String, String> params) {
        if (session.getAttribute("currentUser") != null) {
            User currentUser = (User) session.getAttribute("currentUser");
            if (currentUser.isSuperuser()) {
                String uniName = params.get("name");
                String uniCity = params.get("city");

                if ((uniName != null && !uniName.equals("")) || (uniCity != null && !uniCity.equals(""))) {
                    // Find by city
                    if (!uniCity.equals("") && uniName.equals("")) {
                        ArrayList<University> unisByCity = (ArrayList<University>) this.asi
                                .getAllUniversitiesByCity(uniCity);
                        if (unisByCity != null) {
                            model.addAttribute("universities", unisByCity);
                        }
                    }
                    // Find by name
                    else if (uniCity.equals("") && !uniName.equals("")) {
                        ArrayList<University> unisByName = (ArrayList<University>) this.asi
                                .getAllUniversitiesByName(uniName);
                        if (unisByName != null) {
                            model.addAttribute("universities", unisByName);
                        }
                    }
                    // Find by name and city
                    else {
                        ArrayList<University> unisByName = (ArrayList<University>) this.asi
                                .getAllUniversitiesByName(uniName);
                        ArrayList<University> unisByCity = (ArrayList<University>) this.asi
                                .getAllUniversitiesByCity(uniCity);

                        ArrayList<University> unisToRemove = new ArrayList<>();

                        // Remove users that do not match
                        for (University university : unisByName) {
                            if (!unisByCity.contains(university)) {
                                unisToRemove.add(university);
                            }
                        }
                        unisByName.removeAll(unisToRemove);
                        model.addAttribute("universities", unisByName);
                    }
                }
                if (model.getAttribute("universities") == null) {
                    ArrayList<University> allUniversities = (ArrayList<University>) this.asi.getAllUniversities();
                    model.addAttribute("universities", allUniversities);
                }

                return "university-all";
            }
            return "index";
        }
        return "redirect:/log-in/";
    }

    @GetMapping("/all-programmes")
    public String showAllProgrammes(HttpSession session, Model model) {
        if (session.getAttribute("currentUser") != null) {
            User currentUser = (User) session.getAttribute("currentUser");
            if (currentUser.isSuperuser()) {
                ArrayList<Programme> allProgrammes = (ArrayList<Programme>) this.pri.findAll();
                model.addAttribute("programmes", allProgrammes);
                return "programme-all";
            }
            return "index";
        }
        return "redirect:/log-in/";
    }

    @GetMapping("/new-programme")
    public String createNewProgrammeGet(HttpSession session, Model model) {
        if (session.getAttribute("currentUser") != null) {
            User currentUser = (User) session.getAttribute("currentUser");
            if (currentUser.isSuperuser()) {
                ArrayList<Degree> allDegrees = (ArrayList<Degree>) this.asi.getAllDegrees();
                ArrayList<Faculty> allFaculties = (ArrayList<Faculty>) this.asi.getAllFaculties();
                ArrayList<ProgrammeTypes> allProgrammeTypes = (ArrayList<ProgrammeTypes>) this.asi
                        .getAllProgrammeTypes();
                ArrayList<University> allUniversities = (ArrayList<University>) this.asi.getAllUniversities();
                ArrayList<Duration> allDurations = (ArrayList<Duration>) this.asi.getAllDurations();
                ArrayList<Payment> allPayments = (ArrayList<Payment>) this.asi.getAllPayments();
                ArrayList<StudyForm> allStudyForms = (ArrayList<StudyForm>) this.asi.getAllStudyForms();
                ArrayList<Language> allLanguages = (ArrayList<Language>) this.asi.getAllLanguages();
                ArrayList<StudyTypes> allStudyTypes = (ArrayList<StudyTypes>) this.asi.getAllStudyTypes();

                model.addAttribute("allDegrees", allDegrees);
                model.addAttribute("allFaculties", allFaculties);
                model.addAttribute("allProgrammeTypes", allProgrammeTypes);
                model.addAttribute("allUniversities", allUniversities);
                model.addAttribute("allDurations", allDurations);
                model.addAttribute("allPayments", allPayments);
                model.addAttribute("allStudyForms", allStudyForms);
                model.addAttribute("allLanguages", allLanguages);
                model.addAttribute("allStudyTypes", allStudyTypes);

                model.addAttribute("programme", new Programme());
                return "programme-new";
            }
            return "index";
        }
        return "redirect:/log-in/";
    }

    @PostMapping("/new-programme")
    public String createNewProgrammePost(HttpSession session, Model model, ServletRequest params, Programme programme) {
        if (session.getAttribute("currentUser") != null) {
            User currentUser = (User) session.getAttribute("currentUser");
            if (currentUser.isSuperuser()) {
                if (programme != null) {
                    if (params.getParameter("degree") != null) {
                        Degree tempDegree = this.asi
                                .getDegreeById(Long.parseLong((String) params.getParameter("degree")));
                        programme.setDegree(tempDegree);
                    }
                    if (params.getParameter("faculty") != null) {
                        Faculty tempFaculty = this.asi
                                .getFacultyById(Long.parseLong((String) params.getParameter("faculty")));
                        programme.setFaculty(tempFaculty);
                    }
                    if (params.getParameter("programme_type") != null) {
                        ProgrammeTypes tempProgrammeType = this.asi
                                .getProgrammeTypeById(Long.parseLong((String) params.getParameter("programme_type")));
                        programme.setProgramme_type(tempProgrammeType);
                    }
                    if (params.getParameter("university") != null) {
                        University tempUniversity = this.asi
                                .getUniversityById(Long.parseLong((String) params.getParameter("university")));
                        programme.setUniversity(tempUniversity);
                    }

                    if (params.getParameterValues("duration") != null) {
                        List<String> tempDurationParams = Arrays.asList(params.getParameterValues("duration"));
                        Set<Duration> tempDurationSet = new HashSet<>();
                        for (String durationId : tempDurationParams) {
                            tempDurationSet.add(this.asi.getDurationById(Long.parseLong(durationId)));
                            programme.setDuration(tempDurationSet);
                        }
                    }

                    if (params.getParameterValues("payment") != null) {
                        List<String> tempPaymentParams = Arrays.asList(params.getParameterValues("payment"));
                        Set<Payment> tempPaymentSet = new HashSet<>();
                        for (String paymentId : tempPaymentParams) {
                            tempPaymentSet.add(this.asi.getPaymentById(Long.parseLong(paymentId)));
                            programme.setPayment(tempPaymentSet);
                        }
                    }

                    if (params.getParameterValues("studyForms") != null) {
                        List<String> tempStudyFormsParams = Arrays.asList(params.getParameterValues("studyForms"));
                        Set<StudyForm> tempStudyFormsSet = new HashSet<>();
                        for (String studyFormId : tempStudyFormsParams) {
                            tempStudyFormsSet.add(this.asi.getStudyFormsById(Long.parseLong(studyFormId)));
                            programme.setStudyForms(tempStudyFormsSet);
                        }
                    }

                    if (params.getParameterValues("language") != null) {
                        List<String> tempLanguageParams = Arrays.asList(params.getParameterValues("language"));
                        Set<Language> tempLanguageSet = new HashSet<>();
                        for (String languageId : tempLanguageParams) {
                            tempLanguageSet.add(this.asi.getLanguageById(Long.parseLong(languageId)));
                            programme.setLanguage(tempLanguageSet);
                        }
                    }

                    if (params.getParameterValues("studyType") != null) {
                        List<String> tempStudyTypesParams = Arrays.asList(params.getParameterValues("studyType"));
                        Set<StudyTypes> tempStudyTypesSet = new HashSet<>();
                        for (String studyTypeId : tempStudyTypesParams) {
                            tempStudyTypesSet.add(this.asi.getStudyType(Long.parseLong(studyTypeId)));
                            programme.setStudyType(tempStudyTypesSet);
                        }
                    }
                    if (this.asi.addNewProgramme(programme)) {
                        return "redirect:/admin/all-programmes";
                    }
                }
            }
            return "index";
        }
        return "redirect:/log-in/";
    }

    @GetMapping("/programme-update/{id}")
    public String updateProgrammeGet(HttpSession session, Model model, @PathVariable(name = "id") Long id) {
        if (session.getAttribute("currentUser") != null) {
            User currentUser = (User) session.getAttribute("currentUser");
            if (currentUser.isSuperuser()) {
                Programme programmeToUpdate = this.pri.findById(id).get();
                if (programmeToUpdate != null) {
                    ArrayList<Degree> allDegrees = (ArrayList<Degree>) this.asi.getAllDegrees();
                    ArrayList<Faculty> allFaculties = (ArrayList<Faculty>) this.asi.getAllFaculties();
                    ArrayList<ProgrammeTypes> allProgrammeTypes = (ArrayList<ProgrammeTypes>) this.asi
                            .getAllProgrammeTypes();
                    ArrayList<University> allUniversities = (ArrayList<University>) this.asi.getAllUniversities();
                    ArrayList<Duration> allDurations = (ArrayList<Duration>) this.asi.getAllDurations();
                    ArrayList<Payment> allPayments = (ArrayList<Payment>) this.asi.getAllPayments();
                    ArrayList<StudyForm> allStudyForms = (ArrayList<StudyForm>) this.asi.getAllStudyForms();
                    ArrayList<Language> allLanguages = (ArrayList<Language>) this.asi.getAllLanguages();
                    ArrayList<StudyTypes> allStudyTypes = (ArrayList<StudyTypes>) this.asi.getAllStudyTypes();

                    model.addAttribute("programme", programmeToUpdate);
                    model.addAttribute("allDegrees", allDegrees);
                    model.addAttribute("allFaculties", allFaculties);
                    model.addAttribute("allProgrammeTypes", allProgrammeTypes);
                    model.addAttribute("allUniversities", allUniversities);
                    model.addAttribute("allDurations", allDurations);
                    model.addAttribute("allPayments", allPayments);
                    model.addAttribute("allStudyForms", allStudyForms);
                    model.addAttribute("allLanguages", allLanguages);
                    model.addAttribute("allStudyTypes", allStudyTypes);
                    return "programme-update";
                }
            }
            return "index";
        }
        return "redirect:/log-in/";
    }

    @PostMapping("/programme-update")
    public String updateProgrammePost(HttpSession session, Model model, ServletRequest params, Programme programme) {
        if (session.getAttribute("currentUser") != null) {
            User currentUser = (User) session.getAttribute("currentUser");
            if (currentUser.isSuperuser()) {
                if (programme != null) {
                    Long progammeId = programme.getId();
                    if (params.getParameter("degree") != null) {
                        Degree tempDegree = this.asi
                                .getDegreeById(Long.parseLong((String) params.getParameter("degree")));
                        programme.setDegree(tempDegree);
                    }
                    if (params.getParameter("faculty") != null) {
                        Faculty tempFaculty = this.asi
                                .getFacultyById(Long.parseLong((String) params.getParameter("faculty")));
                        programme.setFaculty(tempFaculty);
                    }
                    if (params.getParameter("programme_type") != null) {
                        ProgrammeTypes tempProgrammeType = this.asi
                                .getProgrammeTypeById(Long.parseLong((String) params.getParameter("programme_type")));
                        programme.setProgramme_type(tempProgrammeType);
                    }
                    if (params.getParameter("university") != null) {
                        University tempUniversity = this.asi
                                .getUniversityById(Long.parseLong((String) params.getParameter("university")));
                        programme.setUniversity(tempUniversity);
                    }

                    if (params.getParameterValues("duration") != null) {
                        List<String> tempDurationParams = Arrays.asList(params.getParameterValues("duration"));
                        Set<Duration> tempDurationSet = new HashSet<>();
                        for (String durationId : tempDurationParams) {
                            tempDurationSet.add(this.asi.getDurationById(Long.parseLong(durationId)));
                        }
                        programme.setDuration(tempDurationSet);
                    }

                    if (params.getParameterValues("payment") != null) {
                        List<String> tempPaymentParams = Arrays.asList(params.getParameterValues("payment"));
                        Set<Payment> tempPaymentSet = new HashSet<>();
                        for (String paymentId : tempPaymentParams) {
                            tempPaymentSet.add(this.asi.getPaymentById(Long.parseLong(paymentId)));
                        }
                        programme.setPayment(tempPaymentSet);
                    }

                    if (params.getParameterValues("studyForms") != null) {
                        List<String> tempStudyFormsParams = Arrays.asList(params.getParameterValues("studyForms"));
                        Set<StudyForm> tempStudyFormsSet = new HashSet<>();
                        for (String studyFormId : tempStudyFormsParams) {
                            tempStudyFormsSet.add(this.asi.getStudyFormsById(Long.parseLong(studyFormId)));
                        }
                        programme.setStudyForms(tempStudyFormsSet);
                    }

                    if (params.getParameterValues("language") != null) {
                        List<String> tempLanguageParams = Arrays.asList(params.getParameterValues("language"));
                        Set<Language> tempLanguageSet = new HashSet<>();
                        for (String languageId : tempLanguageParams) {
                            tempLanguageSet.add(this.asi.getLanguageById(Long.parseLong(languageId)));
                        }
                        programme.setLanguage(tempLanguageSet);
                    }

                    if (params.getParameterValues("studyType") != null) {
                        List<String> tempStudyTypesParams = Arrays.asList(params.getParameterValues("studyType"));
                        Set<StudyTypes> tempStudyTypesSet = new HashSet<>();
                        for (String studyTypeId : tempStudyTypesParams) {
                            tempStudyTypesSet.add(this.asi.getStudyType(Long.parseLong(studyTypeId)));
                        }
                        programme.setStudyType(tempStudyTypesSet);
                    }

                    if (this.asi.updateProgrammeById(progammeId, programme)) {
                        return "redirect:/admin/all-programmes";
                    } else {
                        return "redirect:/admin/programme-update/" + progammeId;
                    }
                }
            }
            return "index";
        }
        return "redirect:/log-in/";
    }

    @GetMapping("/programme-delete/{id}")
    public String deleteProgrammeById(HttpSession session, Model model, @PathVariable(name = "id") Long id) {
        if (session.getAttribute("currentUser") != null) {
            User currentUser = (User) session.getAttribute("currentUser");
            if (currentUser.isSuperuser()) {
                if (this.asi.deleteProgrammeById(id)) {
                    ArrayList<Programme> allProgrammes = (ArrayList<Programme>) this.pri.findAll();
                    model.addAttribute("programmes", allProgrammes);
                    return "programme-all";
                }
            }
            return "index";
        }
        return "redirect:/log-in/";
    }
}

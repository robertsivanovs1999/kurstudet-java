package lv.kurstudet.kurstudet.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class Base {

    @GetMapping("/{!api}*")
    public String initPage() {
        return "index";
    }

    @GetMapping("/result/*")
    public String specificResultPage() {
        // For some reason this endpoint otherwise does not get captured by the
        // `initPage` mapping
        return "index";
    }
}

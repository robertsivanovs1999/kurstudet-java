package lv.kurstudet.kurstudet.repos.education;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;

import lv.kurstudet.kurstudet.models.education.StudyForm;

public interface IStudyFormRepo extends JpaRepository<StudyForm, Long> {
    ArrayList<StudyForm> findByNameIn(ArrayList<String> allowedNames);
}

package lv.kurstudet.kurstudet.repos.education;

import org.springframework.data.jpa.repository.JpaRepository;

import lv.kurstudet.kurstudet.models.education.Duration;

public interface IDurationRepo extends JpaRepository<Duration, Long> {

}
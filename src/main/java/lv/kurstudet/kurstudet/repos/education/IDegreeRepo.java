package lv.kurstudet.kurstudet.repos.education;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;

import lv.kurstudet.kurstudet.models.education.Degree;

public interface IDegreeRepo extends JpaRepository<Degree, Long> {
    ArrayList<Degree> findByAggregateIn(ArrayList<String> allowedAggregates);
}

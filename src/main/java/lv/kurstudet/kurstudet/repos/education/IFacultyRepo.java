package lv.kurstudet.kurstudet.repos.education;

import org.springframework.data.jpa.repository.JpaRepository;

import lv.kurstudet.kurstudet.models.education.Faculty;

public interface IFacultyRepo extends JpaRepository<Faculty, Long> {

}
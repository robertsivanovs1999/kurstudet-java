package lv.kurstudet.kurstudet.repos.education;

import org.springframework.data.jpa.repository.JpaRepository;

import lv.kurstudet.kurstudet.models.education.ProgrammeTypes;

public interface IProgrammeTypesRepo extends JpaRepository<ProgrammeTypes, Long> {

}
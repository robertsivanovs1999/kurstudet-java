package lv.kurstudet.kurstudet.repos.education;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import lv.kurstudet.kurstudet.models.education.Programme;

public interface IProgrammeRepo extends JpaRepository<Programme, Long>, JpaSpecificationExecutor<Programme> {

    Page<Programme> findAll(Pageable pageable);

    Page<Programme> findByIdIn(List<Long> id, Pageable pageable);
}

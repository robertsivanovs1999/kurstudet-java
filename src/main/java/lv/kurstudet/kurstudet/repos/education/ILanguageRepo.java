package lv.kurstudet.kurstudet.repos.education;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;

import lv.kurstudet.kurstudet.models.education.Language;

public interface ILanguageRepo extends JpaRepository<Language, Long> {
    ArrayList<Language> findByAbbreviationIn(ArrayList<String> allowedAbbreviations);
}

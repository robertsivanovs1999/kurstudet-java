package lv.kurstudet.kurstudet.repos.education;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import lv.kurstudet.kurstudet.models.education.Locations;
import lv.kurstudet.kurstudet.models.education.University;

public interface IUniversityRepo extends JpaRepository<University, Long> {
    List<University> findAllByNameContaining(String name);

    List<University> findByLocation(Locations location);
}

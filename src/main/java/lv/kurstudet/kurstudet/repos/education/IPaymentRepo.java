package lv.kurstudet.kurstudet.repos.education;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;

import lv.kurstudet.kurstudet.models.education.Payment;

public interface IPaymentRepo extends JpaRepository<Payment, Long> {
    ArrayList<Payment> findAllByType(String type);

    ArrayList<Payment> findAllByTypeNot(String type);
}

package lv.kurstudet.kurstudet.repos.education;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;

import lv.kurstudet.kurstudet.models.education.Programme;
import lv.kurstudet.kurstudet.models.questions.Parameters;
import lv.kurstudet.kurstudet.models.questions.ProgrammeParameterMap;

public interface IProgParamMapRepo extends JpaRepository<ProgrammeParameterMap, Long> {
    ArrayList<ProgrammeParameterMap> findByParameterIsAndProgrammeIn(Parameters param, ArrayList<Programme> programmes);
}

package lv.kurstudet.kurstudet.repos.education;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;

import lv.kurstudet.kurstudet.models.education.StudyTypes;

public interface IStudyTypesRepo extends JpaRepository<StudyTypes, Long> {
    ArrayList<StudyTypes> findByTypeIn(ArrayList<String> allowedTypes);
}

package lv.kurstudet.kurstudet.repos.questions;

import org.springframework.data.jpa.repository.JpaRepository;

import lv.kurstudet.kurstudet.models.questions.Result;

public interface IResultRepo extends JpaRepository<Result, Long> {

}

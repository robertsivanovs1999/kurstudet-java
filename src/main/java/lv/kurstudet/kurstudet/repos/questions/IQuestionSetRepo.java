package lv.kurstudet.kurstudet.repos.questions;

import org.springframework.data.jpa.repository.JpaRepository;

import lv.kurstudet.kurstudet.models.questions.QuestionSet;

public interface IQuestionSetRepo extends JpaRepository<QuestionSet, Long> {
    QuestionSet findByName(String name);

}

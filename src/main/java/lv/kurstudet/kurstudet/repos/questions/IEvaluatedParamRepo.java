package lv.kurstudet.kurstudet.repos.questions;

import org.springframework.data.jpa.repository.JpaRepository;

import lv.kurstudet.kurstudet.models.questions.EvaluatedParameters;

public interface IEvaluatedParamRepo extends JpaRepository<EvaluatedParameters, Long> {

}

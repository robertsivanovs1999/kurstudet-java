package lv.kurstudet.kurstudet.repos.questions;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import lv.kurstudet.kurstudet.models.questions.QuestionSet;
import lv.kurstudet.kurstudet.models.questions.QuestionSetAndQuestionMap;

@Repository
public interface IQuestionSetAndQuestionMapRepo extends PagingAndSortingRepository<QuestionSetAndQuestionMap, Long> {
    Page<QuestionSetAndQuestionMap> findAllByQuestionSet(QuestionSet questionSet, Pageable pageable);
    Long countByQuestionSet(QuestionSet questionSet);
}

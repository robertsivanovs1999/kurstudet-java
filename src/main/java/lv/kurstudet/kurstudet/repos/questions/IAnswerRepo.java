package lv.kurstudet.kurstudet.repos.questions;

import org.springframework.data.jpa.repository.JpaRepository;

import lv.kurstudet.kurstudet.models.questions.Answer;

public interface IAnswerRepo extends JpaRepository<Answer, Long> {

}

package lv.kurstudet.kurstudet.repos.core;

import org.springframework.data.jpa.repository.JpaRepository;

import lv.kurstudet.kurstudet.models.auth.User;
import lv.kurstudet.kurstudet.models.core.Profile;

public interface IProfileRepo extends JpaRepository<Profile, Long> {
    Profile findByUser(User user);
}

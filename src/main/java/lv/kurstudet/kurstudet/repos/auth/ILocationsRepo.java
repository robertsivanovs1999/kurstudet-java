package lv.kurstudet.kurstudet.repos.auth;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import lv.kurstudet.kurstudet.models.education.Locations;

public interface ILocationsRepo extends JpaRepository<Locations, Long> {
    Locations findByAddress(String address);

    boolean existsByAddress(String address);

    List<Locations> findAllByAddressContaining(String word);
}
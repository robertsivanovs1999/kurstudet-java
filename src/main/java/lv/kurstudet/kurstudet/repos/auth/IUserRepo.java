package lv.kurstudet.kurstudet.repos.auth;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import lv.kurstudet.kurstudet.models.auth.User;

public interface IUserRepo extends JpaRepository<User, Long> {

    User findByUsernameAndPassword(String username, String password);

    User findByUsername(String username);

    User findByEmail(String email);

    List<User> findAllByLastName(String lastName);

    List<User> findAllByUsernameAndLastNameAndEmail(String username, String lastName, String email);

    List<User> findAllByUsernameAndLastName(String username, String lastName);

    List<User> findAllByLastNameAndEmail(String lastName, String email);

    List<User> findAllByUsernameAndEmail(String username, String email);

    List<User> findAllByIsActive(boolean isActive);

    List<User> findAllByIsStaff(boolean isStaff);

    List<User> findAllByIsSuperuser(boolean isSuperuser);

    List<User> findAllByIsActiveAndIsStaff(boolean isActive, boolean isStaff);

    List<User> findAllByIsStaffAndIsSuperuser(boolean isStaff, boolean isSuperuser);

    List<User> findAllByIsActiveAndIsSuperuser(boolean isActive, boolean isSuperuser);

    List<User> findAllByIsActiveAndIsStaffAndIsSuperuser(boolean isActive, boolean isStaff, boolean isSuperuser);

    boolean existsByEmail(String email);

    boolean existsByUsername(String username);
}

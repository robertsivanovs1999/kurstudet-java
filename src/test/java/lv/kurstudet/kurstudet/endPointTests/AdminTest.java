package lv.kurstudet.kurstudet.endPointTests;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;

import lv.kurstudet.kurstudet.AbstractIntegrationTest;
import lv.kurstudet.kurstudet.controllers.admin.AdminController;
import lv.kurstudet.kurstudet.models.auth.User;

@RunWith(SpringRunner.class)
@WebMvcTest(AdminController.class)
public class AdminTest extends AbstractIntegrationTest {

    @Autowired
    AdminController aControl;

    // @Test
    // public void setAdminPanelTest() {
    //     try {
    //         // assertNotNull(aControl.setAdminPanel(null));
    //     } catch (Exception e) {
    //         e.printStackTrace();
            // fail();
    //     }
    // }

    // @Test
    // public void getAddNewUserTest() {
    //     try {
    //         User user = new User();

    //         assertNotNull(aControl.addNewUserGet(null, user));
    //     } catch (Exception e) {
    //         e.printStackTrace();
            // fail();
    //     }
    // }

    // @Test
    // public void postAddNewUserTest() {
    //     try {
    //         String uri = "/temp/new-user";

    //         MvcResult result = this.mvc.perform(post(uri)).andReturn();
    //         // System.out.println("\n\n\n\n\n" + result.getResponse().getContentType());

    //         assertEquals(200, result.getResponse().getStatus());
    //     } catch (Exception e) {
    //         e.printStackTrace();
            // fail();
    //     }
    // }

    // @Test
    // public void getCreateNewUniversityTest() {
    //     try {
    //         String uri = "/temp/new-university";

    //         MvcResult result = this.mvc.perform(get(uri)).andReturn();
    //         // System.out.println("\n\n\n\n\n" + result.getResponse().getContentType());

    //         assertEquals(200, result.getResponse().getStatus());
    //     } catch (Exception e) {
    //         e.printStackTrace();
            // fail();
    //     }
    // }

    // @Test
    // public void postCreateNewUniversityTest() {
    //     try {
    //         String uri = "/temp/new-university";

    //         MvcResult result = this.mvc.perform(post(uri)).andReturn();
    //         // System.out.println("\n\n\n\n\n" + result.getResponse().getContentType());

    //         assertEquals(200, result.getResponse().getStatus());
    //     } catch (Exception e) {
    //         e.printStackTrace();
            // fail();
    //     }
    // }

    // @Test
    // public void getUpdateUserByIdTest() {
    //     try {
    //         // TODO somehow get user id
    //         String uri = "/temp/user-update/{id}";

    //         MvcResult result = this.mvc.perform(get(uri)).andReturn();
    //         // System.out.println("\n\n\n\n\n" + result.getResponse().getContentType());

    //         assertEquals(200, result.getResponse().getStatus());
    //     } catch (Exception e) {
    //         e.printStackTrace();
            // fail();
    //     }
    // }

    // @Test
    // public void postUpdateUserByIdTest() {
    //     try {
    //         String uri = "/temp/user-update";

    //         MvcResult result = this.mvc.perform(post(uri)).andReturn();
    //         // System.out.println("\n\n\n\n\n" + result.getResponse().getContentType());

    //         assertEquals(200, result.getResponse().getStatus());
    //     } catch (Exception e) {
    //         e.printStackTrace();
            // fail();
    //     }
    // }

    // @Test
    // public void deleteUserByIdTest() {
    //     try {
    //         // TODO somehow get user id
    //         String uri = "/temp/user-delete/{id}";
    //         getTest_Status_200(uri);
    //     } catch (Exception e) {
    //         e.printStackTrace();
            // fail();
    //     }
    // }

    // @Test
    // public void getshowAllUsersTest() {
    //     try {
    //         String uri = "/temp/all-user";
    //         getTest_HtmlUTF8(uri);
    //     } catch (Exception e) {
    //         e.printStackTrace();
            // fail();
    //     }
    // }

    // @Test
    // public void getAllUniversitiesTest() {
    //     try {
    //         String uri = "/temp/all-universities";
    //         getTest_HtmlUTF8(uri);
    //     } catch (Exception e) {
    //         e.printStackTrace();
            // fail();
    //     }
    // }

    // @Test
    // public void getAllProgrammesTest() {
    //     try {
    //         String uri = "/temp/all-programmes";
    //         getTest_HtmlUTF8(uri);
    //     } catch (Exception e) {
    //         e.printStackTrace();
            // fail();
    //     }
    // }

    // @Test
    // public void getNewProgrammeTest() {
    //     try {
    //         String uri = "/temp/new-programme";

    //         MvcResult result = this.mvc.perform(get(uri)).andReturn();
    //         // System.out.println("\n\n\n\n\n" + result.getResponse().getContentType());

    //         assertEquals(200, result.getResponse().getStatus());
    //     } catch (Exception e) {
    //         e.printStackTrace();
            // fail();
    //     }
    // }

    // @Test
    // public void postCreateNewProgrammeTest() {
    //     try {
    //         String uri = "/temp/new-programme";

    //         MvcResult result = this.mvc.perform(post(uri)).andReturn();
    //         // System.out.println("\n\n\n\n\n" + result.getResponse().getContentType());

    //         assertEquals(200, result.getResponse().getStatus());
    //     } catch (Exception e) {
    //         e.printStackTrace();
            // fail();
    //     }
    // }

    // @Test
    // public void getUpdateProgrammeTest() {
    //     try {
    //         // TODO somehow get user id
    //         String uri = "/temp/programme-update/{id}";

    //         MvcResult result = this.mvc.perform(get(uri)).andReturn();
    //         // System.out.println("\n\n\n\n\n" + result.getResponse().getContentType());

    //         assertEquals(200, result.getResponse().getStatus());
    //     } catch (Exception e) {
    //         e.printStackTrace();
    //     }
            // fail();
    // }

    // @Test
    // public void postUpdateProgrammeTest() {
    //     try {
    //         String uri = "/temp/programme-update";

    //         MvcResult result = this.mvc.perform(post(uri)).andReturn();
    //         // System.out.println("\n\n\n\n\n" + result.getResponse().getContentType());

    //         assertEquals(200, result.getResponse().getStatus());
    //     } catch (Exception e) {
    //         e.printStackTrace();
            // fail();
    //     }
    // }
}
package lv.kurstudet.kurstudet.endPointTests;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;

import lv.kurstudet.kurstudet.AbstractIntegrationTest;
import lv.kurstudet.kurstudet.controllers.education.CardController;
import lv.kurstudet.kurstudet.models.education.Degree;
import lv.kurstudet.kurstudet.models.education.Duration;
import lv.kurstudet.kurstudet.models.education.Faculty;
import lv.kurstudet.kurstudet.models.education.Language;
import lv.kurstudet.kurstudet.models.education.Locations;
import lv.kurstudet.kurstudet.models.education.Payment;
import lv.kurstudet.kurstudet.models.education.Programme;
import lv.kurstudet.kurstudet.models.education.ProgrammeTypes;
import lv.kurstudet.kurstudet.models.education.StudyForm;
import lv.kurstudet.kurstudet.models.education.StudyTypes;
import lv.kurstudet.kurstudet.models.education.University;
import lv.kurstudet.kurstudet.repos.auth.ILocationsRepo;
import lv.kurstudet.kurstudet.repos.education.IDegreeRepo;
import lv.kurstudet.kurstudet.repos.education.IDurationRepo;
import lv.kurstudet.kurstudet.repos.education.IFacultyRepo;
import lv.kurstudet.kurstudet.repos.education.ILanguageRepo;
import lv.kurstudet.kurstudet.repos.education.IPaymentRepo;
import lv.kurstudet.kurstudet.repos.education.IProgrammeRepo;
import lv.kurstudet.kurstudet.repos.education.IProgrammeTypesRepo;
import lv.kurstudet.kurstudet.repos.education.IStudyFormRepo;
import lv.kurstudet.kurstudet.repos.education.IStudyTypesRepo;
import lv.kurstudet.kurstudet.repos.education.IUniversityRepo;
import lv.kurstudet.kurstudet.services.admin.IAdminService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
@SpringBootTest
public class EducationTest extends AbstractIntegrationTest {
    @Autowired
    private CardController cControl;
    
    @Autowired
    IAdminService cServ;
    
    @Autowired
    IUniversityRepo uRepo;

    @Autowired
    ILocationsRepo lRepo;

    @Autowired
    IDegreeRepo degRepo;

    @Autowired
    IFacultyRepo facRepo;

    @Autowired
    IProgrammeTypesRepo ptRepo;

    @Autowired
    IDurationRepo durRepo;

    @Autowired
    IPaymentRepo pRepo;

    @Autowired
    IStudyFormRepo sfRepo;

    @Autowired
    ILanguageRepo lanRepo;

    @Autowired
    IStudyTypesRepo stRepo;

    @Autowired
    IProgrammeRepo proRepo;

    @Test
    public void getUniversityTest () {
        try {
            String name = "VeA";
            String address = "Ventspils, Inženieru iela 101";
            String country = "Latvija";
            String city = "Liepāja";
            
            Locations uniLocation = new Locations();
            uniLocation.setAddress(address);
            uniLocation.setCity(city);
            uniLocation.setCountry(country);
            
            lRepo.save(uniLocation);

            University newUni = new University();
            newUni.setLocation(uniLocation);
            newUni.setName(name);
            
            uRepo.save(newUni);

            Collection<University> tempList = new ArrayList<>();
            tempList.add(newUni);
            
            JSONObject uniData = new JSONObject();
            JSONArray uniArray = new JSONArray();
            tempList.forEach(i -> uniArray.put(i.constructMiddleLayer()));
            uniData.put("data", uniArray.toString());

            assertNotNull(cControl.getUniversity());
            JSONAssert.assertEquals(uniData.toString(), cControl.getUniversity(), false);
        
            uRepo.deleteAll();
            lRepo.deleteAll();
        }catch(Exception e){
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void getProgrammeNameAgrregateTest () {
        try {
            Locations uniLocation = new Locations();
            uniLocation.setAddress("Ventspils, Inženieru iela 101");
            uniLocation.setCity("Liepāja");
            uniLocation.setCountry("Latvija");

            lRepo.save(uniLocation);

            University newUni = new University();
            newUni.setLocation(uniLocation);
            newUni.setName("VeA");

            uRepo.save(newUni);

            Degree newDeg = new Degree();
            newDeg.setName("PhD");
            newDeg.setAggregate("Oke");

            degRepo.save(newDeg);

            Faculty newFac = new Faculty();
            newFac.setName("ITB");
            newFac.setLocation(uniLocation);

            facRepo.save(newFac);

            ProgrammeTypes newProgTyp = new ProgrammeTypes();
            newProgTyp.setName("Math");

            ptRepo.save(newProgTyp);

            Programme programme = new Programme();
            programme.setTitle("Datorzinātnes");
            programme.setCode(Long.valueOf(357854934));
            programme.setCreditPoints("4");
            programme.setDescription("I like math very much, yesssss");
            programme.setWebsite("venta.lv");
            programme.setPhone("24830568");
            programme.setEmail("dog@gmail.com");
            programme.setDegree(newDeg);
            programme.setFaculty(newFac);
            programme.setProgramme_type(newProgTyp);
            programme.setUniversity(newUni);
            programme.setDuration(null);
            programme.setPayment(null);
            programme.setStudyForms(null);
            programme.setLanguage(null);
            programme.setStudyType(null);
            programme.setIsActive(true);

            proRepo.save(programme);

            ArrayList<Programme> uniqueProgrammes = new ArrayList<>();
            uniqueProgrammes.add(programme);
            
            JSONArray result = new JSONArray();
            uniqueProgrammes.forEach(i -> {
                try {
                    JSONObject innerObjIter = new JSONObject();
                    innerObjIter.put("title", i.getTitle());
                    innerObjIter.put("repeats", 1); // If there will be more than one programme in database then this 
                                                    // will fail, remember to clear the db with repost after each test
                    result.put(innerObjIter);
                } catch (Exception e) {
                    e.printStackTrace();
                    fail();
                }
            });
            JSONObject outerObjIter = new JSONObject();
            outerObjIter.put("data", result);

            // System.out.println("\n\n\n\n\n" + outerObjIter + "\n\n\n\n\n");
            // System.out.println("\n\n\n\n\n" + cControl.getProgrammeNameAggregate() + "\n\n\n\n\n");
            // System.out.println("\n\n\n\n\n" + cControl.getUniversity() + "\n\n\n\n\n");
            
            assertNotNull(cControl.getProgrammeNameAggregate());
            JSONAssert.assertEquals(outerObjIter.toString(), cControl.getProgrammeNameAggregate(), false);

            proRepo.deleteAll();
            degRepo.deleteAll();
            facRepo.deleteAll();
            ptRepo.deleteAll();
            uRepo.deleteAll();
            lRepo.deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void getPaymentTest () {
        try {
            Payment newPay = new Payment();
            newPay.setType("PayPal");

            pRepo.save(newPay);

            ArrayList<Payment> payments = new ArrayList<>();
            payments.add(newPay);
            JSONArray dataArray = new JSONArray();
            JSONObject outerObjIter = new JSONObject();
            payments.forEach(i -> dataArray.put(i.constructMiddleLayer()));
            outerObjIter.put("data", dataArray.toString());

            assertNotNull(cControl.getPayments());
            JSONAssert.assertEquals(outerObjIter.toString(), cControl.getPayments(), false);
        
            pRepo.deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void getStudyFormAggregateTest () {
        try {
            StudyForm newStudFor = new StudyForm();
            newStudFor.setName("Bank");

            sfRepo.save(newStudFor);

            ArrayList<StudyForm> payments = new ArrayList<>();
            payments.add(newStudFor);
            JSONArray result = new JSONArray();
            payments.forEach(i -> {
                try {
                    JSONObject innerObjIter = new JSONObject();
                    innerObjIter.put("name", i.getName());
                    innerObjIter.put("repeats", 1); // If there will be more than one programme in database then this 
                                                    // will fail, remember to clear the db with repost after each test
                    result.put(innerObjIter);
                } catch (Exception e) {
                    e.printStackTrace();
                    fail();
                }
            });
            JSONObject outerObjIter = new JSONObject();
            outerObjIter.put("data", result);

            assertNotNull(cControl.getStudyTypeAggregate());
            JSONAssert.assertEquals(outerObjIter.toString(), cControl.getStudyFormAggregate(), false);

            sfRepo.deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void getProgrammeDurationAggregateTest () {
        try {
            Duration newDur = new Duration();
            newDur.setLength("100 weeks");

            durRepo.save(newDur);

            ArrayList<Duration> payments = new ArrayList<>();
            payments.add(newDur);
            JSONArray result = new JSONArray();
            payments.forEach(i -> {
                try {
                    JSONObject innerObjIter = new JSONObject();
                    innerObjIter.put("pk", i.getId());
                    innerObjIter.put("length", i.getLength());
                    result.put(innerObjIter);
                } catch (Exception e) {
                    e.printStackTrace();
                    fail();
                }
            });
            JSONObject outerObjIter = new JSONObject();
            outerObjIter.put("data", result);

            assertNotNull(cControl.getProgrammeDurationAggregate());
            JSONAssert.assertEquals(outerObjIter.toString(), cControl.getProgrammeDurationAggregate(), false);

            durRepo.deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void getDegreeTest () {
        try {
            Degree newDeg = new Degree();
            newDeg.setName("PhD");
            newDeg.setAggregate("Oke");

            ArrayList<Degree> payments = new ArrayList<>();
            payments.add(newDeg);
            JSONArray result = new JSONArray();
            payments.forEach(i -> {
                result.put(i.getAggregate());
            });
            JSONObject outerObjIter = new JSONObject();
            outerObjIter.put("data", result);

            degRepo.save(newDeg);

            assertNotNull(cControl.getDegree());
            JSONAssert.assertEquals(outerObjIter.toString(), cControl.getDegree(), false);

            degRepo.deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    // @Test
    // public void getProgrammeTest () {
    //     try {
            // Locations uniLocation = new Locations();
            // uniLocation.setAddress("Ventspils, Inženieru iela 101");
            // uniLocation.setCity("Liepāja");
            // uniLocation.setCountry("Latvija");

            // lRepo.save(uniLocation);

            // University newUni = new University();
            // newUni.setLocation(uniLocation);
            // newUni.setName("VeA");

            // uRepo.save(newUni);

            // Degree newDeg = new Degree();
            // newDeg.setName("PhD");
            // newDeg.setAggregate("Oke");

            // degRepo.save(newDeg);

            // Faculty newFac = new Faculty();
            // newFac.setName("ITB");
            // newFac.setLocation(uniLocation);

            // facRepo.save(newFac);

            // ProgrammeTypes newProgTyp = new ProgrammeTypes();
            // newProgTyp.setName("Math");

            // ptRepo.save(newProgTyp);

            // Programme programme = new Programme();
            // programme.setTitle("Datorzinātnes");
            // programme.setCode(Long.valueOf(357854934));
            // programme.setCreditPoints("4");
            // programme.setDescription("I like math very much, yesssss");
            // programme.setWebsite("venta.lv");
            // programme.setPhone("24830568");
            // programme.setEmail("dog@gmail.com");
            // programme.setDegree(newDeg);
            // programme.setFaculty(newFac);
            // programme.setProgramme_type(newProgTyp);
            // programme.setUniversity(newUni);
            // programme.setDuration(null);
            // programme.setPayment(null);
            // programme.setStudyForms(null);
            // programme.setLanguage(null);
            // programme.setStudyType(null);
            // programme.setIsActive(true);

            // proRepo.save(programme);

            // JSONObject temp = new JSONObject();
            // temp.put("university", programme.getUniversity().getId());
            // temp.put("title", programme.getTitle());
            // temp.put("pay_id", programme.getPayment());
            // temp.put("study_form", programme.getStudyForms());
            // temp.put("time", programme.getDuration());
            // temp.put("degree", programme.getDegree());

            // Map<String, Object> tempProg = new HashMap<>();
            // tempProg.put("count", 1);
            // tempProg.put("page", 1);
            // tempProg.put("json", temp);

            // System.out.println("\n\n\n\n\n" + tempProg + "\n\n\n\n\n");
            // System.out.println("\n\n\n\n\n" + cControl.getProgramme(tempProg) + "\n\n\n\n\n");


            // assertNotNull(cControl.getProgramme(new HashMap<String, Object>()));
            // JSONAssert.assertEquals(outerObjIter.toString(), cControl.getProgramme(tempProg), false);

            // System.out.println("\n\n\n\n\n" + cControl.getUniversity() + "\n\n\n\n\n");
            

            // proRepo.deleteAll();
            // degRepo.deleteAll();
            // facRepo.deleteAll();
            // ptRepo.deleteAll();
            // uRepo.deleteAll();
            // lRepo.deleteAll();
    //     } catch (Exception e) {
    //         e.printStackTrace();
    //         fail();
    //     }
    // }
}
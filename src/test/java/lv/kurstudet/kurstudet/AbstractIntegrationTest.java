package lv.kurstudet.kurstudet;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.PostgreSQLContainer;

@ContextConfiguration(initializers = AbstractIntegrationTest.Initializer.class)
public class AbstractIntegrationTest {

    public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>();

        private static HashMap<String, Object> createConnectionConfiguration() {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("spring.datasource.url", postgres.getJdbcUrl());
            map.put("spring.datasource.username", postgres.getUsername());
            map.put("spring.datasource.password", postgres.getPassword());
            return map;
        }

        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {

            postgres.start();

            ConfigurableEnvironment environment = applicationContext.getEnvironment();

            MapPropertySource testcontainers = new MapPropertySource("testcontainers", createConnectionConfiguration());

            environment.getPropertySources().addFirst(testcontainers);
        }
    }
}

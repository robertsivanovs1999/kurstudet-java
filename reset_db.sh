#!/bin/bash

docker-compose up -d
sleep 5

docker exec -it database1 sh -c "psql -U database1_role database1 -c \"DROP SCHEMA public CASCADE;\""
docker exec -it database1 sh -c "psql -U database1_role database1 -c \"CREATE SCHEMA public;\""
docker exec -it database1 sh -c "psql -U database1_role database1 -c \"GRANT ALL ON SCHEMA public TO database1_role;\""
docker exec -it database1 sh -c "psql -U database1_role database1 -c \"GRANT ALL ON SCHEMA public TO public;\""

cat config/db/dump.sql | docker exec -i database1 bash -c "psql database1 database1_role"
docker-compose down

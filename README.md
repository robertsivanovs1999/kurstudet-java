# kurstudet.lv-JAVA

A website made to help pupils decide where to study by providing vast information about universities and their programmes as well as a test, that they can take to find the best study programme match.

#### Installation

Make sure your system has [bash](https://www.howtogeek.com/249966/how-to-install-and-use-the-linux-bash-shell-on-windows-10/) available as well as [docker](https://docs.docker.com/desktop/) and [docker-compose](https://docs.docker.com/compose/install/). We are using custom shell scripts and docker to create a database from scratch and populate the database with data.
After installing these prerequisites.

```bash
./init.sh
```

#### Start the Java project

This will raise the docker PostgreSQL database and start the Maven project, compiling JavaScript in the background - 127.0.0.1:8080.

```bash
./start.sh
```

#### Run tests

```bash
./start.sh --test
```

#### Compile for deployment

```bash
./start.sh --prod
```

#### Reset the database

```bash
./reset_db.sh
```

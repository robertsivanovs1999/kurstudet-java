#!/bin/bash

if [ $1 = '--prod' ];
then
    echo "startig production mode"
    docker-compose -f docker-compose.yml -f docker-compose.prod.yml up
elif [ $1 = '--test' ];
then
    echo "startig tests"
    sudo rm -rf ./target/ && rm .classpath
    mvn clean test
else
    echo "starting development mode"
    docker-compose -f docker-compose.yml up
fi;


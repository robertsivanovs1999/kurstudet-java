import React, { useState, useEffect, useContext } from 'react';
import { getAllProgrammes } from '../common/apiCalls';
import { ModalContext } from '../../../../../../core/static/core/js/src/contexts/MainModalContext';

import style from '../../../style/programme.scss';

export default function ProgrammeInfo({ programmePk }) {
  const [programme, setProgramme] = useState(null);
  const { toggleModal, toggleOffComponent } = useContext(ModalContext);

  useEffect(() => {
    getAllProgrammes((data) => setProgramme(data.data[0]), 1, 1, {
      id: [programmePk],
    });
  }, [programmePk]);
  console.log(programme);

  return (
    <div className={`container-fluid ${style['detailed-university']}`}>
      {programme && (
        <>
          <h1>
            {programme.fields.title} {toggleOffComponent}
          </h1>
          <h2>{programme.fields.university_name}</h2>

          <div className="row mt-3">
            <div className="col-sm">
              <div className={`card ${style['color-strip-purple']}`}>
                <div className={`${style['sub-card']} text-left`}>
                  <h4 className={style.title}>Fakultāte</h4>
                  <div className={style.info}>
                    {programme.fields.faculty_name}
                  </div>

                  <h4 className={style.title}>Valoda</h4>
                  <div className={style.info}>{programme.fields.language}</div>

                  <h4 className={style.title}>Studiju ilgums</h4>
                  <div className={style.info}>{programme.fields.time}</div>

                  <h4 className={style.title}>Studiju maksa</h4>
                  <div className={style.info}>{programme.fields.pay}</div>
                </div>
              </div>
            </div>
            <div className="col-sm">
              <div className={`card ${style['color-strip-orange']}`}>
                <div className={`text-center ${style['sub-card']}`}>
                  <div>{programme.fields.description}</div>
                  <a href={`http:\\\\${programme.fields.website}`}>
                    <button className={`${style['popup-btn']}`}>
                      Uz mājaslapu
                    </button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </div>
  );
}

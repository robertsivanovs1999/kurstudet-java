import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Select from 'react-select';
import CreatableSelect from 'react-select/creatable';

const customStyles = {
  clearIndicator: (base, state) => {
    return {
      ...base,
      color: '#F37335',
      verticalAlign: 'middle',
      textAlign: 'center',
    };
  },
  control: (base, state) => ({
    ...base,
    background: '#FFFFFF',
    border: `0.2rem solid ${state.isFocused ? '#FDC830' : '#60847B'} `,
    borderRadius: '2rem',
    outline: 'none',
    fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontWeight: 'normal',
    display: 'flex',
    boxShadow: 'none',
    borderColor: '#FDC830',
    '&:hover': { borderColor: state.isFocused ? '#FDC830' : '#60847B' },
  }),
  dropdownIndicator: (base) => ({
    ...base,
    display: 'none',
  }),
  indicatorSeparator: (base) => ({
    ...base,
    display: 'none',
  }),
  placeholder: (base) => ({ ...base, color: '#60847B', textAlign: 'center' }),
  multiValueLabel: (base) => ({
    ...base,
    fontSize: '0.8rem',
    fontWeight: 'bold',
    // color: "white",
    backgroundColor: '#9FACA8',
    borderRadius: '1rem 0 0 1rem',
  }),
  multiValueRemove: (base) => ({
    ...base,
    backgroundColor: '#F37335',
    borderRadius: '0 1rem 1rem 0',
  }),
  multiValue: (base) => ({ ...base, backgroundColor: 'transparent' }),
};

export function FilterSelect({
  placeholder,
  data,
  onChange,
  getOptionValue,
  getOptionLabel,
  value,
  overrideStyle,
}) {
  const dropDown = (
    <Select
      styles={overrideStyle ? overrideStyle : customStyles}
      value={value}
      placeholder={placeholder}
      options={data}
      isMulti
      onChange={onChange}
      getOptionValue={getOptionValue}
      getOptionLabel={getOptionLabel}
    />
  );
  return dropDown;
}

const isValidNewOption = (inputValue, selectValue, selectOptions) => {
  if (
    inputValue.trim().length === 0
    || selectOptions.find((option) => option.name === inputValue)
  ) {
    return false;
  }
  return true;
};

export function FilterCreatableSelect({
  placeholder,
  data,
  onChange,
  getOptionValue,
  getOptionLabel,
  getNewOptionData,
  value,
  overrideStyle,
}) {
  const dropDown = (
    <CreatableSelect
      createOptionPosition="first"
      isMulti
      styles={overrideStyle ? overrideStyle : customStyles}
      options={data}
      value={value}
      placeholder={placeholder}
      onChange={onChange}
      getOptionValue={getOptionValue}
      getOptionLabel={getOptionLabel}
      getNewOptionData={getNewOptionData}
      isValidNewOption={isValidNewOption}
    />
  );
  return dropDown;
}

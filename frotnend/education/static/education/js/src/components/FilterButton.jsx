import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default function FilterButton({ onClick, className, children }) {
  return (
    <span className="pl-2">
      <button className={className} type="button" onClick={onClick}>
        {children}
      </button>
    </span>
  );
}

FilterButton.propTypes = {
  onClick: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired,
};

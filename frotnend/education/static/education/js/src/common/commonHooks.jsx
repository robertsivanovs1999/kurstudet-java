import React, { useState } from 'react';
import { FilterSelect, FilterCreatableSelect } from '../components/FilterSelect';

export default function useModelSelect({
  type,
  initialData,
  key,
  placeholder,
  getOptionValue,
  getOptionLabel,
  getNewOptionData,
  values,
  setValues,
  overrideStyle,
}) {
  let input;
  switch (type) {
    case 'creatable':
      input = (
        <FilterCreatableSelect
          key={key}
          placeholder={placeholder}
          data={initialData}
          value={values}
          onChange={(opt) => (opt ? setValues(opt) : setValues([]))}
          getOptionValue={getOptionValue}
          getOptionLabel={getOptionLabel}
          getNewOptionData={getNewOptionData}
          overrideStyle={overrideStyle}
        />
      );
      break;
    case 'selectable':
      input = (
        <FilterSelect
          key={key}
          value={values}
          placeholder={placeholder}
          data={initialData}
          onChange={(opt) => (opt ? setValues(opt) : setValues([]))}
          getOptionValue={getOptionValue}
          getOptionLabel={getOptionLabel}
          overrideStyle={overrideStyle}
        />
      );
      break;
    default:
      input = <div />;
  }
  return [input];
}

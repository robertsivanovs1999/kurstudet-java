import RequesterInstance from '../../../../../../core/static/core/js/src/misc/Requester';

export async function getAllProgrammes(callback, count, page, search) {
  const result = await RequesterInstance.get('get-all-programmes', {
    count,
    page,
    // json: search,
    json: JSON.stringify(search),
  });
  result.data.data = JSON.parse(result.data.data);
  callback(result.data);
}

export async function getAllUniversities(callback, id) {
  const result = await RequesterInstance.get('get-all-universities', {
    id,
  });
  const results = JSON.parse(result.data.data);
  callback(results);
}

export async function getAggregatedProgrammes(callback) {
  const result = await RequesterInstance.get('get-programme-name-aggregate');
  callback(result.data.data);
}

export async function getAggregatedStudyForms(callback) {
  const result = await RequesterInstance.get('get-study-form-aggregate');
  callback(result.data.data);
}

export async function getAggregatedStudyTypes(callback) {
  const result = await RequesterInstance.get('get-study-type-aggregate');
  callback(result.data.data);
}
export async function getAggregatedProgrammeDuration(callback) {
  const result = await RequesterInstance.get(
    'get-programme-duration-aggregate',
  );
  callback(result.data.data);
}

export async function getPayment(callback) {
  const result = await RequesterInstance.get('get-payment');
  const results = JSON.parse(result.data.data);
  callback(results);
}

export async function getLanguage(callback) {
  const result = await RequesterInstance.get('get-language');
  const results = JSON.parse(result.data.data);
  callback(results);
}

export async function getDegree(callback) {
  const result = await RequesterInstance.get('get-degree');
  callback(result.data.data);
}

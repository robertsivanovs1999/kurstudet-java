import React, { useContext } from 'react';

import { Link, useLocation } from 'react-router-dom';

import styles from '../../../style/programme.scss';

export function makeShort(text, len) {
  return text.length - 4 > len ? `${text.slice(0, len - 4)} ...` : text;
}

export default function DisplayCard({ programme, viewProgramme }) {
  const { pathanme } = useLocation();
  return (
    <>
      <div className={`card ${styles.card}`}>
        <aside className={`${styles['top-bar-green']}`}></aside>
        <aside className={`${styles['top-bar-gradient']}`}></aside>
        <div className={`${styles.line1}`}></div>
        <div className={`${styles.line2}`}></div>
        <ul className={`list-group list-group-flush ${styles['card-header']}`}>
          <li className="">
            <h1 className={`${styles.programme}`}>
              {makeShort(programme.fields.title, 35)}
            </h1>
            <h2 className={`${styles.university}`}>
              {makeShort(programme.fields.university_name, 35)}
            </h2>
          </li>
        </ul>
        <ul className={`list-group list-group-flush ${styles['card-details']}`}>
          <li className="text-left">
            <b>Fakultāte:</b>{' '}
            {programme.fields.faculty_name
              ? makeShort(programme.fields.faculty_name, 15)
              : '-'}
          </li>
          <li>
            <b>Studiju ilgums:</b> {makeShort(programme.fields.time, 15)}
          </li>
          <li>
            <b>Valoda:</b>{' '}
            {programme.fields.language.length > 0
              ? programme.fields.language.join(' ').slice(0, 15)
              : 'LV'}
          </li>
          <li>
            <b>Maksa:</b> {makeShort(programme.fields.pay.join(' '), 15)}
          </li>
          <li className={`${styles.desc}`}>
            <b>Apraksts</b>
            {makeShort(programme.fields.description, 100)}
          </li>
        </ul>
        <button
          type="button"
          onClick={() => viewProgramme(programme.pk)}
          className={`${styles['card-btn']}`}
          style={{ height: '3rem' }}
        >
          Skatīt vairāk
        </button>
      </div>
    </>
  );
}


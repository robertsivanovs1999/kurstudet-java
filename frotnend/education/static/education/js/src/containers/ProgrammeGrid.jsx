import React, { useState, useEffect, useCallback } from 'react';

import ReactPaginate from 'react-paginate';
import {
  Switch, useLocation, useParams, useHistory,
} from 'react-router-dom';

import {
  getAllProgrammes,
  getAllUniversities,
  getAggregatedProgrammes,
  getPayment,
  getAggregatedStudyForms,
  getAggregatedStudyTypes,
  getAggregatedProgrammeDuration,
  getDegree,
} from '../common/apiCalls';
import DisplayCard from './DisplayCard';
import useModelSelect from '../common/commonHooks';

import styles from '../../../style/filter.scss';

function renderData(programmes, viewProgramme) {
  return programmes && programmes.data
    ? programmes.data.map((e) => (
      <li className="list-inline-item m-2" key={e.pk}>
        <DisplayCard programme={e} viewProgramme={viewProgramme} />
      </li>
    ))
    : [];
}

function unpackParam(key, param) {
  switch (key) {
    case 'title':
      return (JSON.parse(param) || []).map((e) => ({
        title: e,
      }));
    case 'university':
      return (JSON.parse(param) || []).map((e) => ({
        pk: e,
      }));
    case 'time':
      return (JSON.parse(param) || []).map((e) => ({
        pk: e,
      }));
    case 'study_form':
      return (JSON.parse(param) || []).map((e) => ({
        name: e,
      }));
    case 'pay':
      return (JSON.parse(param) || []).map((e) => ({
        pk: e,
      }));
    case 'study_type':
      return (JSON.parse(param) || []).map((e) => ({
        type: e,
      }));
    case 'degree':
      return (JSON.parse(param) || []).map((e) => e);
    default:
      return [];
  }
}

function pushParam(push, iterable, key, searchParams, page, perPage, view) {
  let query = '';
  let newVal = false;
  if (key === 'title') {
    newVal = true;
    query = query.concat(
      `title=${JSON.stringify(iterable.map((programme) => programme.title))}`,
    );
  } else if (searchParams.get('title') !== null) {
    query = query.concat(`title=${searchParams.get('title')}`);
  }

  if (key === 'university') {
    newVal = true;
    query = query.concat(
      `&university=${JSON.stringify(iterable.map((u) => u.pk))}`,
    );
  } else if (searchParams.get('university') !== null) {
    query = query.concat(`&university=${searchParams.get('university')}`);
  }

  if (key === 'time') {
    newVal = true;
    query = query.concat(`&time=${JSON.stringify(iterable.map((u) => u.pk))}`);
  } else if (searchParams.get('time') !== null) {
    query = query.concat(`&time=${searchParams.get('time')}`);
  }

  if (key === 'study_form') {
    newVal = true;
    query = query.concat(
      `&study_form=${JSON.stringify(iterable.map((u) => u.name))}`,
    );
  } else if (searchParams.get('study_form') !== null) {
    query = query.concat(`&study_form=${searchParams.get('study_form')}`);
  }

  if (key === 'pay') {
    newVal = true;
    query = query.concat(`&pay=${JSON.stringify(iterable.map((u) => u.pk))}`);
  } else if (searchParams.get('pay') !== null) {
    query = query.concat(`&pay=${searchParams.get('pay')}`);
  }

  if (key === 'study_type') {
    newVal = true;
    query = query.concat(
      `&study_type=${JSON.stringify(iterable.map((u) => u.type))}`,
    );
  } else if (searchParams.get('study_type') !== null) {
    query = query.concat(`&study_type=${searchParams.get('study_type')}`);
  }
  if (key === 'degree') {
    newVal = true;
    query = query.concat(
      `&degree=${JSON.stringify(iterable.map((u) => u))}`,
    );
  } else if (searchParams.get('degree') !== null) {
    query = query.concat(`&degree=${searchParams.get('degree')}`);
  }

  if (view !== null) {
    query = query.concat(`&view=${view}`);
  }

  query = query.concat(`&page=${newVal ? 1 : page}`);
  query = query.concat(`&count=${perPage}`);

  push(`/programmes?${query}/`);
}

export default function ProgrammeGrid() {
  const { push } = useHistory();
  const { search, pathname } = useLocation();

  let realSearch = search
    .replace('/', '')
    .replace('log-in', '')
    .replace('sign-up', '');
  if (pathname.includes('sign-up') || pathname.includes('log-in')) {
    realSearch = pathname
      .replace('/programmes?', '')
      .replace('/log-in', '')
      .replace('/sign-up', '')
      .replace('/', '');
  }
  console.log('realSearch', realSearch);

  const searchParams = new URLSearchParams(realSearch);

  const titleParam = searchParams.get('title');
  const universityParam = searchParams.get('university');
  const durationParam = searchParams.get('time');
  const studyFormParam = searchParams.get('study_form');
  const payParam = searchParams.get('pay');
  const studyTypeParam = searchParams.get('study_type');
  const degreeParam = searchParams.get('degree');

  const page = JSON.parse(searchParams.get('page')) || 1;
  const perPage = JSON.parse(searchParams.get('count')) || 30;

  const pushBackHist = useCallback((val, key) => pushParam(push, val, key, searchParams, page, perPage, null));
  const viewProgramme = useCallback((pk) => pushParam(push, null, null, searchParams, page, perPage, pk));

  const [programmes, setProgrammes] = useState();
  const [universities, setUniversities] = useState([]);
  const [payments, setPayments] = useState([]);
  const [studyForms, setStudyForms] = useState([]);
  const [studyTypes, setStudyTypes] = useState([]);
  const [aggregatedDuration, setAggregatedDuration] = useState([]);
  const [aggregatedProgrammes, setAggregatedProgrammes] = useState([]);
  const [degree, setDegree] = useState([]);

  const [setSelectedAggregatedProgrammes] = useModelSelect({
    type: 'creatable',
    initialData: aggregatedProgrammes,
    key: 'programmes',
    placeholder: 'Programmas',
    className: styles['default-filter-button'],
    getOptionValue: (option) => option.title,
    getOptionLabel: (option) => option.title,
    getNewOptionData: (val) => ({ title: val }),
    values: unpackParam('title', titleParam),
    setValues: (val) => pushBackHist(val, 'title'),
  });

  const unpackedUniversities = JSON.parse(universityParam);
  const [setSelectedUniversities] = useModelSelect({
    type: 'selectable',
    initialData: universities,
    key: 'university',
    placeholder: 'Universitāte',
    className: styles['default-filter-button'],
    values: unpackedUniversities
      ? universities.filter((el) => unpackedUniversities.includes(el.pk))
      : [],
    getOptionValue: (option) => option.pk,
    getOptionLabel: (option) => option.fields.name,
    setValues: (val) => pushBackHist(val, 'university'),
  });

  const unpackedDuration = JSON.parse(durationParam);
  const [setSelectedDurations] = useModelSelect({
    type: 'selectable',
    initialData: aggregatedDuration,
    key: 'duration',
    placeholder: 'Ilgums',
    className: styles['default-filter-button'],
    getOptionValue: (option) => option.pk,
    getOptionLabel: (option) => option.length,
    values: unpackedDuration
      ? aggregatedDuration.filter((el) => unpackedDuration.includes(el.pk))
      : [],
    setValues: (val) => pushBackHist(val, 'time'),
  });

  const [setSelectedStudyForms] = useModelSelect({
    type: 'selectable',
    initialData: studyForms,
    key: 'form',
    placeholder: 'Klātiene/neklātiene',
    className: styles['default-filter-button'],
    getOptionValue: (option) => option.name,
    getOptionLabel: (option) => option.name,
    values: unpackParam('study_form', studyFormParam),
    setValues: (val) => pushBackHist(val, 'study_form'),
  });
  const [setSelectedStudyTypes] = useModelSelect({
    type: 'selectable',
    initialData: studyTypes,
    key: 'type',
    placeholder: 'Pilna/nepilna laika',
    className: styles['default-filter-button'],
    getOptionValue: (option) => option.type,
    getOptionLabel: (option) => option.type,
    values: unpackParam('study_type', studyTypeParam),
    setValues: (val) => pushBackHist(val, 'study_type'),
  });
  const [setSelectedDegrees] = useModelSelect({
    type: 'selectable',
    initialData: degree,
    key: 'degree',
    placeholder: 'Grāds',
    className: styles['default-filter-button'],
    getOptionValue: (option) => option,
    getOptionLabel: (option) => option,
    values: unpackParam('degree', degreeParam),
    setValues: (val) => pushBackHist(val, 'degree'),
  });

  const unpackedPay = JSON.parse(payParam);
  const [setSelectedCost] = useModelSelect({
    type: 'selectable',
    initialData: payments,
    key: 'payments',
    placeholder: 'Maksa',
    className: styles['default-filter-button'],
    getOptionValue: (option) => option.pk,
    getOptionLabel: (option) => option.fields.type,
    values: unpackedPay
      ? payments.filter((el) => unpackedPay.includes(el.pk))
      : [],
    setValues: (val) => pushBackHist(val, 'pay'),
  });

  useEffect(() => {
    // Get initial data
    getAggregatedProgrammes(setAggregatedProgrammes);
    getAllUniversities(setUniversities);
    getPayment(setPayments);
    getAggregatedStudyForms(setStudyForms);
    getAggregatedStudyTypes(setStudyTypes);
    getAggregatedProgrammeDuration(setAggregatedDuration);
    getDegree(setDegree);
  }, []);

  useEffect(() => {
    const jsonQuery = {
      university: JSON.parse(universityParam)
        ? unpackParam('university', universityParam).map((school) => school.pk)
        : [],
      title: JSON.parse(titleParam)
        ? unpackParam('title', titleParam).map((programme) => programme.title)
        : [],
      pay_id: JSON.parse(payParam)
        ? unpackParam('pay', payParam).map((p) => p.pk)
        : [],
      study_form: JSON.parse(studyFormParam)
        ? unpackParam('study_form', studyFormParam).map((f) => f.name)
        : [],
      study_type: JSON.parse(studyTypeParam)
        ? unpackParam('study_type', studyTypeParam).map((f) => f.type)
        : [],
      time: JSON.parse(durationParam)
        ? unpackParam('time', durationParam).map((time) => time.pk)
        : [],
      degree: JSON.parse(degreeParam)
        ? unpackParam('degree', degreeParam).map((deg) => deg)
        : [],
    };
    getAllProgrammes(setProgrammes, perPage, page, jsonQuery);
  }, [
    titleParam,
    universityParam,
    durationParam,
    studyFormParam,
    payParam,
    studyTypeParam,
    degreeParam,
    page,
    perPage,
  ]);

  const data = renderData(programmes, viewProgramme);
  const paginate = programmes && (
    <ReactPaginate
      previousLabel="previous"
      nextLabel="next"
      breakLabel="..."
      pageCount={programmes.page.num_pages}
      marginPagesDisplayed={2}
      pageRangeDisplayed={10}
      disableInitialCallback
      forcePage={page - 1}
      onPageChange={(a) => {
        pushParam(
          push,
          null,
          null,
          searchParams,
          a.selected + 1,
          perPage,
          null,
        );
      }}
      containerClassName="pagination"
      pageLinkClassName="page-link"
      pageClassName={`page-item ${styles.isNotPopping}`}
      activeClassName="active"
      activeLinkClassName=" active"
      previousClassName="page-item"
      nextClassName="page-item"
      previousLinkClassName="page-link"
      nextLinkClassName="page-link"
      breakClassName="page-item"
      breakLinkClassName="page-link"
    />
  );
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col">
          <div className="card">
            <div className="card-body pb-0 mx-auto">
              <ul className="list-inline mt-3">
                <li
                  className="list-inline-item"
                  style={{ minWidth: '15rem', maxWidth: '20rem' }}
                >
                  {setSelectedUniversities}
                </li>
                <li
                  className="list-inline-item"
                  style={{ minWidth: '15rem', maxWidth: '20rem' }}
                >
                  {setSelectedAggregatedProgrammes}
                </li>
                <li
                  className="list-inline-item"
                  style={{ minWidth: '15rem', maxWidth: '20rem' }}
                >
                  {setSelectedCost}
                </li>
              </ul>
              <ul className="list-inline mt-3">
                <li
                  className="list-inline-item"
                  style={{ minWidth: '15rem', maxWidth: '20rem' }}
                >
                  {setSelectedStudyForms}
                </li>
                <li
                  className="list-inline-item"
                  style={{ minWidth: '15rem', maxWidth: '20rem' }}
                >
                  {setSelectedStudyTypes}
                </li>
                <li
                  className="list-inline-item"
                  style={{ minWidth: '15rem', maxWidth: '20rem' }}
                >
                  {setSelectedDurations}
                </li>
                <li
                  className="list-inline-item"
                  style={{ minWidth: '15rem', maxWidth: '20rem' }}
                >
                  {setSelectedDegrees}
                </li>
              </ul>
            </div>
            <div className="mx-auto">{paginate}</div>
          </div>
          <ul className="list-inline mt-3">{data}</ul>
          <div className="card">
            <div className="card-body pb-0 mx-auto">
              <div className="mx-auto">{paginate}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

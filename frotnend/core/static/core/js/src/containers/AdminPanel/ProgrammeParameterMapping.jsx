import React, { useState, useEffect } from 'react';

import Select from 'react-select';
import RequesterInstance from '../../misc/Requester';
import useInput from './commonHooks';
import ProgrammeRow from './ProgrammeParameterMapping/ProgrammeRow';
import fetchAllAttachedParameters from '../../../../../../../questions/static/questions/js/src/common/apiCalls';
import { getAllUniversities, getAllProgrammes } from '../../../../../../../education/static/education/js/src/common/apiCalls';


async function manageAttachedParameter(action, programmes) {
  const programmes_pks = programmes.map((el) => el.pk);
  switch (action.action) {
    case 'remove-value':
      result = await RequesterInstance.post('remove-parameter-from-programme', {
        programme_pk: programmes_pks,
        parameter_pk: action.removedValue.pk,
      });
      break;
    case 'select-option':
      result = await RequesterInstance.post('add-parameter-to-programme', {
        parameter_pk: action.option.pk,
        programme_pk: programmes_pks,
        score: 0,
      });
      break;

    default:
  }
}

function updateForLatvian(name) {
  return name
    .toLowerCase()
    .replace('ā', 'a')
    .replace('ž', 'z')
    .replace('č', 'c')
    .replace('ŗ', 'r')
    .replace('ļ', 'l')
    .replace('ķ', 'k')
    .replace('ē', 'e')
    .replace('ī', 'i')
    .replace('ņ', 'n')
    .replace('š', 's')
    .replace('ū', 'u')
    .replace('ŗ', 'r')
    .replace('ģ', 'g');
}

export default function ProgrammeParameterMapping({ parameters }) {
  const [programmes, setProgrammes] = useState(null);
  const [universities, setUniversities] = useState(null);
  const [activeParameters, setActiveParameters] = useState([]);
  const [searchText, changeSearchText] = useInput({
    type: 'text',
    className: 'form-control mb-2 mr-sm-2',
    placeholder: 'Programme',
  });
  useEffect(() => {
    getAllProgrammes(setProgrammes);
    getAllUniversities(setUniversities);
    fetchAllAttachedParameters(1, setActiveParameters);
  }, []);
  const searchLatvianIgnorant = updateForLatvian(searchText);

  const selectedParamPks = activeParameters.map((el) => el.fields.parameter);
  const selectedParamsObj = parameters.filter((el) => selectedParamPks.includes(el.pk));

  return (
    <div className="card">
      <div
        className="card-header"
        id="defineQuestions"
        data-toggle="collapse"
        data-target="#collapseProgrammeParam"
        aria-controls="collapseProgrammeParam"
      >
        <div className="mb-0">
          <h5 className="card-title">Programme management</h5>
          <h6 className="card-subtitle mb-2 text-muted">
            Map University Programmes with different Parameters
          </h6>
        </div>
      </div>
      <div
        id="collapseProgrammeParam"
        className="collapse"
        aria-labelledby="defineQuestions"
        data-parent="#accordionExample"
      >
        {parameters.length > 0 && (
          <Select
            className="mb-2"
            defaultValue={selectedParamsObj}
            placeholder="Parameter"
            onChange={(a, b) => {
              const newVal = a || [];
              setActiveParameters(newVal);
              manageAttachedParameter(b, programmes);
            }}
            options={parameters}
            isMulti
            getOptionValue={(option) => option.pk}
            getOptionLabel={(option) => option.fields.abbreviation}
          />
        )}
        {changeSearchText}
        <ul className="list-group">
          {programmes
            && programmes.map((el) => {
              const title = `${el.fields.title} ${
                universities[el.fields.university].fields.name
              }`;
              if (updateForLatvian(title).includes(searchLatvianIgnorant)) {
                return (
                  <ProgrammeRow
                    key={el.pk}
                    fields={el.fields}
                    pk={el.pk}
                    parameters={activeParameters}
                    university={universities[el.fields.university].fields.name}
                  />
                );
              }
              return null;
            })}
        </ul>
      </div>
    </div>
  );
}

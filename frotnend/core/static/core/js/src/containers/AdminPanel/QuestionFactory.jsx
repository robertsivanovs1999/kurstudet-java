import React, { useState } from 'react';

import Select from 'react-select';

import Requester from '../../misc/Requester';
import useInput from './commonHooks';

async function createNewQuestion(text, parameterId, parameterWeight) {
  await Requester.post('create-question', {
    text,
    parameter: parameterId,
    parameter_weight: parameterWeight,
  });
}

export default function QuestionFactory({
  parameters,
  questions,
  fetchAllBaseData,
}) {
  const [question, changeAbbreviation] = useInput({
    type: 'text',
    className: 'form-control mb-2 mr-sm-2',
    placeholder: 'Question',
  });
  const [selectedParameter, setSelectedParameter] = useState();
  return (
    <div className="card">
      <div
        className="card-header"
        id="defineQuestions"
        data-toggle="collapse"
        data-target="#collapseQuestion"
        aria-controls="collapseQuestion"
      >
        <div className="mb-0">
          <h5 className="card-title">Question Management</h5>
          <h6 className="card-subtitle mb-2 text-muted">
            Define questions here, then add them to Question Sets
          </h6>
        </div>
      </div>

      <div
        id="collapseQuestion"
        className="collapse"
        aria-labelledby="defineQuestions"
        data-parent="#accordionExample"
      >
        {parameters && (
          <div className="card-body">
            <h4>New Question</h4>
            {changeAbbreviation}
            <Select
              className="mb-2"
              placeholder="Parameter"
              onChange={(e) => setSelectedParameter(e.pk)}
              options={parameters}
              isClearable
              getOptionValue={(option) => option.pk}
              getOptionLabel={(option) => option.fields.abbreviation}
            />
            <button
              type="button"
              className="btn btn-primary mb-2"
              onClick={async () => {
                await createNewQuestion(question, selectedParameter, 100);
                await fetchAllBaseData();
              }}
            >
              Save
            </button>

            <h4>All Questions</h4>
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">TEXT</th>
                  <th scope="col">PARAMETER</th>
                  <th scope="col">PAR WEIGHT</th>
                  <th scope="col">CREATED</th>
                  <th scope="col">DISABLED</th>
                  <th scope="col">MANAGE</th>
                </tr>
              </thead>
              <tbody>
                {questions
                  && questions.map((el) => (
                    <tr key={el.pk}>
                      <th
                        scope="row"
                        className={el.fields.disabled_at ? 'table-dark' : ''}
                      >
                        {el.pk}
                      </th>
                      <td>{el.fields.text}</td>
                      <td>
                        {
                          parameters.find(
                            (par) => par.pk === el.fields.parameter,
                          ).fields.abbreviation
                        }
                      </td>
                      <td>{el.fields.parameter_weight}</td>
                      <td>{el.fields.date_created}</td>
                      <td>{el.fields.disabled_at}</td>
                      <td>
                        {el.fields.disabled_at ? (
                          <span className="badge badge-success">Enable</span>
                        ) : (
                          <span className="badge badge-danger">Disable</span>
                        )}
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        )}
      </div>
    </div>
  );
}

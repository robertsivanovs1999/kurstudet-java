import React, { useState, useEffect } from 'react';

import 'react-bootstrap-range-slider/dist/react-bootstrap-range-slider.css';
import RangeSlider from 'react-bootstrap-range-slider';
import RequesterInstance from '../../../misc/Requester';
import fetchAllAttachedParameters from '../../../../../../../../questions/static/questions/js/src/common/apiCalls';

async function updateScore(newScore, programmePk, parameterPk) {
  const result = await RequesterInstance.post(
    'update-parameter-for-programme',
    {
      programmePk,
      parameterPk,
      newScore,
    },
  );
}

function SliderComp({ initialValue, updateScore }) {
  const [value, setValue] = useState(Number(initialValue));
  return (
    <>
      <RangeSlider
        value={value}
        min={-100}
        max={100}
        onChange={(changeEvent) => setValue(Number(changeEvent.target.value))}
      />
      <button
        type="button"
        className="btn btn-primary"
        onClick={() => updateScore(value)}
      >
        Save
      </button>
    </>
  );
}

export default function ProgrammeRow({
  fields, pk, university, style,
}) {
  const dropdownKey = `progrPk${pk}`;
  const [visible, setVisible] = useState(false);

  const [attachedParams, setAttachedParams] = useState([]);

  useEffect(() => {
    if (visible) {
      fetchAllAttachedParameters(pk, setAttachedParams);
    }
  }, [visible]);

  return (
    <li style={style} key={pk} className="list-group-item">
      <div className="text-left">{fields.title}</div>
      <span className="badge badge-pill badge-info">{university}</span>
      <button type="button" onClick={() => setVisible(!visible)}>
        {visible ? '>>' : '<<'}
      </button>
      <div className={`collapse ${visible ? 'show' : ''}`} id={dropdownKey}>
        <div className="card card-body">
          <ul className="list-group">
            {attachedParams.map((el) => (
              <li className="list-group-item" key={el.pk}>
                {el.fields.parameter_name}
                <SliderComp
                  initialValue={el.fields.score}
                  updateScore={(e) => updateScore(e, pk, el.fields.parameter)}
                />
              </li>
            ))}
          </ul>
        </div>
      </div>
    </li>
  );
}

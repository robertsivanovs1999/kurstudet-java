import React, { useState } from 'react';

export default function useInput({
  type, className, placeholder, extra,
}) {
  const [value, setValue] = useState('');
  const input = (
    <input
      value={value}
      placeholder={placeholder}
      className={className}
      onChange={(e) => setValue(e.target.value)}
      type={type}
      {...extra}
    />
  );
  return [value, input];
}

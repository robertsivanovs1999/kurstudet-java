import React from 'react';

import Requester from '../../misc/Requester';
import useInput from './commonHooks';

async function createNewParameter(abbreviation, explanation) {
  await Requester.post('create-parameter', {
    abbreviation,
    explanation,
  });
}

export default function ParameterFactory({ parameters, fetchAllBaseData }) {
  const [abbreviation, changeAbbreviation] = useInput({
    type: 'text',
    className: 'form-control mb-2 mr-sm-2',
    placeholder: 'Abbreviation',
  });
  const [explanation, changeExplanation] = useInput({
    type: 'text',
    className: 'form-control',
    placeholder: 'Explanation',
  });

  return (
    <div className="card">
      <div
        className="card-header"
        id="headingOne"
        data-toggle="collapse"
        data-target="#collapseOne"
        aria-controls="collapseOne"
      >
        <div className="mb-0">
          <h5 className="card-title">Parameters Management</h5>
          <h6 className="card-subtitle mb-2 text-muted">
            Parameters are used for evaluating given answers
          </h6>
        </div>
      </div>

      <div
        id="collapseOne"
        className="collapse"
        aria-labelledby="headingOne"
        data-parent="#accordionExample"
      >
        <div className="card-body">
          <h4>New parameter</h4>
          <form className="form-inline">
            {changeAbbreviation}
            <div className="input-group mb-2 mr-sm-2">{changeExplanation}</div>
            <button
              type="button"
              className="btn btn-primary mb-2"
              onClick={async () => {
                await createNewParameter(abbreviation, explanation);
                await fetchAllBaseData();
              }}
            >
              Save
            </button>
          </form>
          <h4>All parameters</h4>
          <table className="table">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">ABBR</th>
                <th scope="col">EXPLANATION</th>
                <th scope="col">CREATED</th>
                <th scope="col">DISABLED</th>
                <th scope="col">MANAGE</th>
              </tr>
            </thead>
            <tbody>
              {parameters &&
                parameters.map((el) => (
                  <tr
                    key={el.pk}
                    className={el.fields.disabled_at ? 'table-dark' : ''}
                  >
                    <th scope="row">{el.pk}</th>
                    <td>{el.fields.abbreviation}</td>
                    <td>{el.fields.explanation}</td>
                    <td>{el.fields.date_created}</td>
                    <td>{el.fields.disabled_at}</td>
                    <td>
                      {el.fields.disabled_at ? (
                        <span className="badge badge-success">Enable</span>
                      ) : (
                        <span className="badge badge-danger">Disable</span>
                      )}
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

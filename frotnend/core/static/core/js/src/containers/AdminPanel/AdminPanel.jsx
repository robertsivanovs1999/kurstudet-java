import React, { useState, useEffect } from 'react';

import ParameterFactory from './ParameterFactory';
import QuestionFactory from './QuestionFactory';
import QuestionSetsFactory from './QuestionSetFactory';

import { loadQuestions, loadParameters } from '../../../../../../../questions/static/questions/js/src/common/apiCalls';
import ProgrammeParameterMapping from './ProgrammeParameterMapping';

function fetchAllBaseData(setParameters, setQuestions) {
  loadQuestions(setQuestions);
  loadParameters(setParameters);
}

function AdminPanel() {
  const [parameters, setParameters] = useState([]);
  const [questions, setQuestions] = useState(null);
  const fetchBaseDataWrapper = () => {
    fetchAllBaseData(setParameters, setQuestions);
  };

  useEffect(() => {
    fetchBaseDataWrapper();
  }, []);

  return (
    <>
      <div className="accordion" id="accordionExample">
        <ParameterFactory
          parameters={parameters}
          fetchAllBaseData={fetchBaseDataWrapper}
        />
        <QuestionFactory
          parameters={parameters}
          questions={questions}
          fetchAllBaseData={fetchBaseDataWrapper}
        />
        <QuestionSetsFactory allQuestions={questions} parameters={parameters} />
        <ProgrammeParameterMapping parameters={parameters} />
      </div>
    </>
  );
}

AdminPanel.propTypes = {};

export default AdminPanel;

import React, { useState, useEffect } from 'react';

import Select from 'react-select';
import {
  sortableContainer,
  sortableElement,
  sortableHandle,
} from 'react-sortable-hoc';

import arrayMove from 'array-move';

import Requester from '../../misc/Requester';
import useInput from './commonHooks';
import { loadQuestionSets, getQuestionsForQuestionSet } from '../../../../../../../questions/static/questions/js/src/common/apiCalls';

const DragHandle = sortableHandle(() => (
  <span className="btn btn-secondary mr-2">::</span>
));

const SortableQuestion = sortableElement(({ value }) => (
  <tr>
    <td>
      <DragHandle />
    </td>
    <td>{value.fields.text}</td>
    <td>{value.fields.parameter}</td>
    <td>{value.fields.parameter_weight}</td>
  </tr>
));

const SortableContainer = sortableContainer(({ children }) => (
  <ul className="list-group">{children}</ul>
));

async function createNewQuestionSet(name) {
  await Requester.post('create-question-set', {
    name,
  });
}

async function pushBackQuestion({ questionSetId, questionId }) {
  const result = await Requester.post('add-question-to-question-set', {
    questionSetId,
    questionId,
  });
}

async function onSortEnd(oldIndex, newIndex, items, questionSet, callback) {
  const movedArray = arrayMove(items, oldIndex, newIndex);
  const result = await Requester.post('move-question-in-question-set', {
    questionSet,
    oldIndex,
    newIndex,
  });
  callback(JSON.parse(result.data.data));
}



export default function QuestionSetFactory({ allQuestions, parameters }) {
  const [newQuestionSet, setNewQuestionSet] = useInput({
    type: 'text',
    className: 'form-control mb-2 mr-sm-2',
    placeholder: 'Question Set Name',
  });
  const [questions, setQuestions] = useState();
  const [selectedQuestionSet, changeSelectedQuestionSet] = useState(null);
  const [typedInQuestionSet, setTypedInQuestionSet] = useState('');
  const [questionSets, setQuestionSets] = useState();
  const [selectedNewQuestionForQS, setSelectedNewQuestionForQS] = useState();

  useEffect(() => {
    loadQuestionSets(typedInQuestionSet, setQuestionSets);
  }, [typedInQuestionSet]);

  useEffect(() => {
    if (selectedQuestionSet) {
      getQuestionsForQuestionSet(selectedQuestionSet, setQuestions);
    }
  }, [selectedQuestionSet]);
  return (
    <div className="card">
      <div
        className="card-header collapsed"
        id="headingTwo"
        data-toggle="collapse"
        data-target="#collapseTwo"
        aria-controls="collapseTwo"
      >
        <div className="mb-0">
          <h5 className="card-title">Select Question Set</h5>
          <h6 className="card-subtitle mb-2 text-muted">
            Each question set contains multiple questions
          </h6>
        </div>
      </div>

      <div
        id="collapseTwo"
        className="collapse"
        aria-labelledby="headingTwo"
        data-parent="#accordionExample"
      >
        <div className="card-body" style={{ minHeight: '50rem' }}>
          <h4>New Question Set</h4>
          {setNewQuestionSet}
          <button
            type="button"
            className="btn btn-primary mb-2"
            onClick={async () => {
              await createNewQuestionSet(newQuestionSet);
              await loadQuestionSets(typedInQuestionSet, setQuestionSets);
              setTypedInQuestionSet('');
            }}
          >
            Save
          </button>

          <h4>Manage old Question Sets</h4>
          <Select
            defaultOptions
            options={questionSets}
            inputValue={typedInQuestionSet}
            getOptionValue={(option) => option.pk}
            getOptionLabel={(option) => option.fields.name}
            onInputChange={(text) => setTypedInQuestionSet(text)}
            onChange={(opt) => changeSelectedQuestionSet(opt.pk)}
          />
          {selectedQuestionSet && parameters && questions && (
            <>
              <h4>Questions inside the Question Set</h4>

              <SortableContainer
                onSortEnd={(props) => {
                  onSortEnd(
                    props.oldIndex,
                    props.newIndex,
                    questions,
                    selectedQuestionSet,
                    setQuestions,
                  );
                }}
                useDragHandle
                lockAxis="y"
              >
                <table className="table">
                  <thead>
                    <tr>
                      <th scope="col">Change Order</th>
                      <th scope="col">Text</th>
                      <th scope="col">Parameter</th>
                      <th scope="col">Parameter Weight</th>
                    </tr>
                  </thead>
                  <tbody>
                    {questions.map((value, index) => (
                      <SortableQuestion
                        key={`item-${value.pk}`}
                        index={index}
                        value={{
                          ...value,
                          fields: {
                            ...value.fields,
                            parameter: parameters.filter(
                              (el) => el.pk === value.fields.parameter,
                            )[0].fields.abbreviation,
                          },
                        }}
                      />
                    ))}
                  </tbody>
                </table>
              </SortableContainer>
              <h4>Add New Question to Question Set</h4>
              <Select
                defaultOptions
                options={
                  questions
                    ? allQuestions.filter(
                      (q) => !questions.map((el) => el.pk).includes(q.pk),
                    )
                    : allQuestions
                }
                getOptionValue={(option) => option.pk}
                getOptionLabel={(option) => option.fields.text}
                onChange={(opt) => setSelectedNewQuestionForQS(opt.pk)}
              />
              <button
                type="button"
                className="btn btn-primary mb-2"
                onClick={async () => {
                  await pushBackQuestion({
                    questionSetId: selectedQuestionSet,
                    questionId: selectedNewQuestionForQS,
                  });
                  await getQuestionsForQuestionSet(
                    selectedQuestionSet,
                    setQuestions,
                  );
                }}
              >
                Add Question to Question Set
              </button>
            </>
          )}
        </div>
      </div>
    </div>
  );
}

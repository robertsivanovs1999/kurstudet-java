import React, { useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import InputForm from '../components/InputForm';
import LongButton from '../components/Misc/LongButton';
import Alert from '../components/Misc/Alert';
import { UserContext } from '../contexts/UserContext';
import style from '../components/style/form.scss';

export function handleChange(e, cb) {
  cb(e.target.value);
}

async function handleLogInClick(
  setCode,
  username,
  password,
  logInCb,
) {
  logInCb(username, password, (response) => {
    // TODO perform some kind of response validation
    setCode(response.status);
  });
}

export default function LogIn() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [code, setCode] = useState();
  const { logIn } = useContext(UserContext);

  let alert = null;
  switch (code) {
    case 200:
      alert = <Alert Success>Successfully logged in</Alert>;
      break;
    case 400:
      alert = <Alert Warning>Incorrect login!</Alert>;
      break;
    case 404:
    case 500:
      alert = <Alert Danger>There&apos;s a problem with the server!</Alert>;
      break;
    default:
      alert = <Alert />;
      break;
  }
  return (
    <>
      {alert}
      <form className={style['form-login']}>
        <InputForm
          value={username}
          onChange={(e) => handleChange(e, setUsername)}
          id="formBasicUsername"
          label="Lietotājvārds"
          inputType="text"
          placeholder="Mans lietotājvārds"
          name="username"
        />
        <InputForm
          value={password}
          onChange={(e) => handleChange(e, setPassword)}
          id="formBasicPassword1"
          label="Parole"
          inputType="password"
          placeholder="⚬⚬⚬⚬⚬⚬⚬"
          name="password"
        />
        <LongButton
          onClick={() => handleLogInClick(setCode, username, password, logIn)}
        >
          Pieslēgties
        </LongButton>
        <div className={style['container-bottom']}>
          Vēl nav profila?
          <Link
            to={{
              pathname: '/sign-up/',
            }}
          >
            <button type="button" className={style['btn-signup']}>
              Reģistrējies šeit!
            </button>
          </Link>
        </div>
      </form>
      <div className={style.background} />
    </>
  );
}

import React from 'react';
// import HeaderManager from './HeaderManager';
import Main from '../components/Page/Main';
import Footer from '../components/Page/Footer';
import UserContextProvider from '../contexts/UserContext';
import ModalContextProvider from '../contexts/MainModalContext';

import styles from '../../../style/base.scss';
import Header from "../components/Page/Header";

export default function App() {
  return (
    <>
      <ModalContextProvider>
        <UserContextProvider>
          <div className={styles.background}>
            <Header />
            {/* <HeaderManager /> */}
            <Main />
            {/* <Footer /> */}
          </div>
        </UserContextProvider>
      </ModalContextProvider>
    </>
  );
}

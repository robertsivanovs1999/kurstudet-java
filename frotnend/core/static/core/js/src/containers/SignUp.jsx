import React, { useContext, useState } from 'react';
import InputForm from '../components/InputForm';
import LongButton from '../components/Misc/LongButton';
import Modal from '../components/Modal/Modal';
import Alert from '../components/Misc/Alert';
import Requester from '../misc/Requester';
import { handleChange } from './LogIn';
import { UserContext } from '../contexts/UserContext';
import { ModalContext } from '../contexts/MainModalContext';
import style from '../components/style/form.scss';

async function handleRegisterClick(
  email,
  username,
  password1,
  password2,
  successCallBack,
  setCode,
  setErrors,
) {
  const response = await Requester.post('register', {
    email,
    username,
    password1,
    password2,
  });
  if (response.status === 200) {
    successCallBack();
  } else {
    setErrors(response.data.errors);
    setCode(response.status);
  }
}

export default function SignUp() {
  const [email, setEmail] = useState('');
  const [code, setCode] = useState(null);
  const [errors, setErrors] = useState([]);
  const [username, setUsername] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const { logIn } = useContext(UserContext);
  // const { toggleModal, toggleOffComponent } = useContext(ModalContext);

  let alert = null;
  switch (code) {
    case 400:
      alert = (
        <Alert Warning>
          {errors.map(([key, val]) => (
            <div>{`${key}: ${val}`}</div>
          ))}
        </Alert>
      );
      break;
    case 404:
    case 500:
      alert = <Alert Danger>There&apos;s a problem with the server!</Alert>;
      break;
    default:
      alert = <Alert />;
      break;
  }
  return (
    <>
      {/* {toggleOffComponent} */}
      {alert}
      <form className={style['form-signup']}>
        <InputForm
          value={email}
          onChange={(e) => handleChange(e, setEmail)}
          id="formBasicEmail"
          label="Epasts"
          inputType="email"
          placeholder="mans@epasts.lv"
          name="email"
        />
        <InputForm
          value={username}
          onChange={(e) => handleChange(e, setUsername)}
          id="formBasicUsername"
          label="Lietotājvārds"
          inputType="text"
          placeholder="Mans lietotājvārds"
          name="username"
        />
        <InputForm
          value={password1}
          onChange={(e) => handleChange(e, setPassword1)}
          id="formBasicPassword1"
          label="Parole"
          inputType="password"
          placeholder="⚬⚬⚬⚬⚬⚬⚬"
          name="password1"
        />
        <InputForm
          value={password2}
          onChange={(e) => handleChange(e, setPassword2)}
          id="formBasicPassword2"
          label="Atkārtota parole"
          inputType="password"
          placeholder="⚬⚬⚬⚬⚬⚬⚬"
          name="password2"
        />
        <LongButton
          className={style['btn-custom']}
          onClick={() => handleRegisterClick(
            email,
            username,
            password1,
            password2,
            () => {
              logIn(username, password1, () => null);
            },
            setCode,
            setErrors,
          )}
        >
          Reģistrēties
        </LongButton>
      </form>
      <div className={style.background} />
    </>
  );
}

import React, { useState, useCallback, useEffect } from 'react';
import { animated, useSpring } from 'react-spring';

import { useHistory } from 'react-router-dom';
import zoom1 from '../../../images/misc/zoom1.png';

import style from '../../../style/header.scss';

// The states to animate between
const small = {
  width: '3.5rem',
};
const expanded = {
  width: '16rem',
};


export default function SearchBar() {
  const { push } = useHistory();
  const [isExpanded, setIsExpanded] = useState(false);
  const [inputText, setInputText] = useState('');
  const [visibleInput, setVisibleInput] = useState(false);
  let animatedStyling = { ...small };
  const onClick = useCallback(() => {
    if (!isExpanded === false) {
      setVisibleInput(false);
    }
    setIsExpanded(!isExpanded);
  });

  if (isExpanded) {
    animatedStyling = { ...animatedStyling, ...expanded };
  }
  const props = useSpring({
    ...animatedStyling,
    onRest: () => {
      if (isExpanded) {
        setVisibleInput(isExpanded);
      }
    },
    config: { 
      tension: 400,
      friction: 25,
      mass: 0.1,
    },
  });
  return (
    <animated.span
      style={props}
      className={`${style.search} align-left align-items-start text-left`}
    >
      <div className={`row ${style.clickable}`} onClick={onClick}>
        <div
          className={`col-3 pr-0 ${style.clickable}`}
          role="button"
          onClick={onClick}
        >
          <img alt="Search" src={zoom1} role="presentation" onClick={onClick} />
        </div>
        <div className="col-8 pl-0">
          {visibleInput && (
            <li className="list-inline-item">
              <input
                onKeyDown={(e) => {
                  if (e.key === 'Enter') {
                    push(`/programmes?title=["${inputText}"]`);
                  }
                }}
                value={inputText}
                onChange={(e) => setInputText(e.target.value)}
                placeholder="Meklēt progr."
                type="text"
                id="fname"
                name="fname"
                maxLength="50"
                ref={(input) => {
                  if (input !== null) {
                    input.focus();
                  }
                }}
              />
            </li>
          )}
        </div>
      </div>
    </animated.span>
  );
}

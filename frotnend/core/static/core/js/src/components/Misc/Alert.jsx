import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import style from '../style/form.scss';

export default class Alert extends PureComponent {
  render() {
    const {
      children, Success, Warning, Primary, Danger, Info,
    } = this.props;
    let type;

    if (Primary) {
      type = 'primary';
    } else if (Warning) {
      type = 'warning';
    } else if (Success) {
      type = 'success';
    } else if (Danger) {
      type = 'danger';
    } else if (Info) {
      type = 'info';
    }

    return (
      <div className={`alert alert-${type}`} role="alert">
        {children}
      </div>
    );
  }
}

Alert.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  Success: PropTypes.bool,
  Warning: PropTypes.bool,
  Danger: PropTypes.bool,
  Primary: PropTypes.bool,
  Info: PropTypes.bool,
};

Alert.defaultProps = {
  children: null,
  Success: false,
  Warning: false,
  Danger: false,
  Primary: false,
  Info: false,
};

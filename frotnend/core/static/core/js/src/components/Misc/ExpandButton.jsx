import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default class ExpandButton extends PureComponent {
  render() {
    const { dropDownItems, id, onClick } = this.props;
    return (
      <div className="btn-group" role="group">
        <button
          id={id}
          type="button"
          className="btn btn-secondary"
          data-toggle="dropdown"
        >
          ...
        </button>
        {dropDownItems.length > 0 ? (
          <div className="dropdown-menu" aria-labelledby={id}>
            {dropDownItems.map((button) => (
              <button
                key={button.name}
                type="button"
                className="dropdown-item"
                name={button.name}
                onClick={onClick}
              >
                {button.text}
              </button>
            ))}
          </div>
        ) : null}
      </div>
    );
  }
}

ExpandButton.propTypes = {
  id: PropTypes.string,
  dropDownItems: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string,
      name: PropTypes.string,
    }),
  ),
  onClick: PropTypes.func,
};

ExpandButton.defaultProps = {
  id: 'btnGroupDrop1',
  dropDownItems: [],
  onClick: () => null,
};

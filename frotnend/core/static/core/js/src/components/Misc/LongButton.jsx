import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import style from '../style/form.scss';

export default class LongButton extends PureComponent {
  render() {
    const { className, onClick, children } = this.props;
    return (
      <button
        type="button"
        onClick={onClick}
        className={style['btn-custom']}
      >
        {children}
      </button>
    );
  }
}

LongButton.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  className: PropTypes.string,
  onClick: PropTypes.func,
};

LongButton.defaultProps = {
  children: null,
  onClick: () => null,
  className: 'btn-primary',
};

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import stylePropType from 'react-style-proptype';

export default class Input extends PureComponent {
  render() {
    const {
      placeholder,
      onChange,
      children,
      roundLeftTop,
      roundLeftBottom,
      roundRightTop,
      roundRightBottom,
    } = this.props;
    let { style } = this.props;

    if (roundLeftBottom) {
      style = { ...style, borderBottomLeftRadius: 50 };
    }

    if (roundLeftTop) {
      style = { ...style, borderTopLeftRadius: 50 };
    }


    if (roundRightTop) {
      style = { ...style, borderTopRightRadius: 50 };
    }

    if (roundRightBottom) {
      style = { ...style, borderBottomRightRadius: 50 };
    }
    return (
      <>
        <span className="input-group">
          <input
            type="text"
            className="form-control"
            placeholder={placeholder}
            onChange={onChange}
            style={style}
          />
          <span className="input-group-addon">{children}</span>
        </span>
      </>
    );
  }
}

Input.propTypes = {
  // Placeholder info on the text field
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  // Css styling for the input button
  style: stylePropType,
  // The JSX elements to display on the right side of the input button
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  roundLeftTop: PropTypes.bool,
  roundLeftBottom: PropTypes.bool,
  roundRightTop: PropTypes.bool,
  roundRightBottom: PropTypes.bool,
};

Input.defaultProps = {
  placeholder: '',
  onChange: () => null,
  style: null,
  children: null,
  roundLeftTop: false,
  roundLeftBottom: false,
  roundRightTop: false,
  roundRightBottom: false,
};

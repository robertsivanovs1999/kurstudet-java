import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';

import instaLogo from '../../../../images/SocialMediaLogo/instagramLogo.png';
import fbLogo from '../../../../images/SocialMediaLogo/facebookLogo.png';

const footerStyle = {
  fontFamily: 'Alegreya',
  height: '4em',
  width: '100%',
  textAlign: 'center',
  background: '#405952',
  color: '#FFFFFF',
};

const logoStyle = {
  ...footerStyle,
  background: '#FFFFFF',
  width: '2em',
  height: '2em',
  borderRadius: '50%',
};

export default class Footer extends PureComponent {
  render() {
    return (
      <footer style={footerStyle} className="container-fluid fixed-bottom">
        <div className="row pt-2">
          <ul className="col">
            <Link to="/">
              <button type="button" className="btn btn-link text-white alert-heading">
                <b>KurStudet.lv</b>
              </button>
            </Link>
          </ul>
          <ul className="list-inline">
            <Link to="/about">
              <li className="list-inline-item">
                <button type="button" className="btn btn-link text-white alert-heading">
                  <b>Par mums</b>
                </button>
              </li>
            </Link>
            <Link to="/rules">
              <li className="list-inline-item">
                <button type="button" className="btn btn-link text-white alert-heading">
                  <b>Lietošanas noteikumi</b>
                </button>
              </li>
            </Link>
            <Link to="/privacy">
              <li className="list-inline-item">
                <button type="button" className="btn btn-link text-white alert-heading">
                  <b>Privātums</b>
                </button>
              </li>
            </Link>
          </ul>
          <div className="col">
            <ul className="list-inline">
              <li className="list-inline-item">
                <button type="button" className="btn btn-link text-white alert-heading">
                  <img
                    src={fbLogo}
                    style={logoStyle}
                    title="Check out our Instagram!"
                    alt="Instagram logo, link"
                  />
                </button>
              </li>
              <li className="list-inline-item">
                <button type="button" className="btn btn-link text-white alert-heading">
                  <img
                    src={instaLogo}
                    style={logoStyle}
                    title="Check out our Instagram!"
                    alt="Instagram logo, link"
                  />
                </button>
              </li>
            </ul>
            <span className="pl-3" />
          </div>
        </div>
      </footer>
    );
  }
}

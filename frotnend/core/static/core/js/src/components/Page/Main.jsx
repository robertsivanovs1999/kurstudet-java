import React, { useContext, useEffect } from 'react';

import {
  Switch,
  Route,
  useLocation,
  useHistory,
  Redirect,
  useParams,
} from 'react-router-dom';

import AdminPanel from '../../containers/AdminPanel/AdminPanel';
import TestManager from '../../../../../../../questions/static/questions/js/src/containers/TestManager';
import ProgrammeGrid from '../../../../../../../education/static/education/js/src/containers/ProgrammeGrid';
import AboutPage from '../AboutPage';
import SignUp from '../../containers/SignUp';
import LogIn from '../../containers/LogIn';
import ModalContent from '../Modal/ModalContent';

import { ModalContext } from '../../contexts/MainModalContext';
import { UserContext } from '../../contexts/UserContext';
import ProgrammeInfo from '../../../../../../../education/static/education/js/src/components/ProgrammeInfo';
import IndexPage from '../IndexPage';
import ResultDisplay from "../../../../../../../questions/static/questions/js/src/containers/ResultDisplay";

const placeholderPrivacy = () => <div>Privacy</div>;
const placeholderRules = () => <div>Rules</div>;
const placeholderSearch = () => <div>search</div>;
const placeholderUniversities = () => <div>universities</div>;
const placeholderManage = () => (
  <>
    <AdminPanel />
  </>
);
const PlaceholderProfile = () => <div>profile</div>;

export default function MainLayout() {
  const location = useLocation();
  const history = useHistory();

  const { logOut } = useContext(UserContext);
  const {
    modalComponent,
    setModalData,
    toggleModal,
    setCallbackOnExit,
  } = useContext(ModalContext);

  useEffect(() => {
    setModalData(
      <ModalContent>
        <Route
          path="/programmes/:search?/"
          render={({ match, location }) => {

            // Extract get params
            const searchParams = new URLSearchParams(location.search);
            if (searchParams.has('view')) {
              setCallbackOnExit(() => () => {
                history.push(
                  location.pathname
                    + location.search
                      .replace(`&view=${searchParams.get('view')}`, ''),
                );
              });
              toggleModal();
              return <ProgrammeInfo programmePk={searchParams.get('view')} />;
            }
          }}
        />
      </ModalContent>,
    );
    // }
  }, [location.pathname]);

  return (
    <main className="container-fluid pl-0 pr-0 mr-0 ml-0">
      <div className="mx-auto text-center">
        <Switch>
          <Route path="/test/:questionSet?" component={TestManager} />
          <Route exact path="/privacy/" component={placeholderPrivacy} />
          <Route exact path="/rules/" component={placeholderRules} />
          <Route path="/programmes:modal?" component={ProgrammeGrid} />
          <Route path="/search/" component={placeholderSearch} />
          <Route path="/universities/" component={placeholderUniversities} />
          <Route path="/manage/" component={placeholderManage} />
          <Route path="/profile/" render={() => window.location.reload(false)} />
          <Route path="/result/:pk?" component={ResultDisplay} />
          <Route path="/admin/" render={() => window.location.reload(false)} />
          <Route path="/log-in/" component={LogIn} />
          <Route path="/sign-up/" component={SignUp} />
          <Route
            path="/logout/"
            render={() => {
              logOut();
              return <Redirect to="/" />;
            }}
          />
          <Route path="/about/" component={AboutPage} />
          <Route path="/" component={IndexPage} />
        </Switch>
        {modalComponent}
      </div>
    </main>
  );
}

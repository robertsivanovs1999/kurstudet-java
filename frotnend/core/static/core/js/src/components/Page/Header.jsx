import React, { useContext } from 'react';
import { Link, useLocation, NavLink } from 'react-router-dom';

import logo from '../../../../images/logo-sm.png';
import style from '../../../../style/header.scss';

import { UserContext } from '../../contexts/UserContext';

import SearchBar from '../SearchBar';

export default function Header() {
  const { isLoggedIn, isStaff, isSuperuser } = useContext(UserContext);

  return (
    <nav
      className={`navbar navbar-expand-lg navbar-light bg-light p-0 ${style.navbar}`}
    >
      <button
        className={`navbar-toggler ${style["navbar-toggler"]}`}
        type="button"
        data-toggle="collapse"
        data-target="#navbarToggler"
        aria-controls="navbarToggler"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse" id="navbarToggler">
        <div className="container-fluid">
          <div className="row flex-grow-1 align-items-end">
            <div className="col text-center" style={{width: '20px'}}>
              <img
                src={logo}
                alt="KURSTUDET"
                className="d-inline-block align-top"
              />
            </div>
              <div className="row flex-grow-1">
                  <ul className="list-inline mb-1">
                    <li className="list-inline-item">
                      <NavLink
                        exact
                        to="/"
                        activeClassName={style['selected-navbar-btn']}
                        className={`${style['not-selected-navbar-btn']} ${style['nav-link']}`}
                      >
                        Sākums
                      </NavLink>
                    </li>
                    <li className="list-inline-item">
                      <NavLink
                        to="/programmes/"
                        activeClassName={style['selected-navbar-btn']}
                        className={`${style['not-selected-navbar-btn']} ${style['nav-link']}`}
                      >
                        Programmas
                      </NavLink>
                    </li>
                    <li className="list-inline-item">
                      <NavLink
                        to="/test/"
                        activeClassName={style['selected-navbar-btn']}
                        className={`${style['not-selected-navbar-btn']} ${style['nav-link']}`}
                      >
                        Karjeras tests
                      </NavLink>
                    </li>
                    <li className="list-inline-item">
                      <NavLink
                        to="/about/"
                        activeClassName={style['selected-navbar-btn']}
                        className={`${style['not-selected-navbar-btn']} ${style['nav-link']}`}
                      >
                        Par
                      </NavLink>
                    </li>
                  </ul>
                {/* </div> */}
              </div>
            </div>
            <div
              className={`col text-center align-middle mx-auto ${style["search-container"]}`}
            >
              <div className="">
                <SearchBar />
                {!isLoggedIn ? (
                  <Link
                    to="/log-in/"
                  >
                    <button type="button" className="btn ">
                    Pieslēgties
                    </button>
                  </Link>
                ) : (
                  <div
                    className="btn-group"
                    role="group"
                    aria-label="Basic example"
                  >
                    <Link
                      to={{
                        pathname: '/profile/',
                      }}
                    >
                      <button type="button" className="btn ">
                        Profils <i className="fas fa-user" />
                      </button>
                    </Link>
                    <Link
                      to={{
                        pathname: '/logout/',
                      }}
                    >
                      <button type="button" className="btn ">
                        Atslēgties <i className="fas fa-door-closed" />
                      </button>
                    </Link>
                    {(isStaff || isSuperuser) && (
                      <Link
                        to={{
                          pathname: '/admin/',
                        }}
                      >
                        <button type="button" className="btn ">
                          Admin <i className="fas fa-user-shield" />
                        </button>
                      </Link>
                    )}
                  </div>
                )}
              </div>
            </div>
          </div>
          </div>
    </nav>
  );
}

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default class ModalContent extends PureComponent {
  render() {
    const { children } = this.props;
    return <div className="modal-content">{children}</div>;
  }
}

ModalContent.propTypes = {
  children: PropTypes.node,
};

ModalContent.defaultProps = {
  children: null,
};

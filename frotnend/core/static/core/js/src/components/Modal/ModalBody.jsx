import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default class ModalBody extends PureComponent {
  render() {
    const { children } = this.props;
    return (
      <div className="modal-body">
        {children}
      </div>
    );
  }
}

ModalBody.propTypes = {
  children: PropTypes.element,
};

ModalBody.defaultProps = {
  children: null,
};

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default class ModalHeader extends PureComponent {
  render() {
    const { id, close } = this.props;
    return (
      <div className="modal-header">
        <h5 className="modal-title" id={id}>
          Modal title
        </h5>
        {close ? (
          <button
            type="button"
            className="close"
            data-dismiss="modal"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
        ) : null}
      </div>
    );
  }
}

ModalHeader.propTypes = {
  id: PropTypes.string.isRequired,
  close: PropTypes.bool,
};

ModalHeader.defaultProps = {
  close: true,
};

import React, { useState, useEffect } from 'react';

function ModalComp({ children, id }) {
  return (
    <div
      className="modal fade"
      id={id}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
      data-backdrop="static"
    >
      <div className="modal-dialog modal-xl" role="document">
        {children}
      </div>
    </div>
  );
}

function useModalHook(initialData) {
  const [modalData, setModalData] = useState(initialData);
  const [isVisible, setIsVisible] = useState(false);

  const [callbackOnExit, setCallbackOnExit] = useState(() => () => null);
  useEffect(() => {
    if (isVisible) {
      $('#mainModal').modal('show');
    } else {
      $('#mainModal').modal('hide');
      callbackOnExit();
    }
  }, [isVisible]);
  const toggleModal = () => setIsVisible(!isVisible);
  const toggleOffComponent = (
    <button
      type="button"
      className="close"
      onClick={toggleModal}
      data-dismiss="modal"
      aria-label="Close"
    >
      <span aria-hidden="true">&times;</span>
    </button>
  );
  const modalComponent = (
    <ModalComp id="mainModal">
      {modalData}
    </ModalComp>
  );


  return [
    modalComponent,
    toggleModal,
    setModalData,
    setCallbackOnExit,
    toggleOffComponent,
  ];
}


export default useModalHook;

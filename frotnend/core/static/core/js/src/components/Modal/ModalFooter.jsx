import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default class ModalFooter extends PureComponent {
  render() {
    const { closeModalButton, children } = this.props;
    return (
      <div className="modal-footer">
        {closeModalButton ? (
          <button
            type="button"
            className="btn btn-secondary"
            data-dismiss="modal"
          >
          Close
          </button>
        ) : null}
        {children}
      </div>
    );
  }
}

ModalFooter.propTypes = {
  closeModalButton: PropTypes.bool,
  children: PropTypes.element,
};

ModalFooter.defaultProps = {
  closeModalButton: true,
  children: null,
};

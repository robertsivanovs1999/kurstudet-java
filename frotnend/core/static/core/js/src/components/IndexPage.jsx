import React from 'react';

import { Link } from 'react-router-dom';
import { useSpring, animated } from 'react-spring';
import styles from '../../../../style/indexPage.scss';

import quoteBottom from '../../../images/IndexPage/Vector_b.png';
import quoteTop from '../../../images/IndexPage/Vector_t.png';



export default function IndexPage() {
  return (
    <>
      <div className={styles['white-space']}>
        <div className="">
          <h1 className={styles['initial-txt']}>
            Izdari pareizo izvēli savai nākotnei
          </h1>
        </div>
        <div className="col-sm-12 p-0">
          <p className={styles['quote-txt']}>
            <span>
              <img
                src={quoteBottom}
                className={`pt-4 ${styles['img-quotes']}`}
                alt="Opening quote"
              />
            </span>
            Nekad nemāci cūku dziedāt. Tu tērē laiku un garlaiko cūku
            <span>
              <img
                src={quoteTop}
                className={`pb-4 ${styles['img-quotes']}`}
                alt="Closing quote"
              />
            </span>
          </p>
        </div>
        <div className="col-sm-12 p-0">
          <p className={styles['quote-auth']}>Marks Tvens</p>
        </div>
      </div>
      <div className={styles['index-background']}>
        <div className="container">
          <Card
            className={`container d-inline mr-5 float-left ${styles['btn-box']}`}
          >
            <Link
              to="/programmes/"
              className={`btn ${styles['gradient-purple']}`}
            >
                Programmas
            </Link>
            <p className={styles['btn-purple-dsc-txt']}>
                Izzini augstskolu piedāvājumu!
            </p>
          </Card>
          <Card
            className={`container d-inline mr-5 float-right ${styles['btn-box']}`}
          >
            <Link to="/test/" className={`btn ${styles['gradient-orange']}`}>
              Karjeras tests
            </Link>
            <p className={styles['btn-orange-dsc-txt']}>Izzini sevi!</p>
          </Card>
        </div>
      </div>
    </>
  );
}

const calc = (x, y) => [
  (y - window.innerHeight / 2) / 10,
  (x - window.innerWidth / 2) / -50,
  1.1,
];
const trans = (x, y, s) => `perspective(600px) rotateX(${x}deg) rotateY(${y}deg) scale(${s})`;

function Card({ children, className }) {
  const [props, set] = useSpring(() => ({
    xys: [0, 0, 1],
    config: { mass: 5, tension: 700, friction: 50 },
  }));
  return (
    <animated.div
      className={className}
      onMouseMove={({ clientX: x, clientY: y }) => set({ xys: calc(x, y) })}
      onMouseLeave={() => set({ xys: [0, 0, 1] })}
      style={{ transform: props.xys.interpolate(trans) }}
    >
      {children}
    </animated.div>
  );
}

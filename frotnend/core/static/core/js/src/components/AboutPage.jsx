import React from 'react';
import './style/about.css';
import robertsPicture from '../../../images/TeamPictures/roberts.jpg';
import kristofersPicture from '../../../images/TeamPictures/kristofers.jpg';
import emilsPicture from '../../../images/TeamPictures/emils.jpg';
import lauraPicture from '../../../images/TeamPictures/laura.jpg';
import arinaPicture from '../../../images/TeamPictures/arina.jpg';
import ronaldsPicture from '../../../images/TeamPictures/ronalds.jpg';

const pictureStyle = {
  height: '210px',
  width: '210px',
  borderRadius: '50%',
};

export default function AboutPage() {
  return (
    <div className="container-fluid padding">
      <h2>
        <strong>Kas ir Kurstudet.lv?</strong>
      </h2>
      <div className="about-info">
        <p>
          Nezini kur studēt? Tavi meklējumi ir beigušies! Kurstudet.lv ir
          lielisks rīks, kas tieši Tev palīdzēs atrast vispiemērotāko augstskolu
          un studiju programmu. Izpēti sev interesējošās programmas, izmantojot
          meklēšanas rīku, vai atrodi visatbilstošāko augstskolu, izpildot
          karjeras testu.
        </p>
        <p>
          Kurstudet.lv tika veidots kursa programmēšana tīmeklī java ietvaros. Tika izmantotas tādas tehnoloģijas kā Spring, Docker, React. 
        </p>
      </div>

      <h2>
        <strong>Komanda</strong>
      </h2>

      <p>
        Mūsu komandu veido zinoši un apņēmīgi jaunieši ar vienotu mērķi. Mēs
        esam:
      </p>
      
      <div className="team">
        <div className="person roberts">
          <div className="picture">
           <img
              src={robertsPicture}
              style={pictureStyle}
              title="Roberts Ivanovs"
            />
          </div>
          <div className="name">Roberts Ivanovs</div>
        </div>

        <div className="person">
           <img
              src={kristofersPicture}
              style={pictureStyle}
              title="Kristofers Volkovs"
            />
          <div className="name">Kristofers Volkovs</div>
        </div>

        <div className="person">
           <img
              src={arinaPicture}
              style={pictureStyle}
              title="Arina Solovjova"
            />
            <div className="name">Arina Solovjova</div>
        </div>
    </div>

    </div>
  );
}

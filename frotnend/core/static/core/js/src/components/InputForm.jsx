import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import style from './style/form.scss';

export default class InputForm extends PureComponent {
  render() {
    const {
      value,
      onChange,
      id,
      label,
      mutedText,
      children,
      inputType,
      placeholder,
      name,
    } = this.props;
    const helpTag = `${id}-help`;
    return (
      <div className={style['form-group']}>
        {label ? <label htmlFor={id} className={style['label-custom']}>{label}</label> : null}
        <input
          className={style['input-custom']}
          value={value}
          name={name}
          onChange={onChange}
          type={inputType}
          placeholder={placeholder}
          id={id}
          aria-describedby={helpTag}
        />
        {mutedText ? (
          <small id={helpTag} className="form-text text-muted">
            {mutedText}
          </small>
        ) : null}
        {children}
      </div>
    );
  }
}

InputForm.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  id: PropTypes.string,
  label: PropTypes.string,
  mutedText: PropTypes.string,
  children: PropTypes.element,
  inputType: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  extraClassNameInput: PropTypes.string,
};

InputForm.defaultProps = {
  value: undefined,
  onChange: null,
  id: null,
  label: null,
  mutedText: null,
  children: null,
  inputType: null,
  placeholder: null,
  name: null,
  extraClassNameInput: null,
};

import React, { createContext, Component } from 'react';
import Requester from '../misc/Requester';

export const UserContext = createContext();

const defaultState = {
  isLoggedIn: false,
  username: null,
  dateJoined: null,
  email: null,
  firstName: null,
  lastName: null,
  groups: null,
  permissions: null,
  isActive: null,
  isStaff: null,
  isSuperuser: null,
  profilePK: null,
};

export default class UserContextProvider extends Component {
  constructor() {
    super();
    this.state = {
      ...defaultState,
    };
    this.fetchCurrentUser();
  }

  fetchCurrentUser = async () => {
    const response = await Requester.get('fetch-current-user');

    if (response.data.data !== '{}') {
      const profile = JSON.parse(response.data.data);

      this.setState({
        isLoggedIn: true,
        username: profile.fields.username,
        dateJoined: profile.fields.date_joined,
        email: profile.fields.email,
        firstName: profile.fields.first_name,
        lastName: profile.fields.last_name,
        groups: profile.fields.groups,
        permissions: profile.fields.permissions,
        isActive: profile.fields.is_active,
        isStaff: profile.fields.is_staff,
        isSuperuser: profile.fields.is_superuser,
        profilePK: profile.pk,
      });
    } else {
      this.setState({
        ...defaultState,
      });
    }
  };

  logIn = async (username, password, callback) => {
    const response = await Requester.post('login', {
      username,
      password,
    });
    if (response.status === 200) {
      this.fetchCurrentUser();
    }
    callback(response);
  };

  logOut = async () => {
    // logout
    const response = await Requester.get('logout');
    // make sure that we are logged out and reset the global user state
    this.fetchCurrentUser();
  };

  render() {
    const { children } = this.props;
    return (
      <UserContext.Provider
        value={{
          ...this.state,
          logIn: this.logIn,
          logOut: this.logOut,
          ...this.props,
        }}
      >
        {children}
      </UserContext.Provider>
    );
  }
}

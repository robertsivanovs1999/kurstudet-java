import React, { createContext } from 'react';
import useModalHook from '../components/Modal/Modal';

export const ModalContext = createContext();

export default function ModalContextProvider(props) {
  const [
    modalComponent,
    toggleModal,
    setModalData,
    setCallbackOnExit,
    toggleOffComponent,
  ] = useModalHook(<div>Empty modal</div>);
  return (
    <ModalContext.Provider
      value={{
        modalComponent, toggleModal, setModalData, setCallbackOnExit, toggleOffComponent, ...props,
      }}
    >
      {props.children}
    </ModalContext.Provider>
  );
}

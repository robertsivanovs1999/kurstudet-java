import Axios from 'axios';

class Requester {
  constructor() {
    this.axios = Axios;
    this.axios.defaults.xsrfCookieName = 'csrftoken';
    this.axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN';
    this.urls = {
      login: '/auth/log-in',
      logout: '/auth/log-out',
      register: '/auth/register',
      'fetch-current-user': '/auth/fetch-current-user',
      // Questions
      'get-result': '/api/get-result',
      'create-parameter': '/api/create-parameter',
      'create-question': '/api/create-question',
      'create-question-set': '/api/create-question-set',
      'create-result': '/api/create-result',
      'create-answer': '/api/create-answer',
      'get-all-parameters': '/api/get-all-parameters',
      'get-all-questions': '/api/get-all-questions',
      'get-all-question-sets': '/api/get-all-question-sets',
      'get-question-set': '/api/get-question-set',
      'add-question-to-question-set': '/api/add-question-to-question-set',
      'move-question-in-question-set': '/api/move-question-in-question-set',
      'get-evaluated-results': '/api/get-evaluated-results',
      'update-result': '/api/update-result',
      // Education
      'get-all-programmes': '/api/get-programme',
      'get-all-universities': '/api/get-university',
      'add-parameter-to-programme': '/api/add-parameter-to-programme',
      'remove-parameter-from-programme': '/api/remove-parameter-from-programme',
      'update-parameter-for-programme': '/api/update-parameter-for-programme',
      'get-programme-parameters': '/api/get-programme-parameters',
      'get-programme-name-aggregate': '/api/get-programme-name-aggregate',
      'get-study-form-aggregate': '/api/get-study-form-aggregate',
      'get-study-type-aggregate': '/api/get-study-type-aggregate',
      'get-programme-duration-aggregate': '/api/get-programme-duration-aggregate',
      'get-payment': '/api/get-payment',
      'get-degree': '/api/get-degree',
      'get-language': '/api/get-language',
    };
  }

  /**
   * Perform a POST request to the specified URL
   * @param  {string} url - URL to send the data to
   * @param  {json} data={} - the POST data to send
   */
  post = async (url, data = {}) => {
    const response = await this.axios
      .post(this.urls[url], data)
      .catch((error) => error.response);
    return response;
  };

  /**
   * Perform a GET request to the specified URL
   * @param  {string} url - URL request
   * @param  {object} params - get params as json
   */
  get = async (url, params = '') => {
    const response = await this.axios.get(this.urls[url], {
      params,
    });
    return response;
  };
}

const RequesterInstance = new Requester();
export default RequesterInstance;

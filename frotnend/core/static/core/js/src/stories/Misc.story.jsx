import React from 'react';
import { storiesOf } from '@storybook/react';
import ExpandButton from '../components/Misc/ExpandButton';

const dropDownItems = [
  { text: 'Mainīt', name: 'Alter' },
  { text: 'Dzēst', name: 'Delete' },
];

storiesOf('Miscellaneous', module)
  .add('ExpandButton default', () => <ExpandButton />)
  .add('ExpandButton options', () => (
    <ExpandButton dropDownItems={dropDownItems} />
  ));

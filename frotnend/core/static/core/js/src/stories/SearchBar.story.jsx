import React from 'react';
import { storiesOf } from '@storybook/react';
import SearchBar from '../components/SearchBar';

storiesOf('Header', module).add('Search', () => <SearchBar />);

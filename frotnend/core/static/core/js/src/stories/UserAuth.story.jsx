import React from 'react';
import { storiesOf } from '@storybook/react';
import SignUp from '../containers/SignUp';
import LogIn from '../containers/LogIn';
import InputForm from '../components/InputForm';

storiesOf('User Authentication', module)
  .add('SignUp', () => <SignUp />)
  .add('LogIn', () => <LogIn />)
  .add('Input Form | Default', () => <InputForm />)
  .add('Input Form | Password', () => (
    <InputForm
      id="passowrd"
      label="parole:"
      inputType="password"
      placeholder="********"
      name="password2"
      mutedText="Our passwords are stored in a vault"
    >
      <div> Displaying some children info here</div>
    </InputForm>
  ));

import React, { useState, useEffect } from 'react';
import useInput from '../../../../../../core/static/core/js/src/containers/AdminPanel/commonHooks';
import useModelSelect from '../../../../../../education/static/education/js/src/common/commonHooks';

import styles from '../../../style/finalQuestions.scss';
import {
  getPayment,
  getLanguage,
  getAggregatedStudyTypes,
  getAggregatedProgrammeDuration,
  getAggregatedStudyForms,
  getDegree,
} from '../../../../../../education/static/education/js/src/common/apiCalls';

const styleObj = {
  clearIndicator: (base, state) => {
    return {
      ...base,
      color: '#F37335',
      verticalAlign: 'middle',
      textAlign: 'center',
    };
  },
  control: (base, state) => ({
    ...base,
    background: '#FFFFFF',
    border: `0.2rem solid ${state.isFocused ? '#7F00FF' : '#60847B'} `,
    borderRadius: '2rem',
    outline: 'none',
    fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontWeight: 'normal',
    display: 'flex',
    boxShadow: 'none',
    borderColor: '#7F00FF',
    '&:hover': { borderColor: state.isFocused ? '#7F00FF' : '#60847B' },
  }),
  dropdownIndicator: (base) => ({
    ...base,
    display: 'none',
  }),
  indicatorSeparator: (base) => ({
    ...base,
    display: 'none',
  }),
  placeholder: (base) => ({ ...base, color: '#000000', textAlign: 'center', fontWeight: 'bold'}),
  multiValueLabel: (base) => ({
    ...base,
    fontSize: '0.8rem',
    fontWeight: 'bold',
    // color: "white",
    backgroundColor: '#9FACA8',
    borderRadius: '1rem 0 0 1rem',
  }),
  multiValueRemove: (base) => ({
    ...base,
    backgroundColor: '#F37335',
    borderRadius: '0 1rem 1rem 0',
  }),
  multiValue: (base) => ({ ...base, backgroundColor: 'transparent' }),
};

export default function FinalQuestions({ onClick }) {
  const [allStudyForms, setAllStudyForms] = useState([]);
  const [selectedStudyForms, setSelectedStudyFormsPR] = useState([]);
  const [setSelectedStudyForms] = useModelSelect({
    type: 'selectable',
    initialData: allStudyForms,
    key: 'form',
    placeholder: 'Klātiene/ neklātiene',
    className: styles['default-filter-button'],
    getOptionValue: (option) => option.name,
    getOptionLabel: (option) => option.name,
    values: selectedStudyForms,
    setValues: (val) => setSelectedStudyFormsPR(val),
    overrideStyle: styleObj,
  });

  const [aggregatedDuration, setAggregatedDuration] = useState([]);
  const [selectedDuration, setSelectedDurationPR] = useState([]);
  const [setSelectedDurations] = useModelSelect({
    type: 'selectable',
    initialData: aggregatedDuration,
    key: 'duration',
    placeholder: 'Studiju Ilgums',
    className: styles['default-filter-button'],
    getOptionValue: (option) => option.length,
    getOptionLabel: (option) => option.length,
    values: selectedDuration,
    setValues: setSelectedDurationPR,
    overrideStyle: styleObj,
  });

  const [studyTypes, setStudyTypes] = useState([]);
  const [selectedStudyTypesPR, setSelectedStudyTypesPR] = useState([]);
  const [selectedStudyTypes] = useModelSelect({
    type: 'selectable',
    initialData: studyTypes,
    key: 'duration',
    placeholder: 'Pilna/ nepilna laika',
    className: styles['default-filter-button'],
    getOptionValue: (option) => option.type,
    getOptionLabel: (option) => option.type,
    values: selectedStudyTypesPR,
    setValues: setSelectedStudyTypesPR,
    overrideStyle: styleObj,
  });

  const [language, setLanguage] = useState([]);
  const [selectedLanguagesPR, setSelectedLanguagesPR] = useState([]);
  const [selectedLanguages] = useModelSelect({
    type: 'selectable',
    initialData: language,
    key: 'duration',
    placeholder: 'Programams valoda',
    className: styles['default-filter-button'],
    getOptionValue: (option) => option.fields.abbreviation,
    getOptionLabel: (option) => option.fields.abbreviation,
    values: selectedLanguagesPR,
    setValues: setSelectedLanguagesPR,
    overrideStyle: styleObj,
  });

  const [degree, setDegree] = useState([]);
  const [selectedDegreesPR, setSelectedDegreesPR] = useState([]);
  const [selectedDegrees] = useModelSelect({
    type: 'selectable',
    initialData: degree,
    key: 'duration',
    placeholder: 'Kvalifikācijas grāds',
    className: styles['default-filter-button'],
    getOptionValue: (option) => option,
    getOptionLabel: (option) => option,
    values: selectedDegreesPR,
    setValues: setSelectedDegreesPR,
    overrideStyle: styleObj,
  });

  useEffect(() => {
    // setAllStudyForms
    getAggregatedStudyForms(setAllStudyForms);
    // setAggregatedDuration
    getAggregatedProgrammeDuration(setAggregatedDuration);
    // setStudyTypes
    getAggregatedStudyTypes(setStudyTypes);
    // setLanguage
    getLanguage(setLanguage);
    // setDegree
    getDegree(setDegree);
  }, []);

  const [myInstitution, setMyInstitution] = useInput({
    placeholder: 'Tagadējā izglītības iestāde',
    type: 'text',
    className: 'input-group',
  });
  const [myLocation, setMyLocation] = useInput({
    placeholder: 'Mana dzīvesvieta (Pilsēta)',
    type: 'text',
    className: 'input-group',
  });
  const [myAge, setMyAge] = useInput({
    placeholder: 'Mans vecums',
    type: 'number',
    className: 'input',
  });
  const [myGrade, setMyGrade] = useInput({
    placeholder: 'Pašreiz mācos ... klasē',
    type: 'number',
    className: 'input',
  });
  const [mySuggestions, setMySuggestions] = useInput({
    placeholder: 'Atsauksmes par testu',
    type: 'text',
    className: 'input-group',
  });
  const [maxCost, setMaxCost] = useInput({
    placeholder: 'Pašreiz mācos ... klasē',
    type: 'range',
    className: 'slider',
    extra: {
      min: '1',
      max: '10000',
    },
  });
  const [onlyFreebies, setOnlyFreebies] = useState(false);
  const [isActive, setActive] = useState(true);
  const activateBtn = () => {
    setOnlyFreebies(!onlyFreebies);
    setActive(!isActive);
  };

  return (
    <>
      <div className="container">
        <div className="font-weight-bold mt-5 mb-5 h4">
          Norādiet papildus filtrus, ja nepieciešams, lai mēs varētu palīdzēt TEV
          atrast ideālo programmu!
        </div>
        <div className="row">
          <div className="col-4">
            <div>{setSelectedDurations}</div>
          </div>
          <div className="col-4">
            <div>{setSelectedStudyForms}</div>
          </div>
          <div className="col-4">
            <div>{selectedStudyTypes}</div>
          </div>
        </div>
        <div className="row mt-5">
          <div className="col-4">
            <div>{selectedDegrees}</div>
          </div>
          <div className="col-4">
            <div>{selectedLanguages}</div>
          </div>
          <div className="col-4">
            <button onClick={activateBtn} className={` ${isActive ? styles['filter-btn-inactive'] : styles['filter-btn-active']}`}>Budžeta vieta</button>
          </div>
        </div>
        <div className="mt-4">
          {!onlyFreebies && (
            <div>
              <p>Maksimālā cena: {maxCost} € / gadā</p> 
              <div>
                {setMaxCost}
              </div>
            </div>
          )}
        </div>
      </div>
      <div className={`container mb-4 mt-4 ${styles['personal-info-box']}`}>
        <p className="overflow-hidden text-center pt-4 mb-3 mr-5 ml-5 h5">
          Aizpildot zemāk norādītos laukus ar savu informāciju, mēs nākotnē varēsim uzlabot un padarīt anketas rezultātus precīzākus.
        </p>
        <p className={`text-center h5 mb-3 ${styles['not-obligatory-txt']}`}>
          Tas nav obligāti!
        </p>
        <div className="row mt-4 mr-4 ml-4">
          <p className="col-8 mb-1">Izglītības iestāde</p>
          <p className="col-4 mb-1">Klase</p>
        </div>
        <div className="row mr-4 ml-4">
          <div className="col-8">{setMyInstitution}</div>
          <div className="col-4">{setMyGrade}</div>
        </div>
        <div className="row mt-4 mr-4 ml-4">
          <p className="col-8 mb-1">Pilsēta</p>
          <p className="col-4 mb-1">Vecums</p>
        </div>
        <div className="row mr-4 ml-4">
          <div className="col-8">{setMyLocation}</div>
          <div className="col-4">{setMyAge}</div>
        </div>
        <div className="mr-5 ml-5">
          <p className="mt-4 mb-1">Atsauksmes par testu</p>
          <div className="pb-5">{setMySuggestions}</div>
        </div>
      </div>
      <div className={styles['darker-green-bg']}>
        <button
          type="button"
          className={`btn ${styles['white-btn']}`}
          onClick={() =>
            onClick({
              myInstitution,
              myLocation,
              myAge,
              myGrade,
              mySuggestions,
              selectedDegreesPR,
              selectedStudyForms: selectedStudyForms.map((el) => el.name),
              selectedDuration: selectedDuration.map((el) => el.length),
              selectedStudyTypes: selectedStudyTypesPR.map((el) => el.type),
              selectedLanguages: selectedLanguagesPR.map(
                (el) => el.fields.abbreviation,
              ),
              currentOnlyFree: onlyFreebies,
              currentMaxCost: maxCost,
            })
          }
        >
          Iegūt rezultātus
        </button>
      </div>
    </>
  );
}

import React, { } from 'react';
import ClickableOptionHolster from '../components/ClickableOptionHolster';
import style from '../../../style/test.scss';

export default function TestWidget({title, onClick}) {
  return (
    <div className={style["single-test-container"]}>
      <div className={`card m-2 ${style["single-test"]}`}>
        <div className={`card-body ${style["options-container"]}`}>
          <h5
            className={`card-title text-center pb-2 font-weight-bold ${style.title}`}
          >
            {title}
          </h5>
          <div className="container">
            <div className="card-text row text-center font-weight-bold">
              <div className={`col-2 ${style["options-text"]}`}>Nepiekrītu</div>
              <div className="col-8">
                <ClickableOptionHolster onClick={onClick} />
              </div>
              <div className={`col-2 ${style["options-text"]}`}>Piekrītu</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

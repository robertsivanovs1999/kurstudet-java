import React, {
  useState, useEffect, useContext, useCallback,
} from 'react';
import Select from 'react-select';

import { useHistory, useParams } from 'react-router-dom';
import { getAnswer, getQuestionsForQuestionSet } from '../common/apiCalls';

import SingleTest from './SingleTest';
import FinalQuestions from './FinalQuestions';
import BtnNext from '../components/BtnNext';

import style from '../../../style/test.scss'

export default function TestManager() {
  const { questionSet } = useParams();
  const { push } = useHistory();

  const [page, setPage] = useState({
    num_pages: null,
    total_count: null,
    current_page: '1',
    per_page: '20',
  });

  const [questions, setQuestions] = useState([]);
  const [renderFinalQuestions, setRenderFinalQuestions] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [answers, setAnswers] = useState({});

  const fetchNextPage = useCallback((nextPage) => {
    setIsLoading(true);
    getQuestionsForQuestionSet(questionSet || 'Base', nextPage, (result) => {
      setQuestions(JSON.parse(result.data));
      setPage(result.page);
      setIsLoading(false);
    });
  });

  useEffect(() => {
    fetchNextPage(1);
  }, [questionSet]);

  const saveLocalAnswers = useCallback((qsaqmPk, value) => {
    setAnswers({ ...answers, [qsaqmPk]: value });
  });

  const fetchResults = useCallback((filterObject) => {
    getAnswer(
      {
        answers: Object.entries(answers).map(([key, val]) => ({
          questionsetmap: key,
          answer_weight: val,
        })),
        ...filterObject,
      },
      (res) => {
        push(`/result/${res.resultId}`);
      },
    );
  });
  const amountToCheckToContinue = Number(page.per_page) * Number(page.current_page);
  const checkedOnTotal = Object.keys(answers).length;

  let button = null;

  if (checkedOnTotal === Number(page.total_count)) {
    button = (
      <div className={style['fixed-btn']}>
        <BtnNext onClick={() => setRenderFinalQuestions(true)} />
      </div>
    );
  } else if (checkedOnTotal === amountToCheckToContinue) {
    button = (
      <div className={style["fixed-btn"]}>
        <BtnNext onClick={() => fetchNextPage(Number(page.current_page) + 1)} />
      </div>
    );
  }
  return (
    <div>
      {isLoading && <div>LOADING...</div>}
      {!isLoading && questions && !renderFinalQuestions && (
        <>
          <div className={`progress ${style["progress-container"]}`}>
            <div
              className={`progress-bar ${style["progress-bar"]}`}
              style={{
                width: `${(checkedOnTotal / Number(page.total_count)) * 100}%`,
              }}
            />
          </div>
          <SingleTest questions={questions} onClick={saveLocalAnswers}>
            {button}
          </SingleTest>
        </>
      )}
      {renderFinalQuestions && <FinalQuestions onClick={fetchResults} />}
    </div>
  );
}

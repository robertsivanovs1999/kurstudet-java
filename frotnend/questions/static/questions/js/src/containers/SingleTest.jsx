import React, { useState, useEffect, useCallback } from 'react';

import TestWidget from './TestWidget';
import RequesterInstance from '../../../../../../core/static/core/js/src/misc/Requester';
import style from '../../../style/test.scss'

function setSingleAnswer(index, value, list, callback) {
  const alteredAnswers = [...list];
  alteredAnswers[index] = value;
  callback(alteredAnswers);
}

export default function SingleTest({ questions, onClick, children }) {
  return (
    <div className={style['single-page']}>
      {questions &&
        questions.map((el, idx) => (
          <TestWidget
            key={`${el.pk}`}
            title={el.fields.text}
            onClick={(val) => onClick(el.fields.questionsetandquestionmap, val)}
          />
        ))}
      {children}
    </div>
  );
}

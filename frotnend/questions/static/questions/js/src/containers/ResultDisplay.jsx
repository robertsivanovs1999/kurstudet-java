import React, { useState, useEffect, useCallback } from 'react';

import { useParams } from 'react-router-dom';
import ProgrammeRow from '../components/ProgrammeRow';
import DetailedResult from '../components/DetailedResult';
import { getCalculatedAnswer, updateAnswer } from '../common/apiCalls';
import FinalQuestions from './FinalQuestions';


export default function ResultDisplay() {
  const { pk } = useParams();
  const [result, setResult] = useState(null);

  useEffect(() => {
    getCalculatedAnswer(pk, setResult);
  }, [pk]);

  const [selectedIndex, setSelectedIndex] = useState(0);
  const updateResult = useCallback((filterObject) => {
    updateAnswer(
      {
        ...filterObject,
        result: pk,
      },
      () => {
        getCalculatedAnswer(pk, setResult);
      },
    );
  });
  if (!result) return null;
  console.log(result.data);

  if (result.data.length === 0) {
    return (
      <div>
        Piedod, bet mēs nevarējām atrast nevienu rezultātu, kas būtu līdzvērtīgi
        taviem filtriem. Lūdzu tos pamaini!
        <FinalQuestions onClick={updateResult} />
      </div>
    );
  }

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-8 m-5">
          {result.data.map((el, idx) => (
            <ProgrammeRow
              result={el}
              key={el.programme.pk}
              index={idx}
              onClick={() => setSelectedIndex(idx)}
            />
          ))}
        </div>
        <div className="col-3">
          <DetailedResult programme={result.data[selectedIndex]} />
        </div>
      </div>
    </div>
  );
}

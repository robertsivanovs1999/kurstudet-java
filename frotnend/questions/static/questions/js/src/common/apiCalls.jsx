import RequesterInstance from '../../../../../../core/static/core/js/src/misc/Requester';

export async function loadQuestionSets(inputValue, callback) {
  const results = await await RequesterInstance.get('get-all-question-sets', {
    name: inputValue,
  });
  callback(JSON.parse(results.data.data));
}

export async function loadQuestions(callback) {
  const resultQuestions = await RequesterInstance.get('get-all-questions');
  callback(JSON.parse(resultQuestions.data.data));
}

export async function getQuestionsForQuestionSet(
  QuestionSet,
  currentPage,
  callback,
) {
  const result = await RequesterInstance.get('get-question-set', {
    QuestionSet,
    page: currentPage,
  });
  callback(result.data);
}

export async function getAnswer(payload, callback) {
  const result = await RequesterInstance.post('get-result', {
    ...payload,
  });
  callback(result.data.data);
}

export async function getCalculatedAnswer(pk, callback) {
  const result = await RequesterInstance.get('get-evaluated-results', {
    result: pk,
  });
  callback(result.data.data);
}

export async function updateAnswer(payload, callback) {
  const result = await RequesterInstance.post('update-result', {
    ...payload,
  });
  callback(result.data.data);
}

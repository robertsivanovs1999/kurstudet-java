import React from 'react';

import style from '../../../style/results.scss';
import { makeShort } from '../../../../../../education/static/education/js/src/containers/DisplayCard';

export default function ProgrammeRow({ result, onClick }) {
  return (
    <div
      role="button"
      type="button"
      className={`pt-3 mt-3 ${style['result-card']}`}
      onClick={onClick}
    >
      <div className="row">
        <div className="col">
          <div className={`progress ml-2 ${style['gradient-top']}`}>
            <div
              className={`progress-bar ${style['gradient-bot']}`}
              style={{ width: `${result.val.score}%` }}
            >
              {Math.floor(result.val.score.toFixed(2))} %
            </div>
          </div>
        </div>
        <div className="col text-right mr-2">
          {makeShort(result.programme.fields.university_name, 65)}
        </div>
      </div>
      <div className="font-weight-bold text-left ml-2">
        <h1 className="font-weight-bold">
          {makeShort(result.programme.fields.title, 30)}
        </h1>
      </div>
      <div className="row mb-2">
        <div className="col text-left ml-2">
          <div className="font-weight-bold">Fakultāte</div>
          <div>{makeShort(result.programme.fields.faculty_name, 30)}</div>
        </div>
        <div className="col text-left">
          <div className="font-weight-bold">Valoda</div>
          <div>{makeShort(result.programme.fields.language, 30)}</div>
        </div>
        <div className="col text-left">
          <div className="font-weight-bold">Studiju ilgums</div>
          <div>{makeShort(result.programme.fields.time, 30)}</div>
        </div>
        <div className="col text-left mr-2">
          <div className="font-weight-bold">Maksa</div>
          <div>{makeShort(result.programme.fields.pay.join(' '), 30)}</div>
        </div>
      </div>
    </div>
  );
}

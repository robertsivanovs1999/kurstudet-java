import React from 'react';
import style from '../../../style/test.scss';
import { useSpring, animated, config } from 'react-spring';
import { useState } from 'react';

export default function BtnNext({onClick}){

    const [props, set, stop] = useSpring(() => ({
      opacity: 0,
    }));
  
    set({
      opacity: 1,
    });
    stop();

    return (
      <animated.div style={props} className={style.props}>
        <div className={style["btn-container"]}>
          <button
            type="button"
            className={`btn btn-primary ${style["button-custom"]}`}
            onClick={onClick}
          >
            Nākošā lapa
          </button>
        </div>
      </animated.div>
    );
}
import React, { Component } from 'react';
import style from '../../../style/test.scss'

import StrongDisPas from '../../../images/Circles/Orange_big_outside.png';
import StrongDisAct from '../../../images/Circles/Orange_big_filled.png';

import DisPas from '../../../images/Circles/Orange_small_outside.png';
import DisAct from '../../../images/Circles/Orange_small_filled.png';

import MildDisPas from "../../../images/Circles/Orange_smallest_outside.png";
import MildDisAct from "../../../images/Circles/Orange_smallest_filled.png";

import NeutralPas from '../../../images/Circles/Gray_outside.png';
import NeutralAct from '../../../images/Circles/Gray_filled.png';

import StrongAgrPas from '../../../images/Circles/Blue_big_outside.png';
import StrongAgrAct from '../../../images/Circles/Blue_big_filled.png';

import AgrPas from '../../../images/Circles/Blue_small_outside.png';
import AgrAct from '../../../images/Circles/Blue_small_filled.png';

import MildAgrPas from "../../../images/Circles/Blue_smallest_outside.png";
import MildAgrAct from "../../../images/Circles/Blue_smallest_filled.png";

export default class ClickableOptionHolster extends Component {
  constructor(props) {
    super(props);
    this.options = [
      {
        urlPassive: StrongDisPas,
        urlActive: StrongDisAct,
        description: 'Strongly disagree',
        strength: -100,
      },
      {
        urlPassive: DisPas,
        urlActive: DisAct,
        description: 'Disagree',
        strength: -50,
      },
      {
        urlPassive: MildDisPas,
        urlActive: MildDisAct,
        description: 'Mildly Disagree',
        strength: -25,
      },
      {
        urlPassive: NeutralPas,
        urlActive: NeutralAct,
        description: 'Neutral',
        strength: 0,
      },
      {
        urlPassive: MildAgrPas,
        urlActive: MildAgrAct,
        description: 'Mildly Agree',
        strength: 25,
      },
      {
        urlPassive: AgrPas,
        urlActive: AgrAct,
        description: 'Agree',
        strength: 50,
      },
      {
        urlPassive: StrongAgrPas,
        urlActive: StrongAgrAct,
        description: 'Strongly Agree',
        strength: 100,
      },
    ];
    this.state = {
      selected: null,
    };
    this.grid = {
      display: 'grid',
      gridTemplateColumns: 'auto auto auto auto auto auto auto',
      gridTemplateRows: 'auto auto',
    };
  }

  selectCircle = (id) => {
    this.setState({ selected: id });
  };

  render() {
    const { selected } = this.state;
    const { onClick } = this.props;
    return (
      <div style={this.grid}>
        {this.options.map((el, id) => (
          <button
            key={el.strength}
            type="button"
            onClick={() => {
              this.selectCircle(id);
              onClick(el.strength);
            }}
            className="btn btn-link"
          >
            <img
              alt={el.description}
              className={`img mx-auto d-block ${style.option}`}
              src={selected === id ? el.urlActive : el.urlPassive}
            />
          </button>
        ))}
      </div>
    );
  }
}

import React from 'react';
import style from '../../../style/results.scss';
import icons from '../../../images/result_page/Icons.png';

export default function DetailedResult({ programme }) {
  const aggrResults = {};
  programme.val.results.forEach((el) => {
    if (el.tag in aggrResults) {
      aggrResults[el.tag] = {
        count: aggrResults[el.tag].count + 1,
        res: aggrResults[el.tag].res + el.parameter_eval_score,
      };
    } else {
      aggrResults[el.tag] = {
        count: 1,
        res: el.parameter_eval_score,
      };
    }
  });
  const aggrResultsArray = Object.entries(aggrResults)
    .map(([key, value]) => ({
      parameter_abbr: key,
      parameter_eval_score: value.res / value.count,
    }))
    .sort((a, b) => a.parameter_abbr > b.parameter_abbr);
  return (
    <div className="container-fluid">
      <div className={`card ${style['result-icon']}`}>
        <img src={icons} className="card-img-top" alt="Card image cap" />
        <div className="">
          {/* add short so that id doesn't display more than 2 words */}
          <h5 className="">{programme.programme.fields.title}</h5>
        </div>
      </div>
      <div className={`card ${style['result-bars']}`}>
        <div className={style['result-bars-top']}>
          <div className="mt-4 mb-4">
            {aggrResultsArray.map((el) => (
              <div key={el.parameter_abbr}>
                <p className="h5">{el.parameter_abbr}</p>
                <div className={`progress ${style['gradient-top']}`}>
                  <div
                    className={`progress-bar ${style['gradient-right-sidebar']}`}
                    style={{ width: `${el.parameter_eval_score}%` }}
                  >
                    {Math.floor(el.parameter_eval_score.toFixed(2))}%
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
        <button className="btn">Skatīt vairāk</button>
      </div>
    </div>
  );
}

FROM maven:3.6.3-openjdk-8

# arbitrary location choice: you can change the directory
RUN mkdir -p /opt/services/web/src
WORKDIR /opt/services/web/src

# copy our project code
COPY . /opt/services/web/src

# expose the port 8000
EXPOSE 8080

# define the default command to run when starting the container
RUN mvn dependency:resolve
CMD ["mvn", "clean", "spring-boot:run"]

